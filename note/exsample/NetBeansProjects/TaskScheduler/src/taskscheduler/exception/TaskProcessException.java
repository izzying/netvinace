/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package taskscheduler.exception;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import taskscheduler.util.ReloadEvent;
import taskscheduler.util.SystemReloader;

/**
 *
 * @author user
 */
public class TaskProcessException extends RuntimeException{

    SystemReloader reload;
    public TaskProcessException(){
        super();
        reload=new SystemReloader();
        reload.reload(new ReloadEvent(this));
    }
    
    public TaskProcessException(String message){
        super();
         reload=new SystemReloader();
        try {
            PrintStream pr = new PrintStream(message);
             this.printStackTrace(pr);
             reload.reload(new ReloadEvent(this));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TaskProcessException.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        
    }


    
}
