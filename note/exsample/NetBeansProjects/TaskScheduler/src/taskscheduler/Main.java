/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package taskscheduler;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.logging.Level;
import java.util.logging.Logger;
import taskscheduler.exception.TaskProcessException;
import taskscheduler.util.ThreadPool;

/**
 *
 * @author user
 */
public class Main {

    private final static String FILE_SERVER_PATH = "C://Documents and Settings//user//My Documents//NetBeansProjects//FileServer//dist//FileServer.jar";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String file1 = "file01.txt";
        String file2 = "file02.txt";
        String file3 = "file01.pdf";

        try {
            File extranelFile = new File(FILE_SERVER_PATH);
            if (extranelFile.exists()) {
                String cmd = "java -jar " + "\"" + FILE_SERVER_PATH + "\"" + " " + "\"" + file1 + "\"" + " " + "\"" + file2 + "\"" + " " + "\"" + file3 + "\"";
//                ProcessBuilder pb = new ProcessBuilder();
//                pb.command(cmd);
//                pb.redirectErrorStream(true);
//                pb.start();
                final Process p = Runtime.getRuntime().exec(cmd);
                new Thread() {

                    public void run() {
                        try {
                            String content = "";
                            InputStreamReader ins = new InputStreamReader(p.getInputStream());
                            BufferedReader bufR = new BufferedReader(ins);
                            while ((content = bufR.readLine()) != null) {
                                System.out.println(content);
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }.start();
                p.waitFor();
            } else {
                System.err.println("the file is not exists..");
                System.exit(0);
            }

        } catch (InterruptedException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
