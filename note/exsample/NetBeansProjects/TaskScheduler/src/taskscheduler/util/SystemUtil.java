/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package taskscheduler.util;

import java.io.File;

/**
 *
 * @author user
 */
public class SystemUtil {

    public static boolean isDirectory(String filePath) {

        boolean flag = false;

        File dir = new File(filePath);
        flag = dir.isDirectory();

        return flag;
    }

    public static boolean isEmpty(String filePath) {


        boolean flag = true;

        File dir = new File(filePath);
        File[] fileList = dir.listFiles();
        if (fileList == null && fileList.length <= 0) {
            flag = true;
        } else {
            flag = false;
        }


        return flag;
    }
}
