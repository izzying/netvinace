/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package taskscheduler.util;

import java.util.LinkedList;

/**
 *
 * @author user
 */
public class ThreadPool extends ThreadGroup {

    private static LinkedList taskList;
    private static int maxSize;
    private static int currectIdx = 0;

    public ThreadPool(String poolID, int maxSize) {
        super(poolID);
        this.maxSize = maxSize;
    }

    public int addTask(Process p) {

        if (currectIdx < maxSize) {
            taskList.add(currectIdx + 1, p);
        } else {
            return -1;
        }
        return currectIdx;

    }

    public static boolean killThread(Process p){
        try{
        p.destroy();
        return true;
        }catch(Exception e){
            e.printStackTrace();
            System.exit(-1);
            return true;
        }
    }
}
