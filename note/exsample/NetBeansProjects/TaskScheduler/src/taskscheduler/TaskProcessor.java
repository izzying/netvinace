/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package taskscheduler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author user
 */
public class TaskProcessor {

    private final static String FILE_SERVER_PATH = "C://Documents and Settings//user//My Documents//NetBeansProjects//FileServer//dist//FileServer.jar";
    private static Map<File, List<File>> FileList;
    private static String UPLOAD_ROOT = "";
    private TaskSchedulerClient gui;

    public TaskProcessor(String fileURL, TaskSchedulerClient gui) {
        FileList = new HashMap<File, List<File>>(0);
        this.gui = gui;
        File uploadFolder = new File(fileURL);
        UPLOAD_ROOT = uploadFolder.getName();
        System.out.println("UPLOAD_ROOT is " + UPLOAD_ROOT);
    }

    private void checkFolder(String fileURL) {
        //UPLOAD_ROOT=fileURL;
        File uploadFolder = new File(fileURL);


        if (uploadFolder.isDirectory()) {

            File[] subFiles = uploadFolder.listFiles();
            List<File> files = new ArrayList(0);
            for (File subFile : subFiles) {

                if (subFile.isDirectory()) {
                    checkFolder(subFile.getAbsolutePath());
                } else if (subFile.isFile()) {
                    files.add(subFile);
                    continue;
                }
            }

            FileList.put(uploadFolder, files);
        }



    }

    public void showFloder() {

        Set<File> parent = FileList.keySet();
        Iterator<File> ist = parent.iterator();
        while (ist.hasNext()) {
            File par = ist.next();
            System.out.println("parent:");
            System.out.println(par.getAbsolutePath());
            List<File> subFiles = FileList.get(par);
            System.out.println("sub:");
            for (File file : subFiles) {
                System.out.println(file.getAbsolutePath());
            }
        }

    }

    private void copyFileToServer(final String fileURL) {
      
            new Thread() {

                public void run() {
                    try {
                        Thread.currentThread().sleep(5000);
                        //gui.getUploadBtn().setEnabled(false);
                        gui.getjLabel4().setText("uploading...");
                        File sourceFolder = new File(fileURL);
                        if (sourceFolder.exists()) {
                            File targetFolder = new File("D://test2");
                            if (!targetFolder.exists()) {
                                targetFolder.mkdirs();
                            }
                            try {
                                FileUtils.copyDirectoryToDirectory(sourceFolder, targetFolder);
                            } catch (IOException ex) {
                                Logger.getLogger(TaskProcessor.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        SwingUtilities.invokeLater(new Runnable() {

                            public void run() {
                                gui.getUploadBtn().setEnabled(true);
                                gui.getjLabel4().setText("finish...");
                            }
                        });
                    } catch (InterruptedException ex) {
                        Logger.getLogger(TaskProcessor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }.start();
        

    }

    private void chansferFile(File file) {
    }

    private TaskProcessor() {
    }

    public boolean upload(String fileURL) {

        UPLOAD_ROOT = fileURL;
        checkFolder(fileURL);
        showFloder();   //just for debug....
        if (FileList.size() != 0) {
            copyFileToServer(this.UPLOAD_ROOT);
            return true;
        } else {
            return false;
        }



    }
}
