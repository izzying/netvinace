/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package client;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class Client {


    private Socket socket;

    private String connectIP="127.0.0.1";

    private int connectPort=9000;


    public void sendMessage(String content){
        try {
            
            socket = new Socket(connectIP, connectPort);
            DataOutputStream out=new DataOutputStream(socket.getOutputStream());
            out.writeUTF(content);
            out.flush();
            out.close();
        } catch (UnknownHostException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
