/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fileserver;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class Server extends Thread {

    private ServerSocket serverSocket;
    private DataInputStream serverMsgReader;
    private Socket clientSocket;
    private PrintWriter serverMsgWriter;

    public Server() {
        try {
            serverSocket = new ServerSocket(9000);

        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    public void run() {
        try {
            while (!isInterrupted()) {
                acceptClient();
            }
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void acceptClient() throws IOException {

        clientSocket = serverSocket.accept();
        while (true) {
            while (clientSocket.getInputStream() != null) {
                ServerInputStreamReader read = new ServerInputStreamReader();
                Thread rt = new Thread(read);
                rt.start();
            }
        }

    }

    class ServerInputStreamReader implements Runnable {

        private boolean resc = true;

        public void run() {
            String content = "";
            try {
                serverMsgReader = new DataInputStream(clientSocket.getInputStream());
                while (resc) {
                    resc = false;
                    content = serverMsgReader.readUTF();
                    System.out.println("content:" + content);
                    serverMsgReader.close();
                    resc = true;

                }
            } catch (IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    acceptClient();
                } catch (IOException ex) {
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
    }
}
