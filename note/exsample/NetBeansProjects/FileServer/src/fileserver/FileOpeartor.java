/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fileserver;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class FileOpeartor {

    private List<File> targetFile;
    private String targetDIR = "D://taskTest";

    public FileOpeartor(String[] fileURL) {

        if (fileURL == null || fileURL.length <= 0) {
            return;
        } else {
            targetFile = new ArrayList<File>(0);
            int len = createFile(fileURL);
            if (len > 0) {
                System.out.println("File are created successfully..");
            }
        }
    }

    private int createFile(String[] fileURL) {
        if (isWorkFolderExists() == 0) {
            for (String url : fileURL) {
                File tmpFile = new File(targetDIR+"//"+url);
                if (tmpFile.exists()) {
                    tmpFile.delete();
                } else {
                    try {
                        tmpFile.createNewFile();
                        
                        targetFile.add(tmpFile);
                    } catch (IOException ex) {
                        Logger.getLogger(FileOpeartor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        return targetFile.size();

    }

    private int isWorkFolderExists() {
        File workFolder = new File(targetDIR);
        if (!workFolder.exists()) {
            boolean succ = workFolder.mkdirs();
              System.out.println("mkDIR:"+succ);
            if (succ) {
              
                return 0;
            } else {
                return -1;
            }
        }
        return 0;
    }
}
