/*
SQLyog Enterprise - MySQL GUI v7.02 
MySQL - 5.5.12 : Database - backgroudmanager
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`backgroudmanager` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;

USE `backgroudmanager`;

/*Table structure for table `departments` */

DROP TABLE IF EXISTS `departments`;

CREATE TABLE `departments` (
  `ID` int(8) NOT NULL AUTO_INCREMENT,
  `DEPTID` varchar(13) COLLATE utf8_bin NOT NULL,
  `DEPTNAME` varchar(50) COLLATE utf8_bin NOT NULL,
  `HEADER` varchar(13) COLLATE utf8_bin DEFAULT NULL,
  `PARENTID` varchar(13) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `STATU` int(1) NOT NULL DEFAULT '0',
  `NOTE` text COLLATE utf8_bin,
  PRIMARY KEY (`DEPTID`),
  KEY `ID` (`ID`),
  KEY `IDX_DEPT_DEPTID` (`DEPTID`),
  KEY `IDX_DEPT_HEADER` (`HEADER`),
  KEY `FK_FK_DETP_PARENT` (`PARENTID`),
  CONSTRAINT `FK_DEPARTMENTS_HEADER` FOREIGN KEY (`HEADER`) REFERENCES `sys_users` (`UID`),
  CONSTRAINT `FK_FK_DETP_PARENT` FOREIGN KEY (`PARENTID`) REFERENCES `departments` (`DEPTID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='部门信息表';

/*Data for the table `departments` */

insert  into `departments`(`ID`,`DEPTID`,`DEPTNAME`,`HEADER`,`PARENTID`,`DESCRIPTION`,`STATU`,`NOTE`) values (2,'0000','顶级部门',NULL,NULL,NULL,1,NULL),(1,'0001','开发部',NULL,'0000','测试',1,NULL),(3,'0002','设计部',NULL,'0000','测试',1,NULL),(4,'0003','财务部',NULL,'0000','测试',1,NULL);

/*Table structure for table `functionlists` */

DROP TABLE IF EXISTS `functionlists`;

CREATE TABLE `functionlists` (
  `ID` int(8) NOT NULL AUTO_INCREMENT,
  `FUID` varchar(20) COLLATE utf8_bin NOT NULL,
  `FNAME` varchar(20) COLLATE utf8_bin NOT NULL,
  `FSTAT` int(1) NOT NULL DEFAULT '0',
  `FURI` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `ACCSTR` varchar(32) COLLATE utf8_bin NOT NULL,
  `NOTE` text COLLATE utf8_bin,
  `PARENTID` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`FUID`),
  KEY `ID` (`ID`),
  KEY `IDX_FUNCTIONLIST_FUID` (`FUID`),
  KEY `FK_FUNCTIONLIST_PARENT` (`PARENTID`),
  CONSTRAINT `FK_FUNCTIONLIST_PARENT` FOREIGN KEY (`PARENTID`) REFERENCES `functionlists` (`FUID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='功能菜单表';

/*Data for the table `functionlists` */

insert  into `functionlists`(`ID`,`FUID`,`FNAME`,`FSTAT`,`FURI`,`ACCSTR`,`NOTE`,`PARENTID`) values (8,'10','系统管理',1,NULL,'1000',NULL,NULL),(4,'100','用户管理',0,'userCURD!queryAllUsers.action','10000','     killerWuShangbin\r\n    ','10'),(9,'20','项目管理',1,NULL,'50000',NULL,NULL),(5,'200','角色管理',1,'roleCURD!queryAllRoles.action','20000',NULL,'10'),(6,'300','菜单管理',1,'functionCURD!ListAllFunction.action','30000',NULL,'10'),(7,'400','部门管理',1,'departmentCURD!listDept.action','40000','          暂无数据\r\n    \r\n    ','10'),(10,'500','代码管理',1,'暂无数据','60000','               暂无数据\r\n    \r\n    \r\n    ','20'),(11,'600','文档管理',1,NULL,'70000',NULL,'20'),(12,'700','版本管理',0,'暂无数据','80000','          暂无数据\r\n    \r\n    ','500');

/*Table structure for table `role_acc` */

DROP TABLE IF EXISTS `role_acc`;

CREATE TABLE `role_acc` (
  `ID` int(8) NOT NULL AUTO_INCREMENT,
  `ACCID` varchar(13) COLLATE utf8_bin NOT NULL,
  `RID` varchar(13) COLLATE utf8_bin NOT NULL,
  `FUID` varchar(20) COLLATE utf8_bin NOT NULL,
  `ACCSTR` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `NOTE` text COLLATE utf8_bin,
  PRIMARY KEY (`FUID`,`RID`),
  KEY `ID` (`ID`),
  KEY `IDX_ROLEACC_ACCID` (`ACCID`),
  KEY `FK_ROLE_ACC_ROLW` (`RID`),
  CONSTRAINT `FK_ROLE_ACC_FUNCTION` FOREIGN KEY (`FUID`) REFERENCES `functionlists` (`FUID`),
  CONSTRAINT `FK_ROLE_ACC_ROLW` FOREIGN KEY (`RID`) REFERENCES `sys_role` (`RID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='角色权限表';

/*Data for the table `role_acc` */

insert  into `role_acc`(`ID`,`ACCID`,`RID`,`FUID`,`ACCSTR`,`NOTE`) values (3,'1004','50002','10','1000','test'),(4,'1005','50002','20','1001','test');

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `ID` int(8) NOT NULL AUTO_INCREMENT,
  `RID` varchar(13) COLLATE utf8_bin NOT NULL,
  `RNAME` varchar(20) COLLATE utf8_bin NOT NULL,
  `DEPT` varchar(13) COLLATE utf8_bin DEFAULT NULL,
  `STATU` int(1) NOT NULL DEFAULT '0',
  `NOTE` text COLLATE utf8_bin,
  PRIMARY KEY (`RID`),
  KEY `ID` (`ID`),
  KEY `IDX_ROOM_RID` (`RID`),
  KEY `FK_SYS_ROLE` (`DEPT`),
  CONSTRAINT `FK_SYS_ROLE` FOREIGN KEY (`DEPT`) REFERENCES `departments` (`DEPTID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='角色信息表';

/*Data for the table `sys_role` */

insert  into `sys_role`(`ID`,`RID`,`RNAME`,`DEPT`,`STATU`,`NOTE`) values (8,'50001','总经理','0000',1,'     总经理，公司最高负责人。\r\n    '),(9,'50002','部门经理','0001',1,'软件开发部主管');

/*Table structure for table `sys_users` */

DROP TABLE IF EXISTS `sys_users`;

CREATE TABLE `sys_users` (
  `ID` int(8) NOT NULL AUTO_INCREMENT,
  `UID` varchar(13) COLLATE utf8_bin NOT NULL,
  `UNAME` varchar(8) COLLATE utf8_bin NOT NULL,
  `USEX` char(1) COLLATE utf8_bin NOT NULL DEFAULT 'M',
  `DEPT` varchar(13) COLLATE utf8_bin DEFAULT '',
  `FPHONE` varchar(13) COLLATE utf8_bin NOT NULL,
  `SPHONE` varchar(13) COLLATE utf8_bin DEFAULT NULL,
  `FMOBILE` varchar(13) COLLATE utf8_bin DEFAULT NULL,
  `SMOBILE` varchar(13) COLLATE utf8_bin DEFAULT NULL,
  `FADDRESS` text COLLATE utf8_bin NOT NULL,
  `SADDRESS` text COLLATE utf8_bin,
  `QQ` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `MSN` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `EMAIL` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `STATU` int(1) NOT NULL DEFAULT '1',
  `NOTE` text COLLATE utf8_bin,
  PRIMARY KEY (`UID`),
  KEY `ID` (`ID`),
  KEY `IDX_SYSUSER_UID` (`UID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统用户表';

/*Data for the table `sys_users` */

insert  into `sys_users`(`ID`,`UID`,`UNAME`,`USEX`,`DEPT`,`FPHONE`,`SPHONE`,`FMOBILE`,`SMOBILE`,`FADDRESS`,`SADDRESS`,`QQ`,`MSN`,`EMAIL`,`STATU`,`NOTE`) values (3,'001','Windy','M','0001','15914356304','13927791111','15914356304','暂无数据','广州大学华软软件学院','广州大学华软软件学院','364468269','windyjack@hotmail.com','windyjack@hotmail.com',1,' 广州大学华软软件学院\r\n    \r\n    '),(4,'9879','Jacket','M','0002','020-38446823','020-38446810','13526988189','13526988189','广州海珠','广州天河','87809976','Jacket@hotmail.com','Jacket@hotmail.com',1,'测试');

/*Table structure for table `urole_dept_user` */

DROP TABLE IF EXISTS `urole_dept_user`;

CREATE TABLE `urole_dept_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RID` varchar(13) COLLATE utf8_bin DEFAULT NULL,
  `UID` varchar(13) COLLATE utf8_bin DEFAULT NULL,
  `DEPTID` varchar(13) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_URD_DEPT` (`DEPTID`),
  KEY `FK_URD_ROLE` (`RID`),
  KEY `FK_URD_USER` (`UID`),
  CONSTRAINT `FK_URD_DEPT` FOREIGN KEY (`DEPTID`) REFERENCES `departments` (`DEPTID`),
  CONSTRAINT `FK_URD_ROLE` FOREIGN KEY (`RID`) REFERENCES `sys_role` (`RID`),
  CONSTRAINT `FK_URD_USER` FOREIGN KEY (`UID`) REFERENCES `sys_users` (`UID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `urole_dept_user` */

insert  into `urole_dept_user`(`ID`,`RID`,`UID`,`DEPTID`) values (1,'50002','001','0001');

/*Table structure for table `users_pass` */

DROP TABLE IF EXISTS `users_pass`;

CREATE TABLE `users_pass` (
  `ID` int(8) NOT NULL AUTO_INCREMENT,
  `UID` varchar(13) COLLATE utf8_bin NOT NULL DEFAULT '',
  `UPASS` varchar(32) COLLATE utf8_bin NOT NULL,
  `QUESENT` varchar(100) COLLATE utf8_bin DEFAULT '',
  `ANSWER` varchar(100) COLLATE utf8_bin DEFAULT '',
  `NOTE` text COLLATE utf8_bin,
  PRIMARY KEY (`UID`),
  UNIQUE KEY `IDX_USERPASS_UID` (`UID`),
  UNIQUE KEY `ID` (`ID`),
  CONSTRAINT `FK_users_pass_upass` FOREIGN KEY (`UID`) REFERENCES `sys_users` (`UID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `users_pass` */

insert  into `users_pass`(`ID`,`UID`,`UPASS`,`QUESENT`,`ANSWER`,`NOTE`) values (4,'001','bb9b362064b2aad61e11c18fd8b642cd','1','1','test'),(5,'9879','cf79ae6addba60ad018347359bd144d2',NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
