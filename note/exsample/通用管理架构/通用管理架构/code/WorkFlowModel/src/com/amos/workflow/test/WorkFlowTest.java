package com.amos.workflow.test;

import java.io.File;

import com.amos.workflow.model.Notice;
import com.amos.workflow.model.Recvtor;
import com.amos.workflow.model.Sender;
import com.amos.workflow.model.WorkStream;
import com.amos.workflow.service.WorkFlowService;

public class WorkFlowTest {

	private static long StreamID=0;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Sender sender=new Sender();
		Recvtor recvtor=new Recvtor();
		sender.setUsername("windy Wu");
		sender.setUserRole("user");
		recvtor.setUsername("vinson");
		recvtor.setUserRole("admin");
		
		WorkStream strean=new WorkStream();
		Notice notice=strean.writeNotice("hello world", "task", sender, recvtor,new File("text.txt"));
		WorkFlowService service=new WorkFlowService();
		WorkFlowService.addNotice(notice);
		StreamID++;
		WorkFlowService.addNotice(notice);
		StreamID++;
		WorkFlowService.addNotice(notice);
		StreamID++;
		WorkFlowService.addNotice(notice);
		StreamID++;
		WorkFlowService.addNotice(notice);
		StreamID++;
		System.out.println(WorkFlowService.getNoticeList().size());
		WorkFlowService.readNotice(StreamID);
		WorkFlowService.moveNotice(notice);
		StreamID--;
		System.out.println(WorkFlowService.getNoticeList().size());
		WorkFlowService.readNotice(StreamID);
		WorkFlowService.moveNotice(notice);
	}

}
