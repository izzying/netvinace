package com.amos.workflow.service;

import java.util.Map;
import java.util.TreeMap;

import com.amos.workflow.model.Notice;

public class WorkFlowService {
	
	private static Map<Long,Notice> noticeList=new TreeMap<Long,Notice>();
	
	private static long noticeNum=0;
	
	public static Map<Long, Notice> getNoticeList() {
		return noticeList;
	}

	public static void setNoticeList(Map<Long, Notice> notice) {
		WorkFlowService.noticeList = noticeList;
	}
	
	
	public static boolean moveNotice(Notice notice)
	{
		try{
			
			noticeList.remove(notice.getStreamID());
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
		return false;
		}
	}
	
	
	public static boolean addNotice(Notice notice)
	{
		try{
			noticeNum++;
			notice.setStreamID(noticeNum);
			noticeList.put(noticeNum, notice);
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
		return false;
		}
	}

	public static boolean readNotice(long streamID){
		
		Notice notice=noticeList.get(streamID);
		System.out.println("--------------开始读取工作流-------------------");
		System.out.println(notice.getSender().getUsername());
		System.out.println(notice.getSendDate());
		System.out.println(notice.getText());
		
		System.out.println(notice.getNote());
		System.out.println(notice.getRecvtor().getUsername());
		System.out.println(notice.getRecvtor().getUserRole());
		System.out.println(notice.getReadDate()==null?"":notice.getReadDate().toString());
		System.out.println("--------------读取工作流结束-------------------");
		return true;
	}
	
	public static boolean sendNotice(Notice notice){
		boolean sign=false;
		try{
			sign=WorkFlowService.addNotice(notice);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return sign;
	}
	
	
	
	
	
	
	
}
