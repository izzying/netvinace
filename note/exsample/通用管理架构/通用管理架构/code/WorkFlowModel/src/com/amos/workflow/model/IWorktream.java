package com.amos.workflow.model;

import java.io.File;
/**
 * 
 * @author Administrator
 * @version 1.0.0
 *
 */
public interface IWorktream {
  public INotice	writeNotice(String work,String note,  Sender sender, Recvtor recvtor,File file);
  public INotice	readNotice(INotice notice);
  
}
