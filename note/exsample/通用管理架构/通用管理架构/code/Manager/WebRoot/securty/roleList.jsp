<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>角色管理</title>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.STYLE1 {font-size: 12px}
.STYLE3 {font-size: 12px; font-weight: bold; }
.STYLE4 {
	color: #03515d;
	font-size: 12px;
}
-->
</style>

<script>
var  highlightcolor='#c1ebff';
//此处clickcolor只能用win系统颜色代码才能成功,如果用#xxxxxx的代码就不行,还没搞清楚为什么:(
var  clickcolor='#51b2f6';
function  changeto(){
source=event.srcElement;
if  (source.tagName=="TR"||source.tagName=="TABLE")
return;
while(source.tagName!="TD")
source=source.parentElement;
source=source.parentElement;
cs  =  source.children;
//alert(cs.length);
if  (cs[1].style.backgroundColor!=highlightcolor&&source.id!="nc"&&cs[1].style.backgroundColor!=clickcolor)
for(i=0;i<cs.length;i++){
	cs[i].style.backgroundColor=highlightcolor;
}
}

function  changeback(){
if  (event.fromElement.contains(event.toElement)||source.contains(event.toElement)||source.id=="nc")
return
if  (event.toElement!=source&&cs[1].style.backgroundColor!=clickcolor)
//source.style.backgroundColor=originalcolor
for(i=0;i<cs.length;i++){
	cs[i].style.backgroundColor="";
}
}

function  clickto(){
source=event.srcElement;
if  (source.tagName=="TR"||source.tagName=="TABLE")
return;
while(source.tagName!="TD")
source=source.parentElement;
source=source.parentElement;
cs  =  source.children;
//alert(cs.length);
if  (cs[1].style.backgroundColor!=clickcolor&&source.id!="nc")
for(i=0;i<cs.length;i++){
	cs[i].style.backgroundColor=clickcolor;
}
else
for(i=0;i<cs.length;i++){
	cs[i].style.backgroundColor="";
}
}


//页面复选框全选中/反选函数
function selectAll(checked){

   var selData=document.getElementsByName("chkData"); 
	   for(i=0;i<selData.length;i++)
	   {
	        selData[i].checked=checked;
	   }
}
//删除页面选中记录
function deleteSelected()
{
   var sign=window.confirm("记录将永久删除，无法恢复，真的要删除记录吗？");
   if(sign==true){
   var selData=document.getElementsByName("chkData"); 
   var dataArr=new Array();
   
	   for(i=0;i<selData.length;i++)
	   {
	        if(selData[i].checked==true)
	        {
	             dataArr[dataArr.length]=selData[i].value;
	        }
	        else{
	           continue;
	        }
	   }
	   
	  window.location="<%=path%>/roleCURD!delRoles.action?roleId="+dataArr+"";
	  
	  }else{
	  
	      return;
	  }	  
}

function deleteSingleData(roleId){

    var sign=window.confirm("记录将永久删除，无法恢复，真的要删除记录吗？");
   if(sign==true){
   window.location="<%=path%>/roleCURD!delRole.action?roleId="+roleId+"";
   }
   else{
      return;
   }
}


</script>

</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="30" background="<%=path%>/securty/images/tab_05.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="12" height="30"><img src="<%=path%>/securty/images/tab_03.gif" width="12" height="30" /></td>
        
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="46%" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="5%"><div align="center"><img src="<%=path%>/securty/images/tb.gif" width="16" height="16" /></div></td>
                <td width="95%" class="STYLE1"><span class="STYLE3">你当前的位置</span>：[系统管理]-[角色管理]</td>
              </tr>
            </table></td>
            <td width="54%" colspan="2"><table border="0" align="right" cellpadding="0" cellspacing="0">
              <tr>
                <td width="60"><table width="90%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="STYLE1"><div align="center"><img src="<%=path%>/securty/images/22.gif" width="14" height="14" /></div></td>
                    <td class="STYLE1"><div align="center"><a href="roleCURD!newRole.action">增加</a></div></td>
                  </tr>
                </table></td>
                <%--<td width="60"><table width="90%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="STYLE1"><div align="center"><img src="<%=path%>/securty/images/33.gif" width="14" height="14" /></div></td>
                    <td class="STYLE1"><div align="center">修改</div></td>
                  </tr>
                </table></td>
                --%><td width="52"><table width="88%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="STYLE1"><div align="center"><img src="<%=path%>/securty/images/11.gif" width="14" height="14" /></div></td>
                    <td class="STYLE1"><div align="center"><a href="javascript:deleteSelected();">删除</a></div></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
          </tr>
          <tr><td width="46%" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            
            </table></td>
           
          </tr>
        </table></td>
        <td width="16"><img src="<%=path%>/securty/images/tab_07.gif" width="16" height="30" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="8" background="<%=path%>/securty/images/tab_12.gif">&nbsp;</td>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="b5d6e6" onmouseover="changeto()"  onmouseout="changeback()">
          <tr>
            <td width="4%" height="22" background="<%=path%>/securty/images/bg.gif" bgcolor="#FFFFFF"><div align="center">
              <input type="checkbox"  name="checkbox"  onclick="selectAll(this.checked);" />
            </div></td>
            <td width="9%" height="22" background="<%=path%>/securty/images/bg.gif" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1">角色名</span></div></td>
<%--            <td width="6%" height="22" background="<%=path%>/securty/images/bg.gif" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1">性别</span></div></td>--%>
             <td width="10%" height="22" background="<%=path%>/securty/images/bg.gif" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1">部门</span></div></td>
<%--            <td width="20%" height="22" background="<%=path%>/securty/images/bg.gif" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1">联系电话</span></div></td>--%>
<%--            <td width="16%" background="<%=path%>/securty/images/bg.gif" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1">手机</span></div></td>--%>
            <td width="22%" height="22" background="<%=path%>/securty/images/bg.gif" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1">备注</span></div></td>
            <td width="13%" height="22" background="<%=path%>/securty/images/bg.gif" bgcolor="#FFFFFF" class="STYLE1"><div align="center">操作</div></td>
          </tr>
         <s:iterator value="#request.currentPage.result">
          <tr>
            <td height="20" bgcolor="#FFFFFF"><div align="center">
              <input type="checkbox" name="chkData"  value="<s:property value="rid"/>" />
            </div></td>
            <td height="20" bgcolor="#FFFFFF"><div align="center">
              <span class="STYLE1"><s:property value="rname" /></span>
            </div></td>
<%--            <td height="20" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1"><s:if test='usex=="M"'>男</s:if><s:else>女</s:else></span></div></td>--%>
             <td height="20" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1"><s:property value="departments.deptname" /></span></div></td>
<%--            <td height="20" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1"><s:if test='fphone==null'>暂无数据</s:if><s:else><s:property value="fphone" /></s:else>  </span></div></td>--%>
<%--            <td bgcolor="#FFFFFF"><div align="center"><span class="STYLE1"><s:if test='fmobile==null'>暂无数据</s:if><s:else><s:property value="fmobile" /></s:else> </span></div></td>--%>
            <td height="20" bgcolor="#FFFFFF"><div align="center"><span class="STYLE1"><s:if test='note==null'>暂无数据</s:if><s:else><s:property value="note" /></s:else>  </span></div></td>
            <td height="20" bgcolor="#FFFFFF"><div align="center"><span class="STYLE4"><img src="<%=path%>/securty/images/edt.gif" width="16" height="16" /><a href="roleCURD!getRolesDetail.action?roleID=<s:property value="rid" />">编辑</a>&nbsp; &nbsp;<img src="<%=path%>/securty/images/del.gif" width="16" height="16" /><a href="javascript:deleteSingleData(<s:property value="rid"/>);">删除</a></span></div></td>
          </tr>
          </s:iterator>
       
          
          
        
        
        </table></td>
        <td width="8" background="<%=path%>/securty/images/tab_15.gif">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="35" background="<%=path%>/securty/images/tab_19.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="12" height="35"><img src="<%=path%>/securty/images/tab_18.gif" width="12" height="35" /></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="STYLE4">&nbsp;&nbsp;共有 <s:property value="#request.currentPage.resultTotal" />条记录，当前第  <s:property value="#request.currentPage.pageIndex" />/<s:property value="#request.currentPage.pageTotal" /> 页</td>
            <td><table border="0" align="right" cellpadding="0" cellspacing="0">                                                                                                    
                <tr>
                  <td width="40"><a href="roleCURD!queryAllRoles.action?pageIndex=1"><img src="<%=path%>/securty/images/first.gif" width="37" height="15" /></a></td>
                  <td width="45"><a href="roleCURD!queryAllRoles.action?pageIndex=<s:if test="#request.currentPage.pageIndex<=1">1</s:if><s:else><s:property value="#request.currentPage.pageIndex-1"/></s:else>&pageSize=<s:property value="#request.currentPage.pageSize"/>"><img src="<%=path%>/securty/images/back.gif" width="43" height="15" /></a></td>
                  <td width="45"><a href="roleCURD!queryAllRoles.action?pageIndex=<s:if test="#request.currentPage.pageIndex<#request.currentPage.pageTotal"><s:property value="#request.currentPage.pageIndex+1"/></s:if><s:else><s:property value="#request.currentPage.pageTotal"/></s:else>&pageSize=<s:property value="#request.currentPage.pageSize"/>"><img src="<%=path%>/securty/images/next.gif" width="43" height="15" /></a></td>
                  <td width="40"><a href="roleCURD!queryAllRoles.action?pageIndex=<s:property value="#request.currentPage.pageTotal"/>"><img src="<%=path%>/securty/images/last.gif" width="37" height="15" /></a></td>
                  <td width="100"><div align="center"><span class="STYLE1">转到第
                    <input name="textfield" type="text" size="4" style="height:12px; width:20px; border:1px solid #999999;" /> 
                    页 </span></div></td>
                  <td width="40"><img src="<%=path%>/securty/images/go.gif" width="37" height="15" /></td>
                </tr>
            </table></td>
          </tr>
        </table></td>
        <td width="16"><img src="<%=path%>/securty/images/tab_20.gif" width="16" height="35" /></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
