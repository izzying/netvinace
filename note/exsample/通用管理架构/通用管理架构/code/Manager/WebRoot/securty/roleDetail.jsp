<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.STYLE1 {font-size: 12px}
.STYLE3 {font-size: 12px; font-weight: bold; }
.STYLE4 {
	color: #03515d;
	font-size: 12px;
}
-->
</style>

<script>
var  highlightcolor='#c1ebff';
//此处clickcolor只能用win系统颜色代码才能成功,如果用#xxxxxx的代码就不行,还没搞清楚为什么:(
var  clickcolor='#51b2f6';
function  changeto(){
source=event.srcElement;
if  (source.tagName=="TR"||source.tagName=="TABLE")
return;
while(source.tagName!="TD")
source=source.parentElement;
source=source.parentElement;
cs  =  source.children;
//alert(cs.length);
if  (cs[1].style.backgroundColor!=highlightcolor&&source.id!="nc"&&cs[1].style.backgroundColor!=clickcolor)
for(i=0;i<cs.length;i++){
	cs[i].style.backgroundColor=highlightcolor;
}
}

function  changeback(){
if  (event.fromElement.contains(event.toElement)||source.contains(event.toElement)||source.id=="nc")
return
if  (event.toElement!=source&&cs[1].style.backgroundColor!=clickcolor)
//source.style.backgroundColor=originalcolor
for(i=0;i<cs.length;i++){
	cs[i].style.backgroundColor="";
}
}

function  clickto(){
source=event.srcElement;
if  (source.tagName=="TR"||source.tagName=="TABLE")
return;
while(source.tagName!="TD")
source=source.parentElement;
source=source.parentElement;
cs  =  source.children;
//alert(cs.length);
if  (cs[1].style.backgroundColor!=clickcolor&&source.id!="nc")
for(i=0;i<cs.length;i++){
	cs[i].style.backgroundColor=clickcolor;
}
else
for(i=0;i<cs.length;i++){
	cs[i].style.backgroundColor="";
}
}
//更新角色信息表单提交函数
function updateRole()
{
     var frm=document.getElementById("roleInfor");
     var pass=chkFrm();
    
     if(pass==true){
    	 frm.action="roleCURD!updateRole.action";
    	 frm.submit();
	}
	else{
	   	 return;
	}
}	
//表单验证函数
function chkFrm()
{
  var roleId=document.getElementById("roleId").value;
  var roleName=document.getElementById("roleName").value;
  
   
  if(roleId==""){
      alert("角色编号不能为空");
      return false;
  }
  if(roleName=="")
  {
   alert("角色名称不能为空");
      return false;
  }
 
  return true
}


</script>

</head>

<body leftmargin="0" rightmargin="0" bottommargin="0" topmargin="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="30" background="<%=path%>/securty/images/tab_05.gif"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="12" height="30"><img src="<%=path%>/securty/images/tab_03.gif" width="12" height="30" /></td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="46%" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="5%"><div align="center"><img src="<%=path%>/securty/images/tb.gif" width="16" height="16" /></div></td>
                <td width="95%" class="STYLE1"><span class="STYLE3">你当前的位置</span>：[系统管理]-[角色管理]</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
        <td width="12"><img src="<%=path%>/securty/images/tab_07.gif" width="12" height="30" /></td>
      </tr>
    </table></td>
  </tr>
  
   <tr>
    <td height="30"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td background="<%=path%>/securty/images/bg04.gif"  style="background-repeat:repeat-y" width="12">e</td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr><td align="center">
          <form  name="roleInfor"  method="post" id="roleInfor">
          <table width="600" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="97" align="right"><span style="font-size:12px;">角色编号：</span></td>
    <td width="497" align="left"><input name="roleId"  readonly id="roleId"  value="<s:property value="roleId"/>"  style="border-style:solid; border-width:1px; border-color:#CCCCCC" type="text" /></td>
  </tr>
  <tr>
    <td align="right"><span style="font-size:12px">角色名称：</span></td>
    <td  align="left"><input name="roleName"  id="roleName" type="text" value="<s:property value="roleName"/>"  style="border-style:solid; border-width:1px; border-color:#CCCCCC" /></td>
  </tr>
  
   <tr>
    <td align="right"><span style="font-size:12px">所属部门：</span></td>
    <td align="left">
    <select name="roleDept" id="roleDept"  style="width:170px">
    <option value="<s:property value="deptId" />" selectd><s:property value="roleDept" /></option>
         <s:iterator value="#request.deptList">
    <option value="<s:property value="deptid" />"><s:property value="deptname" /></option>
    </s:iterator>
    </select></td>
  </tr>
 
  <tr>
    <td align="right"><span style="font-size:12px">备注：</span></td>
    <td  align="left">
    <textarea name="roleNote" id="roleNote" style="border-style:solid; border-width:1px; border-color:#CCCCCC" cols="50" rows="3">
     <s:property value="roleNote" default="暂无数据" />
    </textarea></td>
  </tr>
  <tr>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="29%">&nbsp;</td>
        <td width="21%" align="center"><input type="button" onclick="updateRole();" value="更新" /></td>
        <td width="19%" align="center"><input type="button" onclick="resetFrm();" value="重置" /></td>
        <td width="31%">&nbsp;</td>
      </tr>
    </table></td>
    </tr>
</table>
</form>
</table>
</body>
</html>
