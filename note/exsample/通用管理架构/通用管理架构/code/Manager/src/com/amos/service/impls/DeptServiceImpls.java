package com.amos.service.impls;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.interceptor.TransactionProxyFactoryBean;

import com.amos.dao.CommonDao;
import com.amos.entry.Departments;
import com.amos.entry.SysUsers;
import com.amos.service.itf.IDeptService;
import com.amos.service.itf.IUserService;
import com.amos.utils.IEntry;

public class DeptServiceImpls implements IDeptService {

	private CommonDao dao;

	private IUserService userService;

	public IUserService getUserService() {
		return userService;
	}

	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	public CommonDao getDao() {
		return dao;
	}

	public void setDao(CommonDao dao) {
		this.dao = dao;
	}

	public Departments getDept(String deptID) {
		Departments dept = (Departments) dao.querySingleData(Departments.class,
				deptID);
		return dept;
	}

	public int updateDept(Departments dept) {

		return 0;

	}

	public List<Departments> listDept() {

		List<Departments> deptList = new ArrayList<Departments>();

		String condition = " where a.statu=1";
		List<IEntry> resultList = dao.queryDataByCondition(condition,
				Departments.class,true);
		for (IEntry entry : resultList) {
			Departments dept = (Departments) entry;
			deptList.add(dept);
		}

		return deptList;
	}

	public List<Departments> listHistoryDept() {

		List<Departments> deptList = new ArrayList<Departments>();
		List<IEntry> resultList = dao.queryAllData(Departments.class);

		for (IEntry entry : resultList) {
			Departments dept = (Departments) entry;
			deptList.add(dept);
		}

		return deptList;
	}

	public int addDept(Departments dept) {
		int sign = dao.saveData(dept);
		return sign;
	}

	public int delDept(String deptID) {
		Departments dept = (Departments) dao.querySingleData(Departments.class,
				deptID);
		if (dept != null) {
			dept.setStatu(-1);
			int sign = dao.updateData(dept);
			
			return sign;
		} else {
			return 0;
		}
	}

	public Departments getDeptByName(String deptName) {
		String condition=" where a.deptName='"+deptName+"'";
		List<IEntry> resutList=dao.queryDataByCondition(condition, Departments.class,true);
		if(resutList.size()==1){
		Departments dept=(Departments) resutList.get(0);
		return dept;
		}
		else{
		return null;
		}
	}

}
