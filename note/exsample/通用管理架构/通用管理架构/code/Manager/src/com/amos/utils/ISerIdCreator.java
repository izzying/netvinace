package com.amos.utils;

public interface ISerIdCreator {

	public String buildBussinessID(String preFix);
	
	public boolean configBussinessID(String preFix,long maxNum,long strartNum);
	
	
}
