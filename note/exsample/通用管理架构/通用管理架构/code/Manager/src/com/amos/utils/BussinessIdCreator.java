package com.amos.utils;
/**
 * 
 * @author Administrator
 * @category 业务编号生成类
 * @version 1.0.0
 *
 */
public class BussinessIdCreator implements ISerIdCreator {

	private static  String preFix;
	
	private static long maxNum;
	
	private static long strartNum;
	
	public String buildBussinessID(String preFix) {
		this.setPreFix(preFix);
		
		return null;
	}

	public boolean configBussinessID(String preFix, long maxNum, long strartNum) {
		// TODO Auto-generated method stub
		return false;
	}

	public String getPreFix() {
		return preFix;
	}

	public void setPreFix(String preFix) {
		this.preFix = preFix;
	}

	public long getMaxNum() {
		return maxNum;
	}

	public void setMaxNum(long maxNum) {
		this.maxNum = maxNum;
	}

	public long getStrartNum() {
		return strartNum;
	}

	public void setStrartNum(long strartNum) {
		this.strartNum = strartNum;
	}

}
