package com.amos.service.impls;

import java.util.List;

import com.amos.dao.CommonDao;
import com.amos.entry.Departments;
import com.amos.entry.SysRole;
import com.amos.service.itf.IDeptService;
import com.amos.service.itf.IRoleService;
import com.amos.utils.PageUtil;
import com.amos.vo.SysRoleVO;

public class RoleServiceImpls implements IRoleService {

	private CommonDao dao;
	private IDeptService deptService;
	
	public CommonDao getDao() {
		return dao;
	}

	public void setDao(CommonDao dao) {
		this.dao = dao;
	}

	public boolean addRole(SysRoleVO roleVO) {
		SysRole role=new SysRole();
		role.setRid(roleVO.getRoleId());
		role.setRname(roleVO.getRoleName());
		role.setStatu(roleVO.getStatu());
		role.setNote(roleVO.getNote());
		Departments dept=deptService.getDept(roleVO.getDeptId());
		int sign=0;
		if(dept!=null){
		role.setDepartments(dept);
		sign=dao.saveData(role);
		if(sign==1){
			return true;
		}
		else{
			return false;
		}
		}
		else{
			return false;
		}
		
	}

	public boolean delRole(String roleId) {
		int sign=dao.deleteData(SysRole.class, roleId);
		if(sign!=1){
			return false;
		}
		else{
			return true;
		}
	}

	public int delRoles(String[] roleIdArr) {
		int sign=dao.deleteDatas(SysRole.class, roleIdArr);
		
		return sign;
	}

	public SysRole getRole(String roleID) {
		SysRole role=(SysRole)dao.querySingleData(SysRole.class, roleID);
		return role;
	}

	public List<SysRole> listRole() {
		// TODO Auto-generated method stub
		return null;
	}

	public PageUtil listRolePage(int pageNo, int pageSize) {
		PageUtil currentPage=dao.queryAllData(SysRole.class, pageNo, pageSize);
		return currentPage;
	}

	public boolean updateRole(SysRoleVO roleVo, String roleId) {
		SysRole result= (SysRole)dao.querySingleData(SysRole.class, roleId);
		Departments dept=(Departments) dao.querySingleData(Departments.class, roleVo.getDeptId());
		if(dept!=null){
			
			result.setRname(roleVo.getRoleName());
			result.setDepartments(dept);
			result.setNote(roleVo.getNote());
			result.setStatu(roleVo.getStatu());
			int sign=dao.updateData(result);
			if(sign==1)
			{
				return true;
			}
			else{
				return false;
			}
	
		}
		else{
			return false;
			
		}
		
		
	}

	public IDeptService getDeptService() {
		return deptService;
	}

	public void setDeptService(IDeptService deptService) {
		this.deptService = deptService;
	}

}
