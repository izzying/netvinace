package com.amos.web;

import java.util.List;

import org.apache.struts2.ServletActionContext;

import com.amos.entry.Departments;
import com.amos.entry.SysUsers;
import com.amos.service.itf.IDeptService;
import com.amos.service.itf.IUserService;
import com.opensymphony.xwork2.ActionSupport;

public class DepartmentsAction extends ActionSupport {

	private String deptID;
	private String deptName;
	private String description;
	private String statu;
	private String note;
	private String headerName;
	
	private IDeptService deptService;
	private IUserService userService;
	
	private List<Departments> deptList;
	
	
	
	public String listDept()throws Exception{
		
		deptList=deptService.listDept();
		
		return "list";
	}
	
	public String listHistoryDept()throws Exception{
		
		deptList=deptService.listHistoryDept();
		
		return "list";
	}
	
	
	public String getDeptEmpoyees()throws Exception{
		
	//	userService.listDeptEmpoyee(deptID);
		
		return "listEopyee";
		
		
	}
	
	public String getDeptDetail()throws Exception{
//		deptID=ServletActionContext.getRequest().getParameter("deptID");
//		if(deptID!=null||!"".equals(deptID)){
//			Departments deptDetail=deptService.getDept(deptID);
//			this.deptID=deptDetail.getDeptID();
//			this.deptName=deptDetail.getDeptName();
//			this.description=deptDetail.getDescription();
//			if(deptDetail.getHeader()!=null){
//			this.headerName=deptDetail.getHeader().getUname();
//			
//			}
//			else{
//				this.headerName="";
//			}
//			if(deptDetail.getStatu()==1){
//				this.statu="未成立";
//			}
//			else{
//				this.statu="成立";
//			}
//			this.note=deptDetail.getNote();
			return "detail";
//		}
//		else{
//			return "unDetail";
//		}
		
		
	}
    
	public String updateDEPT()throws Exception{
		
//		Departments dept=new Departments();
//		dept.setDeptID(deptID);
//		dept.setDeptName(deptName);
//		dept.setDescription(description);
//		dept.setNote(note);
//		if("成立".equals(statu)){
//			dept.setStatu(1);
//		}
//		else{
//			dept.setStatu(0);
//		}
//		System.out.println("============Action============");
//		System.out.println(this.headerName);
//		System.out.println("============Action============");
//		dept.setHeaderName(headerName);
		
//		int sign=deptService.updateDept(dept);
//		
//		if(sign==1){
			return "update";
//		}
//		else{
//			return "unUpdate";
//		}
	}
	
	// Getter/Setter Methods
	
	public String getDeptID() {
		return deptID;
	}

	public void setDeptID(String deptID) {
		this.deptID = deptID;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatu() {
		return statu;
	}

	public void setStatu(String statu) {
		this.statu = statu;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getHeaderName() {
		return headerName;
	}

	public void setHeaderName(String headerName) {
		this.headerName = headerName;
	}

	public IDeptService getDeptService() {
		return deptService;
	}

	public void setDeptService(IDeptService deptService) {
		this.deptService = deptService;
	}

	public List<Departments> getDeptList() {
		return deptList;
	}

	public void setDeptList(List<Departments> deptList) {
		this.deptList = deptList;
	}

	public IUserService getUserService() {
		return userService;
	}

	public void setUserService(IUserService userService) {
		this.userService = userService;
	}
	
	
	
	
	
}
