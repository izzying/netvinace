package com.amos.utils;

import java.util.ArrayList;
import java.util.List;
/**
 * 
 * @author Administrator
 * @category ������ҳʵ��
 *
 */
public class PageUtil {

	private int pageTotal=1; //总页数��
	public static int pageSize=10;  //每页显示数��¼��
	private int pageIndex=1;  // ��ǰ当前页页码��
	private long resultTotal=0; //  总记录数�ܼ�¼��
	private List<IEntry> result=new ArrayList<IEntry>(0);  //当前页面显示记录集��¼��ʵ��
	
	
	
	
	public PageUtil() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PageUtil(int pageNo, int pageSize, List<IEntry> resultList,int resultTotal) {
		this.result=resultList;
		this.pageSize=pageSize;
		this.pageIndex=pageNo;
		this.resultTotal=resultTotal;
		if(resultTotal%pageSize==0){
			this.pageTotal=resultTotal/pageSize;
		}
		else{
			this.pageTotal=resultTotal/pageSize+1;
		}
	}
	public int getPageTotal() {
		return pageTotal;
	}
	public void setPageTotal(int pageTotal) {
		this.pageTotal = pageTotal;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPageIndex() {
		return pageIndex;
	}
	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}
	public long getResultTotal() {
		return resultTotal;
	}
	public void setResultTotal(long resultTotal) {
		this.resultTotal = resultTotal;
	}
	public List<IEntry> getResult() {
		return result;
	}
	public void setResult(List<IEntry> result) {
		this.result = result;
	}
	
}
