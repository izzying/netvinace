package com.amos.web;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.amos.entry.Departments;
import com.amos.entry.SysRole;
import com.amos.entry.SysUsers;
import com.amos.service.itf.IDeptService;
import com.amos.service.itf.IRoleService;
import com.amos.utils.PageUtil;
import com.amos.vo.SysRoleVO;
import com.amos.vo.SysUsersVO;
import com.opensymphony.xwork2.ActionSupport;

public class RoleAction extends ActionSupport {

	private IDeptService deptService;
	private IRoleService roleService;
	
	private String roleId;
	private String deptId;
	private String roleDept;
	private String roleName;
	private Integer statu=1;;
	private String roleNote;

	
	
	public String newRole()throws Exception{
		
		HttpServletRequest request=ServletActionContext.getRequest();
		List<Departments> deptList=deptService.listDept();
		request.setAttribute("deptList", deptList);
		return "new";
	}
	
	public String addRole()throws Exception{
		HttpServletResponse response=ServletActionContext.getResponse();
		SysRoleVO roleVO=new SysRoleVO(roleId,deptId,roleName,statu,roleNote);
	
		boolean sign=roleService.addRole(roleVO);
		PrintWriter out=response.getWriter();
		if(sign==true){
			out.println("<script>alert('添加成功');window.location='roleCURD!queryAllRoles.action';</script>");
			
		}
		else{
			out.println("<script>alert('添加失败');window.location='roleCURD!queryAllRoles.action';</script>");
			
		}
		return null;
	}
	
	public String queryAllRoles()throws Exception{

		HttpServletRequest request=ServletActionContext.getRequest();
		

		int pageIndex=request.getParameter("pageIndex")==null?1:Integer.parseInt(request.getParameter("pageIndex").trim().toString());
		int pageSize=request.getParameter("pageSize")==null?PageUtil.pageSize:Integer.parseInt(request.getParameter("pageSize").trim().toString());
		
			
			if(pageIndex==0||pageIndex<1){
				pageIndex=1;
			}
		PageUtil currentPage=new PageUtil();

		currentPage=roleService.listRolePage(pageIndex, pageSize);

		request.setAttribute("currentPage", currentPage);
		return "list";
	}
	
	
	public String delRoles()throws Exception{
		HttpServletRequest request=ServletActionContext.getRequest();
		HttpServletResponse response=ServletActionContext.getResponse();
		String roleIds=request.getParameter("roleId").toString();
		String [] roleIdArr=new String[PageUtil.pageSize];
		System.out.println("roleIds--------------------"+roleIds);
		if(roleIds.indexOf(",")!=-1){
			roleIdArr=roleIds.split(",");
		}
		else{
			roleIdArr[0]=roleIds;
		}
		int sign=roleService.delRoles(roleIdArr);
		PrintWriter out=response.getWriter();
		if(sign>=1){
			out.println("<script>alert('删除成功');window.location='roleCURD!queryAllRoles.action';</script>");
			
		}
		else{
			out.println("<script>alert('删除失败');window.location='roleCURD!queryAllRoles.action';</script>");
			
		
		}
		return null;
		
	}
	
	public String  delRole()throws Exception{
		HttpServletRequest request=ServletActionContext.getRequest();
		HttpServletResponse response=ServletActionContext.getResponse();
		String roleId=request.getParameter("roleId").toString();
		System.out.println("roleId-----------"+roleId);
		//return null;
		boolean sign=roleService.delRole(roleId);
		PrintWriter out=response.getWriter();
		if(sign==true){
			out.println("<script>alert('删除成功');window.location='roleCURD!queryAllRoles.action';</script>");
			
		}
		else{
			out.println("<script>alert('删除失败');window.location='roleCURD!queryAllRoles.action';</script>");
			
		
		}
		return null;
		
	}
	
	public String getRolesDetail()throws Exception{
		HttpServletRequest request=ServletActionContext.getRequest();
		String roleID =request.getParameter("roleID");

		SysRole role = roleService.getRole(roleID);
		List<Departments> deptList=deptService.listDept();
		
		for(Departments dept:deptList){
		    if(dept.getDeptid().equals(role.getDepartments().getDeptid())){
		    	deptList.remove(deptList.indexOf(dept));
		    	break;
		    }
		    else{
		    	continue;
		    }
		}
	if (role != null) {
			this.roleId = role.getRid();
			this.roleName = role.getRname();
			this.roleDept=role.getDepartments().getDeptname();
			this.deptId=role.getDepartments().getDeptid();
			this.roleNote = role.getNote();
			
			request.setAttribute("deptList", deptList);
			return "detail";
		} else {
			return "unDetail";
		}
		
		
	}
	
	
	public String updateRole()throws Exception{
		HttpServletResponse response=ServletActionContext.getResponse();
		SysRoleVO roleVo=new SysRoleVO(roleId, deptId, roleName,
				 statu, roleNote);
		boolean sign=roleService.updateRole(roleVo, roleId);
		PrintWriter out=response.getWriter();
		if(sign==true){
			out.println("<script>alert('更新成功');window.location='roleCURD!queryAllRoles.action';</script>");
			
		}
		else{
			out.println("<script>alert('更新失败');window.location='roleCURD!queryAllRoles.action';</script>");
			
		
		}
		return null;
	}
	
	
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	
	public String getRoleDept() {
		return roleDept;
	}

	public void setRoleDept(String roleDept) {
		this.roleDept = roleDept;
	}

	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Integer getStatu() {
		return statu;
	}
	public void setStatu(Integer statu) {
		this.statu = statu;
	}
	

	public IRoleService getRoleService() {
		return roleService;
	}

	public void setRoleService(IRoleService roleService) {
		this.roleService = roleService;
	}

	public IDeptService getDeptService() {
		return deptService;
	}

	public void setDeptService(IDeptService deptService) {
		this.deptService = deptService;
	}

	public String getRoleNote() {
		return roleNote;
	}

	public void setRoleNote(String roleNote) {
		this.roleNote = roleNote;
	}
}
