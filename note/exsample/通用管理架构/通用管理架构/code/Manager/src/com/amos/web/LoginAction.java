package com.amos.web;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.struts2.ServletActionContext;

import com.amos.dao.CommonDao;
import com.amos.entry.Function;
import com.amos.entry.SysRole;
import com.amos.service.itf.ILoginService;
import com.amos.utils.IEntry;
import com.amos.utils.LoginUser;
import com.amos.utils.SysMessage;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
/*
 * 描述：系统登录组件
 * 作者：伍尚彬
 * 版本：测试版
 * 创建日期：2010-08-22
 * 修改日期：2010-08-22
 */
public class LoginAction extends ActionSupport {

	private String username; //用户名
	
	private String password; //密码
	
	private String clientIP;//用户IP
	
	private String loginDate;//登录时间字符串
	
	private ILoginService loginSrv;//登录业务组件接口
	
/*
 * 方法：login()
 * 描述：用于系统登录业务
 * 参数：用户名，密码
 * 参数来源：页面
 */
	
	
	
	public String login()throws Exception{
		
		LoginUser loginUser=loginSrv.login(username, password);
			
		if(loginUser!=null&&loginUser.getLoginStatu().equals("LOGIN_SUCCESS")){
			
			clientIP=ServletActionContext.getRequest().getRemoteAddr();
			
			loginDate=loginSrv.catchUserLoginDate(new Date());
			
			loginUser.setLoginIP(clientIP);
			
			loginUser.setLoginTime(loginDate);
			
			Map<Function,Set<Function>> sysMenun=loginUser.getSysMenun();
			
			ServletActionContext.getRequest().getSession().setAttribute("loginUser",loginUser);
			
		}
			return loginUser.getLoginStatu();
	}
	
	
	

/*
 *Getter/Setter Mehtods 
 */	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getClientIP() {
		return clientIP;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	public String getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(String loginDate) {
		this.loginDate = loginDate;
	}

	public ILoginService getLoginSrv() {
		return loginSrv;
	}

	public void setLoginSrv(ILoginService loginSrv) {
		this.loginSrv = loginSrv;
	}


	
	
}
