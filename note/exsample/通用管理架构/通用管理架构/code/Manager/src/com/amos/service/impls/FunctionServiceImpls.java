package com.amos.service.impls;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.amos.dao.CommonDao;
import com.amos.entry.Function;
import com.amos.entry.SysUsers;
import com.amos.service.itf.IFunctionService;
import com.amos.utils.IEntry;
import com.amos.vo.FunctionVO;

public  class FunctionServiceImpls implements IFunctionService {

	private CommonDao dao;
	
	public List<Function> ListFunctions(String funId){
		 List<Function> functionList=new ArrayList<Function>();
		 String hql=null;
		 
		if("".equals(funId)||funId==null){
			hql=" where a.parent=null";
		}
		else{
			 hql=" where a.parent.fuid='"+funId+"'";
		}
		 List<IEntry>  resultList=dao.queryDataByCondition(hql, Function.class, false);
		for(IEntry result:resultList){
			Function function=(Function) result;
			functionList.add(function);
		}
		return functionList;
	}
	public CommonDao getDao() {
		return dao;
	}

	public void setDao(CommonDao dao) {
		this.dao = dao;
	}
	
	public int isHasItem(String parentID) {
		 String hql=null;
		if("".equals(parentID)||parentID==null){
			hql=" where a.parent=null";
		}
		else{
			 hql=" where a.parent.fuid='"+parentID+"'";
		}
		int sign=dao.queryDataByCondition(hql, Function.class, true).size();
		return sign;
	}
	public Function getFunctionDetail(String funcID) {
		System.out.println("--------------------"+funcID);
		Function function=(Function) dao.querySingleData(Function.class, funcID);
		return function;
	}
	public List<Function> listAllFunctions() {
		 List<Function> functionList=new ArrayList<Function>();
		 List<IEntry>  resultList=dao.queryAllData(Function.class);
			for(IEntry result:resultList){
				Function function=(Function) result;
				functionList.add(function);
			}
		return functionList;
	}
	public boolean updateFunction(String functionId,FunctionVO functionVo){
		Function function=(Function) dao.querySingleData(Function.class, functionId);
		Function parent=(Function)dao.querySingleData(Function.class,functionVo.getParentId());
		function.setFname(functionVo.getFname());
		function.setFuri(functionVo.getFuri());
		function.setParent(parent);
		function.setFstatu(functionVo.getFstatu());
		function.setNote(functionVo.getNote());
		int sign=dao.updateData(function);
		if(sign==1){
			return true;
		}
		else{
			return true;
		}
	}
}
