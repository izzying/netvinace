package com.amos.vo;

public class SysUsersVO {
	
	private String uid;
	private String uname;
	private String usex;
	private String deptId;
	private String fphone;
	private String sphone;
	private String fmobile;
	private String smobile;
	private String faddress;
	private String saddress;
	private String qq;
	private String msn;
	private String email;
	private Integer statu=1;
	private String note;
	private String password;
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public SysUsersVO() {
		// TODO Auto-generated constructor stub
	}
/**
 * 
 * @param uid
 * @param uname
 * @param usex
 * @param deptName
 * @param fphone
 * @param sphone
 * @param fmobile
 * @param smobile
 * @param faddress
 * @param saddress
 * @param qq
 * @param msn
 * @param email
 * @param statu
 * @param note
 * @author Administrator
 * @category 全参构造函数
 */
	public SysUsersVO(String uid, String uname, String usex, String deptId,
			String fphone, String sphone, String fmobile, String smobile,
			String faddress, String saddress, String qq, String msn,
			String email, Integer statu, String note,String password) {
		super();
		this.uid = uid;
		this.uname = uname;
		this.usex = usex;
		this.deptId = deptId;
		this.fphone = fphone;
		this.sphone = sphone;
		this.fmobile = fmobile;
		this.smobile = smobile;
		this.faddress = faddress;
		this.saddress = saddress;
		this.qq = qq;
		this.msn = msn;
		this.email = email;
		this.statu = statu;
		this.note = note;
		this.password=password;
	}
/**
 * 
 * @param uid
 * @param uname
 * @param usex
 * @param deptName
 * @param fphone
 * @param fmobile
 * @param faddress
 * @param statu
 * @category 最少参数构造函数
 */
	public SysUsersVO(String uid, String uname, String usex, String deptId,
			String fphone, String fmobile, String faddress,String password, Integer statu) {
		super();
		this.uid = uid;
		this.uname = uname;
		this.usex = usex;
		this.deptId = deptId;
		this.fphone = fphone;
		this.fmobile = fmobile;
		this.faddress = faddress;
		this.password=password;
		this.statu = statu;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getUsex() {
		return usex;
	}

	public void setUsex(String usex) {
		this.usex = usex;
	}

	

	public String getDeptId() {
		return deptId;
	}
	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}
	public String getFphone() {
		return fphone;
	}

	public void setFphone(String fphone) {
		this.fphone = fphone;
	}

	public String getSphone() {
		return sphone;
	}

	public void setSphone(String sphone) {
		this.sphone = sphone;
	}

	public String getFmobile() {
		return fmobile;
	}

	public void setFmobile(String fmobile) {
		this.fmobile = fmobile;
	}

	public String getSmobile() {
		return smobile;
	}

	public void setSmobile(String smobile) {
		this.smobile = smobile;
	}

	public String getFaddress() {
		return faddress;
	}

	public void setFaddress(String faddress) {
		this.faddress = faddress;
	}

	public String getSaddress() {
		return saddress;
	}

	public void setSaddress(String saddress) {
		this.saddress = saddress;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getMsn() {
		return msn;
	}

	public void setMsn(String msn) {
		this.msn = msn;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getStatu() {
		return statu;
	}

	public void setStatu(Integer statu) {
		this.statu = statu;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

}
