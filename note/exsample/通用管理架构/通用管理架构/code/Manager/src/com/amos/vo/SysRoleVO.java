package com.amos.vo;

import com.amos.entry.Departments;

public class SysRoleVO {
	private String roleId;
	private String deptId;
	
	private String roleName;
	private Integer statu=1;;
	private String note;
	
	
	public SysRoleVO(String roleId, String deptId, String roleName,
			Integer statu, String note) {
		super();
		this.roleId = roleId;
		this.deptId = deptId;
		this.roleName = roleName;
		this.statu = statu;
		this.note = note;
	}

	public SysRoleVO(String roleId, String deptId, String roleName,
			Integer statu) {
		super();
		this.roleId = roleId;
		this.deptId = deptId;
		this.roleName = roleName;
		this.statu = statu;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Integer getStatu() {
		return statu;
	}

	public void setStatu(Integer statu) {
		this.statu = statu;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public SysRoleVO() {
	
	}

	

}
