package com.amos.web;

import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.amos.entry.Function;
import com.amos.service.itf.IFunctionService;
import com.amos.vo.FunctionVO;
import com.opensymphony.xwork2.ActionSupport;

public class FunctionAction extends ActionSupport {

	private IFunctionService functionService;
	
	private String fuid;
	private String parentName;
	private String parentId;
	private String fname;
	private Integer fstatu;
	
	private String furi;
	private String accstr;
	private String note;
	
	

	public String ListAllFunction() throws Exception {
		HttpServletResponse response = ServletActionContext.getResponse();
		HttpServletRequest request = ServletActionContext.getRequest();
		 fuid=request.getParameter("fuId")==null?"":request.getParameter("fuId").toString();
		PrintWriter out = response.getWriter();
		 List<Function> sysMenun=functionService.ListFunctions(fuid);
		System.out.println("加载完毕");
		
		request.setAttribute("sysMenun", sysMenun);
		return "list";
		
		
	}

	public String getFunctionDetail() throws Exception {
		HttpServletResponse response = ServletActionContext.getResponse();
		HttpServletRequest request = ServletActionContext.getRequest();
		 fuid=request.getParameter("fuId")==null?"":request.getParameter("fuId").toString();
		 List<Function> sysMenun=functionService.listAllFunctions();
		 Function functionDetail=functionService.getFunctionDetail(fuid);
			this. fuid=functionDetail.getFuid();
			this. fname=functionDetail.getFname();
			this. parentName=functionDetail.getParent()==null?"系统菜单":functionDetail.getParent().getFname();
			this. parentId=functionDetail.getParent()==null?"系统菜单":functionDetail.getParent().getFuid();;
			this.fstatu=functionDetail.getFstatu();
			this. furi=functionDetail.getFuri();
			this. accstr=functionDetail.getAccstr();
			this. note=functionDetail.getNote();
			request.setAttribute("sysMenun", sysMenun);
		return "detail";
	}

	
	public String updateFunction()throws Exception{
		
		
		FunctionVO functionVo=new FunctionVO();
		functionVo.setFname(fname);
		functionVo.setFuid(fuid);
		functionVo.setFstatu(fstatu);
		functionVo.setFuri(furi);
		functionVo.setAccstr(accstr);
		functionVo.setNote(note);
		functionVo.setParentId(parentId);
		functionVo.setParentName(parentName);
		functionService.updateFunction(fuid, functionVo);
		return "update";
	}
	
	//Getter/Setter Methods

	public String getFuid() {
		return fuid;
	}

	public void setFuid(String fuid) {
		this.fuid = fuid;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public Integer getFstatu() {
		return fstatu;
	}

	public void setFstatu(Integer fstatu) {
		this.fstatu = fstatu;
	}

	public String getFuri() {
		return furi;
	}

	public void setFuri(String furi) {
		this.furi = furi;
	}

	public String getAccstr() {
		return accstr;
	}

	public void setAccstr(String accstr) {
		this.accstr = accstr;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public IFunctionService getFunctionService() {
		return functionService;
	}

	public void setFunctionService(IFunctionService functionService) {
		this.functionService = functionService;
	}

}
