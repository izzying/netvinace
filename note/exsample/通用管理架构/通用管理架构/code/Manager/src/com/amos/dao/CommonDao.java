package com.amos.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.amos.utils.IEntry;
import com.amos.utils.PageUtil;


public class CommonDao extends HibernateDaoSupport {

	
	
	public int saveData(IEntry entry){
		try{
		this.getHibernateTemplate().save(entry);
		return 1;
		}
		catch(Exception ex){
			ex.printStackTrace();
			return 0;
		}
		}
	
	/**
	 * 
	 * @param entry
	 * @return int
	 * @category 更新数据
	 */
	
	public int updateData( IEntry entry){
		try{
			this.getHibernateTemplate().merge(entry);
			this.getSession().flush();
			this.getSession().clear();
			return 1;
		}
		catch(Exception e){
			e.printStackTrace();
			return 0;
		}
		finally{
			this.getSession().close();
		}
	}
	
	
	/**
	 * 
	 * @param entryClass
	 * @param id
	 * @return int
	 * @category 删除单条数据
	 */
	
	public int deleteData(Class entryClass,String id){
		try{
			IEntry result=(IEntry) this.getHibernateTemplate().get(entryClass,id);
			if(result!=null){
			this.getHibernateTemplate().delete(result);
			return 1;
			}
			return 0;
		}
		catch(Exception e){
			e.printStackTrace();
			this.getHibernateTemplate().getSessionFactory().getCurrentSession().getTransaction().rollback();
			return 0;
		}
		}
	
	
	
	/**
	 * @author Administrator
	 * @category 批量删除数据
	 * @param entryClass,ids
	 * @return int
	 * @exception Exception
	 * @since 1.0.0   2011-01-19
	 */

	public int deleteDatas(Class entryClass,String[] ids){
			int sign=0;
		try{
			for(String id:ids){
			IEntry result=(IEntry) this.getHibernateTemplate().get(entryClass,id);
			if(result!=null){
			this.getHibernateTemplate().delete(result);
			sign++;
			}
			}
			return sign;
		}
		catch(Exception e){
			e.printStackTrace();
			this.getHibernateTemplate().getSessionFactory().getCurrentSession().getTransaction().rollback();
			return 0;
		}
		}
	
	
	/**
	 * 
	 * @param entryClass
	 * @param pageNo
	 * @param pageSize
	 * @return PageUtil
	 * @category DAO查询分页方法
	 */
	public PageUtil queryAllData(Class entryClass,final int pageNo,final int pageSize){
		
		List<IEntry> resultList=new ArrayList<IEntry>();
		 int resultTotal=this.getHibernateTemplate().loadAll(entryClass).size();
		 final String entryClassName=entryClass.getName();
		 resultList=this.getHibernateTemplate().executeFind(new HibernateCallback(){
				public Object doInHibernate(Session session) throws HibernateException, SQLException {
					return (List<IEntry>)session.createQuery("from "+entryClassName).setFirstResult((pageNo-1)*pageSize).setMaxResults(pageSize).list();
				}});
		 PageUtil page =new PageUtil(pageNo,pageSize,resultList,resultTotal);
		return page;
	}
	
	/**
	 * @category DAO查询非分页方法
	 * @param entryClass
	 * @return
	 */
	public List<IEntry> queryAllData(Class entryClass){
		
		List<IEntry> resultList=new ArrayList<IEntry>();
		resultList=this.getHibernateTemplate().loadAll(entryClass);
		 
		return resultList;
	}
	
	
	public IEntry querySingleData(Class entryClass,String id){
		IEntry result=(IEntry) this.getHibernateTemplate().get(entryClass,id);
		return result;
		
	}
	
	/**
	 * 
	 * @param condition
	 * @param entryClass
	 * @return PageUtil
	 * @category DAO条件查询分页方法
	 */
	
	public PageUtil queryDataByCondition(String condition,Class entryClass,final int pageNo,final int pageSize){
		
		final String HQL="from "+entryClass.getName()+" a " +condition;
		int resultTotal=this.getHibernateTemplate().find(HQL).size();
		
		 final String entryClassName=entryClass.getName();
		 List<IEntry> resultList=this.getHibernateTemplate().executeFind(new HibernateCallback(){
				public Object doInHibernate(Session session) throws HibernateException, SQLException {
					return (List<IEntry>)session.createQuery(HQL).setFirstResult((pageNo-1)*pageSize).setMaxResults(pageSize).list();
				}});	
		 PageUtil page =new PageUtil(pageNo,pageSize,resultList,resultTotal);
		return page;
	}
	
	/**
	 * 
	 * @param condition
	 * @param entryClass
	 * @return
	 * @category DAO条件查询非分页方法
	 */
	public List<IEntry> queryDataByCondition(String condition,Class entryClass,boolean distinct){
		 String HQL=new String();
		if(distinct){
			 HQL="select distinct a from "+entryClass.getName()+" a " +condition;
		}
		else{
			 HQL="select  a from "+entryClass.getName()+" a " +condition;
		}
		List<IEntry> resultList=this.getHibernateTemplate().find(HQL);
		
		
		
		return resultList;
	}
	

	/**
	 * 
	 * @param sql
	 * @return  List
	 * @category SQL查询方法
	 */
	
	public List queryDataBySQL( final String sql){
			
		List resultList=(List)this.getHibernateTemplate().execute(new HibernateCallback(){
			
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				   List resultList=session.createSQLQuery(sql).list();
				return resultList;
			}
			
		});
		return resultList;
	}

	
	/**
	 * 
	 * @param sql
	 * @return  List
	 * @category SQL更新方法
	 */
	
	public void updateDataBySQL( final String sql){
	
		 this.getHibernateTemplate().execute(new HibernateCallback(){
			
			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				int resultTmp=session.createSQLQuery(sql).executeUpdate();
				
				   return resultTmp;
			}
			
		}
		
		 );
	
	}
}
