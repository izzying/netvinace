package com.amos.utils;

import java.util.HashMap;
import java.util.Map;

/*
 * 描述：系统信息代码类，用于返回系统提示信息；
 * 信息结构：MAP<String,String>
 * 版本：测试版
 * 作者：伍尚彬
 * 创建日期：2010-08-22
 * 修改日期：2010-08-22
 */
public class SysMessage {

	private  Map<String,String> sysMessage;

	

	/*
	 * 描述：用于获取系统状态信息，系统信息内容由配置文件提供
	 */
	public  String getSystemMessage(String msgCode) {
		return sysMessage.get(msgCode);
	}
	
	public  Map<String, String> getSysMessage() {
		return sysMessage;
	}

	public  void setSysMessage(Map<String, String> sysMessage) {
		this.sysMessage = sysMessage;
	}
	
	
}
