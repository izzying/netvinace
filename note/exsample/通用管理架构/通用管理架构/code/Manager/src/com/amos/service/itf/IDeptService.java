package com.amos.service.itf;

import java.util.List;

import com.amos.entry.Departments;

public interface IDeptService {

	public Departments getDept(String deptID);
	
	public int updateDept(Departments dept);
	
	public List<Departments> listDept();
	
	public int delDept(String deptID);
	
	public int addDept(Departments dept);
	
	public Departments getDeptByName(String deptName);
	
	public List<Departments> listHistoryDept();
}
