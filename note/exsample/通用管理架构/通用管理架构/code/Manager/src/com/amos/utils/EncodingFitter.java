package com.amos.utils;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.struts2.dispatcher.ng.filter.StrutsPrepareAndExecuteFilter;

public class EncodingFitter implements Filter  {

	private String pageEncode="UTF-8";
;
	
	public String getPageEncode() {
		return pageEncode;
	}

	public void setPageEncode(String pageEncode) {
		this.pageEncode = pageEncode;
	}

	public void destroy() {
		// TODO Auto-generated method stub

	}

	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {
		req.setCharacterEncoding(pageEncode);
		resp.setCharacterEncoding(pageEncode);
		resp.setContentType("text/html");
		
		chain.doFilter(req, resp);

	}

	public void init(FilterConfig config) throws ServletException {
		String encodingParam = config.getInitParameter("pageEnCode");
        if (encodingParam != null && encodingParam.trim().length() != 0) {
        	pageEncode = encodingParam;
        }

	}

}
