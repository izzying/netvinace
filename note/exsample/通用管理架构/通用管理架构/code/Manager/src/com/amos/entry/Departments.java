package com.amos.entry;

import java.util.HashSet;
import java.util.Set;

import com.amos.utils.IEntry;

/**
 * Departments entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class Departments implements IEntry {

	// Fields

	private String deptid;
	private SysUsers sysUsers;
	private Departments departments;
	private Integer id;
	private String deptname;
	private String description;
	private Integer statu;
	private String note;
	
	private Set departmentses = new HashSet(0);
	private Set sysRoles = new HashSet(0);

	// Constructors

	/** default constructor */
	public Departments() {
	}


	// Property accessors

	public String getDeptid() {
		return this.deptid;
	}

	public void setDeptid(String deptid) {
		this.deptid = deptid;
	}

	public SysUsers getSysUsers() {
		return this.sysUsers;
	}

	public void setSysUsers(SysUsers sysUsers) {
		this.sysUsers = sysUsers;
	}

	public Departments getDepartments() {
		return this.departments;
	}

	public void setDepartments(Departments departments) {
		this.departments = departments;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDeptname() {
		return this.deptname;
	}

	public void setDeptname(String deptname) {
		this.deptname = deptname;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStatu() {
		return this.statu;
	}

	public void setStatu(Integer statu) {
		this.statu = statu;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}



	public Set getDepartmentses() {
		return this.departmentses;
	}

	public void setDepartmentses(Set departmentses) {
		this.departmentses = departmentses;
	}

	public Set getSysRoles() {
		return this.sysRoles;
	}

	public void setSysRoles(Set sysRoles) {
		this.sysRoles = sysRoles;
	}

}