package com.amos.entry;

import com.amos.utils.IEntry;

public class UserPassword implements IEntry {

	private SysUsers user;
	
	private String uid;
	
	private String password;
	
	private String question;
	
	private String answer;
	
	private String note;
	
	public UserPassword() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserPassword(String md5) {
		password=md5;
	}
	public SysUsers getUser() {
		return user;
	}
	public void setUser(SysUsers user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public UserPassword(SysUsers user, String password) {
		super();
		this.user = user;
		this.password = password;
	}
	
	
}
