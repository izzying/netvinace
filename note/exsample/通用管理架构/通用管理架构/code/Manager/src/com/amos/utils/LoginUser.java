package com.amos.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.amos.entry.Function;
import com.amos.entry.SysRole;
import com.amos.entry.SysUsers;

public class LoginUser {

	//private String uid;
	private String loginTime;
	private String loginIP;
	private SysUsers user;
	private String loginStatu;
	private Map<Function,Set<Function>> sysMenun=new HashMap(0);
	
	public SysUsers getUser() {
		return user;
		
	}
	public void setUser(SysUsers user) {
		this.user = user;
	}
//	public String getUid() {
//		return uid;
//	}
//	public void setUid(String uid) {
//		this.uid = uid;
//	}
	public String getLoginTime() {
		return loginTime;
	}
	public void setLoginTime(String loginTime) {
		this.loginTime = loginTime;
	}
	public String getLoginIP() {
		return loginIP;
	}
	public void setLoginIP(String loginIP) {
		this.loginIP = loginIP;
	}
	public void setLoginStatu(String loginStatu) {
		this.loginStatu=loginStatu;
		
	}
	public String getLoginStatu() {
		return loginStatu;
	}
	

	public Map<Function, Set<Function>> getSysMenun() {
		
		Iterator<SysRole> itRole=this.user.getUrole().iterator();
		while(itRole.hasNext()){
		Iterator<Function> itFunc=itRole.next().getFunctionList().iterator();
		while(itFunc.hasNext())
		{
			Function topMenun=itFunc.next();
			
			if( topMenun.getSubListses()!=null&&topMenun.getSubListses().size()!=0){
				Set<Function> subList=topMenun.getSubListses();
				
				this.sysMenun.put(topMenun, subList);
				
			}
		}
		}
		return sysMenun;
		
	}


	
	private Set<Function> displaySubItem(Set<SysRole> urole,Function item,Set<Function> subList) {
		
		Iterator<SysRole> itRole=urole.iterator();
		
		while(itRole.hasNext()){
			SysRole role=itRole.next();
			if(item.getRoleList().contains(role)){
				subList.remove(item);
			}
			else{
				continue;
			}
		}
		return subList;
	}
	public void setSysMenun(Map<Function, Set<Function>> sysMenun) {
//		
//		Iterator<Function> itFunc=this.user.getUrole().getFunctionList().iterator();
//		while(itFunc.hasNext())
//		{
//			Function topMenun=itFunc.next();
//			
//			if( topMenun.getSubList()!=null&&topMenun.getSubList().size()!=0){
//				this.sysMenun.put(topMenun, topMenun.getSubList());
//				
//			}
//		}
		this.sysMenun=sysMenun;
		

	}
	
}
