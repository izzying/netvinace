/*
SQLyog v10.2 
MySQL - 5.5.27 : Database - netvinace
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`netvinace` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;

USE `netvinace`;

/*Table structure for table `ams_acsinfos` */

DROP TABLE IF EXISTS `ams_acsinfos`;

CREATE TABLE `ams_acsinfos` (
  `acsid` longblob NOT NULL,
  `roleid` longblob,
  `resid` longblob,
  `retid` longblob,
  `ctrl` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统权限信息';

/*Data for the table `ams_acsinfos` */

/*Table structure for table `ams_activedetail` */

DROP TABLE IF EXISTS `ams_activedetail`;

CREATE TABLE `ams_activedetail` (
  `serid` longblob NOT NULL,
  `act_id` longblob,
  `member_id` longblob,
  `work_descp` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `cometime` datetime DEFAULT NULL,
  `gotime` datetime DEFAULT NULL,
  `result` varchar(10) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='活动/会议详细信息，成员信息';

/*Data for the table `ams_activedetail` */

/*Table structure for table `ams_activeinfos` */

DROP TABLE IF EXISTS `ams_activeinfos`;

CREATE TABLE `ams_activeinfos` (
  `act_id` longblob NOT NULL,
  `act_name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `act_status` int(11) DEFAULT NULL,
  `act_descp` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `act_note` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `act_address` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `starts` datetime DEFAULT NULL,
  `ends` datetime DEFAULT NULL,
  `creator` longblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='活动/会议基本信息表';

/*Data for the table `ams_activeinfos` */

/*Table structure for table `ams_buginfos` */

DROP TABLE IF EXISTS `ams_buginfos`;

CREATE TABLE `ams_buginfos` (
  `serid` longblob NOT NULL,
  `tmid` longblob,
  `pro_id` longblob,
  `uploaderId` longblob,
  `attach` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `commit_time` datetime DEFAULT NULL,
  `last_time` datetime DEFAULT NULL,
  `handelId` longblob,
  `chksum` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `bug_note` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='项目bug跟踪信息';

/*Data for the table `ams_buginfos` */

/*Table structure for table `ams_msginfos` */

DROP TABLE IF EXISTS `ams_msginfos`;

CREATE TABLE `ams_msginfos` (
  `serid` longblob NOT NULL,
  `msg_content` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `senderId` longblob,
  `toId` longblob,
  `Msg_type` longblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统信息表';

/*Data for the table `ams_msginfos` */

/*Table structure for table `ams_msgtype` */

DROP TABLE IF EXISTS `ams_msgtype`;

CREATE TABLE `ams_msgtype` (
  `mtid` longblob NOT NULL,
  `typeName` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `handelClz` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='消息类型信息表';

/*Data for the table `ams_msgtype` */

/*Table structure for table `ams_procinfos` */

DROP TABLE IF EXISTS `ams_procinfos`;

CREATE TABLE `ams_procinfos` (
  `procid` longblob NOT NULL,
  `proid` longblob,
  `procname` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `descp` text COLLATE utf8_bin,
  `attach` varchar(200) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='项目里程碑信息';

/*Data for the table `ams_procinfos` */

/*Table structure for table `ams_projectinfos` */

DROP TABLE IF EXISTS `ams_projectinfos`;

CREATE TABLE `ams_projectinfos` (
  `pro_id` longblob NOT NULL,
  `pro_name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `pro_status` int(11) DEFAULT NULL,
  `pro_descp` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `pro_note` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `resp_type` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `streamType` longblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='项目基本信息表';

/*Data for the table `ams_projectinfos` */

/*Table structure for table `ams_protmdetail` */

DROP TABLE IF EXISTS `ams_protmdetail`;

CREATE TABLE `ams_protmdetail` (
  `prm_id` longblob NOT NULL,
  `tmid` longblob,
  `pro_id` longblob,
  `uid` longblob,
  `rid` longblob,
  `prm_note` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='项目组成员信息';

/*Data for the table `ams_protmdetail` */

/*Table structure for table `ams_protminfos` */

DROP TABLE IF EXISTS `ams_protminfos`;

CREATE TABLE `ams_protminfos` (
  `tmid` longblob NOT NULL,
  `tmname` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `pro_id` longblob,
  `tmnote` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `leader` longblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='项目组信息';

/*Data for the table `ams_protminfos` */

/*Table structure for table `ams_restypeinfos` */

DROP TABLE IF EXISTS `ams_restypeinfos`;

CREATE TABLE `ams_restypeinfos` (
  `retid` longblob NOT NULL,
  `retname` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `handleClass` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `note` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统资源类型信息';

/*Data for the table `ams_restypeinfos` */

/*Table structure for table `ams_roleinfos` */

DROP TABLE IF EXISTS `ams_roleinfos`;

CREATE TABLE `ams_roleinfos` (
  `rid` longblob NOT NULL,
  `rname` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `rtype` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `status` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `note` varchar(200) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统角色基本信息,包含系统角色和项目角色';

/*Data for the table `ams_roleinfos` */

/*Table structure for table `ams_taskinfos` */

DROP TABLE IF EXISTS `ams_taskinfos`;

CREATE TABLE `ams_taskinfos` (
  `serid` longblob NOT NULL,
  `tmid` longblob,
  `pro_id` longblob,
  `senderId` longblob,
  `receverId` longblob,
  `attach` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `commit_Time` datetime DEFAULT NULL,
  `last_time` datetime DEFAULT NULL,
  `chksum` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `status` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `task_note` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='任务信息';

/*Data for the table `ams_taskinfos` */

/*Table structure for table `ams_userinfos` */

DROP TABLE IF EXISTS `ams_userinfos`;

CREATE TABLE `ams_userinfos` (
  `uid` longblob NOT NULL,
  `username` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `pwd` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `question` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `answer` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `phone` varchar(13) COLLATE utf8_bin DEFAULT NULL,
  `qq` varchar(13) COLLATE utf8_bin DEFAULT NULL,
  `mobile` varchar(13) COLLATE utf8_bin DEFAULT NULL,
  `mail` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `addr` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `note` varchar(200) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统用户信息表';

/*Data for the table `ams_userinfos` */

/*Table structure for table `test` */

DROP TABLE IF EXISTS `test`;

CREATE TABLE `test` (
  `id` int(10) NOT NULL,
  `name` varchar(10) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

/*Data for the table `test` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
