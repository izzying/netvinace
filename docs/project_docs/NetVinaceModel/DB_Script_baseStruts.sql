/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2013-1-29 15:57:18                           */
/*==============================================================*/


alter table Ams_acsinfos
   drop primary key;

drop table if exists Ams_acsinfos;

alter table Ams_activeDetail
   drop primary key;

drop table if exists Ams_activeDetail;

alter table Ams_activeinfos
   drop primary key;

drop table if exists Ams_activeinfos;

alter table Ams_buginfos
   drop primary key;

drop table if exists Ams_buginfos;

alter table Ams_msgType
   drop primary key;

drop table if exists Ams_msgType;

alter table Ams_msginfos
   drop primary key;

drop table if exists Ams_msginfos;

alter table Ams_procinfos
   drop primary key;

drop table if exists Ams_procinfos;

alter table Ams_projectinfos
   drop primary key;

drop table if exists Ams_projectinfos;

alter table Ams_protmDetail
   drop primary key;

drop table if exists Ams_protmDetail;

alter table Ams_protminfos
   drop primary key;

drop table if exists Ams_protminfos;

alter table Ams_resInfos
   drop primary key;

drop table if exists Ams_resInfos;

alter table Ams_resTypeInfos
   drop primary key;

drop table if exists Ams_resTypeInfos;

alter table Ams_roleinfos
   drop primary key;

drop table if exists Ams_roleinfos;

alter table Ams_taskinfos
   drop primary key;

drop table if exists Ams_taskinfos;

alter table Ams_userinfos
   drop primary key;

drop table if exists Ams_userinfos;

/*==============================================================*/
/* Table: Ams_acsinfos                                          */
/*==============================================================*/
create table Ams_acsinfos
(
   acsid                long not null,
   roleid               long,
   resid                long,
   retid                long,
   ctrl                 varchar(100)
);

alter table Ams_acsinfos comment '系统权限信息';

alter table Ams_acsinfos
   add primary key (acsid);

/*==============================================================*/
/* Table: Ams_activeDetail                                      */
/*==============================================================*/
create table Ams_activeDetail
(
   serid                long not null,
   act_id               long,
   member_id            long,
   work_descp           varchar(1000),
   cometime             datetime,
   gotime               datetime,
   result               varchar(10)
);

alter table Ams_activeDetail comment '活动/会议详细信息，成员信息';

alter table Ams_activeDetail
   add primary key (serid);

/*==============================================================*/
/* Table: Ams_activeinfos                                       */
/*==============================================================*/
create table Ams_activeinfos
(
   act_id               long not null,
   act_name             varchar(100),
   act_status           int,
   act_descp            varchar(1000),
   act_note             varchar(200),
   act_address          varchar(200),
   starts               datetime,
   ends                 datetime,
   creator              long
);

alter table Ams_activeinfos comment '活动/会议基本信息表';

alter table Ams_activeinfos
   add primary key (act_id);

/*==============================================================*/
/* Table: Ams_buginfos                                          */
/*==============================================================*/
create table Ams_buginfos
(
   serid                long not null,
   tmid                 long,
   pro_id               long,
   uploaderId           long,
   attach               varchar(200),
   commit_time          datetime,
   last_time            datetime,
   handelId             long,
   chksum               varchar(128),
   bug_note             varchar(100)
);

alter table Ams_buginfos comment '项目bug跟踪信息';

alter table Ams_buginfos
   add primary key (serid);

/*==============================================================*/
/* Table: Ams_msgType                                           */
/*==============================================================*/
create table Ams_msgType
(
   mtid                 long not null,
   typeName             varchar(20),
   handelClz            varchar(100)
);

alter table Ams_msgType comment '消息类型信息表';

alter table Ams_msgType
   add primary key (mtid);

/*==============================================================*/
/* Table: Ams_msginfos                                          */
/*==============================================================*/
create table Ams_msginfos
(
   serid                long not null,
   msg_content          varchar(200),
   senderId             long,
   toId                 long,
   Msg_type             long
);

alter table Ams_msginfos comment '系统信息表';

alter table Ams_msginfos
   add primary key (serid);

/*==============================================================*/
/* Table: Ams_procinfos                                         */
/*==============================================================*/
create table Ams_procinfos
(
   procid               long not null,
   proid                long,
   procname             varchar(100),
   descp                text,
   attach               varchar(200)
);

alter table Ams_procinfos comment '项目里程碑信息';

alter table Ams_procinfos
   add primary key (procid);

/*==============================================================*/
/* Table: Ams_projectinfos                                      */
/*==============================================================*/
create table Ams_projectinfos
(
   pro_id               long not null,
   pro_name             varchar(100),
   pro_status           int,
   pro_descp            varchar(1000),
   pro_note             varchar(200),
   resp_type            varchar(500),
   streamType           long
);

alter table Ams_projectinfos comment '项目基本信息表';

alter table Ams_projectinfos
   add primary key (pro_id);

/*==============================================================*/
/* Table: Ams_protmDetail                                       */
/*==============================================================*/
create table Ams_protmDetail
(
   prm_id               long not null,
   tmid                 long,
   pro_id               long,
   uid                  long,
   rid                  long,
   prm_note             varchar(100)
);

alter table Ams_protmDetail comment '项目组成员信息';

alter table Ams_protmDetail
   add primary key (prm_id);

/*==============================================================*/
/* Table: Ams_protminfos                                        */
/*==============================================================*/
create table Ams_protminfos
(
   tmid                 long not null,
   tmname               varchar(20),
   pro_id               long,
   tmnote               varchar(100),
   leader               long
);

alter table Ams_protminfos comment '项目组信息';

alter table Ams_protminfos
   add primary key (tmid);

/*==============================================================*/
/* Table: Ams_resInfos                                          */
/*==============================================================*/
create table Ams_resInfos
(
   reid                 long not null,
   "rename"             varchar(100),
   restype              long,
   status               varchar(20),
   resurl               Varchar(200),
   chksum               Varchar(128),
   note                 varchar(200)
);

alter table Ams_resInfos comment '系统资源信息';

alter table Ams_resInfos
   add primary key (reid);

/*==============================================================*/
/* Table: Ams_resTypeInfos                                      */
/*==============================================================*/
create table Ams_resTypeInfos
(
   retid                long not null,
   retname              varchar(100),
   handleClass          varchar(100),
   note                 varchar(100)
);

alter table Ams_resTypeInfos comment '系统资源类型信息';

alter table Ams_resTypeInfos
   add primary key (retid);

/*==============================================================*/
/* Table: Ams_roleinfos                                         */
/*==============================================================*/
create table Ams_roleinfos
(
   rid                  long not null,
   rname                Varchar(20),
   rtype                varchar(20),
   status               varchar(10),
   note                 Varchar(200)
);

alter table Ams_roleinfos comment '系统角色基本信息,包含系统角色和项目角色';

alter table Ams_roleinfos
   add primary key (rid);

/*==============================================================*/
/* Table: Ams_taskinfos                                         */
/*==============================================================*/
create table Ams_taskinfos
(
   serid                long not null,
   tmid                 long,
   pro_id               long,
   senderId             long,
   receverId            long,
   attach               varchar(200),
   commit_Time          datetime,
   last_time            datetime,
   chksum               varchar(128),
   status               varchar(10),
   task_note            varchar(100)
);

alter table Ams_taskinfos comment '任务信息';

alter table Ams_taskinfos
   add primary key (serid);

/*==============================================================*/
/* Table: Ams_userinfos                                         */
/*==============================================================*/
create table Ams_userinfos
(
   uid                  long not null,
   username             varchar(20),
   pwd                  varchar(128),
   question             varchar(100),
   answer               varchar(100),
   status               int,
   phone                varchar(13),
   qq                   varchar(13),
   mobile               varchar(13),
   mail                 varchar(50),
   addr                 Varchar(200),
   note                 varchar(200)
);

alter table Ams_userinfos comment '系统用户信息表';

alter table Ams_userinfos
   add primary key (uid);

