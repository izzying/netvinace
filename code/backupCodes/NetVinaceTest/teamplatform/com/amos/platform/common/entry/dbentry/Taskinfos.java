package com.amos.platform.common.entry.dbentry;

/***********************************************************************
 * Module:  Taskinfos.java
 * Author:  Administrator
 * Purpose: Defines the Class Taskinfos
 ***********************************************************************/

/**
 * 任务信息
 * 
 * @pdOid 08ac0ebf-0d0c-4c08-bad2-d8c3b2aeb980
 */
public class Taskinfos {
	/** @pdOid 991461b3-b5d7-4698-bb18-15efa51add61 */
	public int serid;
	/** @pdOid 60bc1cc3-b056-4651-8904-ca261082d0f4 */
	public java.lang.String attach;
	/** @pdOid 4b9fb58c-95a3-4dd3-a05f-f740e7678818 */
	public java.util.Date commitTime;
	/** @pdOid d9c62021-90c2-4d77-b0e8-46a48d01e71f */
	public java.util.Date lastTime;
	/** @pdOid d9d904bc-7d72-4ea6-9027-4aee3b71274b */
	public java.lang.String chksum;
	/** @pdOid a22b9f87-9d7b-4806-a173-b58ba9c76850 */
	public java.lang.String status;
	/** @pdOid e53876bf-7638-4001-8a78-7118c62574f6 */
	public java.lang.String taskNote;

	/**
	 * @pdRoleInfo migr=no name=AmsProjectinfos assc=taskProjectInfosRef
	 *             mult=0..1 side=A
	 */
	public AmsProjectinfos amsProjectinfos;
	/**
	 * @pdRoleInfo migr=no name=Userinfos assc=taskReceverInfosRef mult=0..1
	 *             side=A
	 */
	public Userinfos taskRecever;
	/**
	 * @pdRoleInfo migr=no name=Userinfos assc=taskSenderInfosRef mult=0..1
	 *             side=A
	 */
	public Userinfos taskSender;
	/**
	 * @pdRoleInfo migr=no name=AmsProtminfos assc=taskProjectTeamInfors
	 *             mult=0..1 side=A
	 */
	public AmsProtminfos amsProtminfos;

	/** @pdOid 84c53c80-ae4d-45b9-a92f-91f95b0d2840 */
	public int getSerid() {
		return serid;
	}

	/**
	 * @param newSerid
	 * @pdOid 274d4955-5c91-46ee-a451-50c3df0ce39f
	 */
	public void setSerid(int newSerid) {
		serid = newSerid;
	}

	/** @pdOid 7ca265b1-0fc4-4b27-a71d-e87ae4645ca9 */
	public java.lang.String getAttach() {
		return attach;
	}

	/**
	 * @param newAttach
	 * @pdOid 0c2b76a1-4c96-4851-bf24-fd87f50af31d
	 */
	public void setAttach(java.lang.String newAttach) {
		attach = newAttach;
	}

	/** @pdOid ebdaf4ed-52b6-40f0-b66a-e5c1c515964c */
	public java.util.Date getCommitTime() {
		return commitTime;
	}

	/**
	 * @param newCommitTime
	 * @pdOid 09def38e-5558-4d58-bd42-23bff73c1973
	 */
	public void setCommitTime(java.util.Date newCommitTime) {
		commitTime = newCommitTime;
	}

	/** @pdOid 6d6441bd-16dd-4d79-9bfd-de2b2f3b35f3 */
	public java.util.Date getLastTime() {
		return lastTime;
	}

	/**
	 * @param newLastTime
	 * @pdOid fda72e64-88e2-4f26-bcf2-c85c09b51306
	 */
	public void setLastTime(java.util.Date newLastTime) {
		lastTime = newLastTime;
	}

	/** @pdOid ff67575b-e1fd-44b0-afdd-950ef84ec4ac */
	public java.lang.String getChksum() {
		return chksum;
	}

	/**
	 * @param newChksum
	 * @pdOid 8b9ad050-ab09-4d3f-be07-fbfdccc5cf0e
	 */
	public void setChksum(java.lang.String newChksum) {
		chksum = newChksum;
	}

	/** @pdOid 01b8e110-3a29-4137-82a1-f2d300ed36e1 */
	public java.lang.String getStatus() {
		return status;
	}

	/**
	 * @param newStatus
	 * @pdOid a683aa01-fced-4a74-818d-d95017f4fb24
	 */
	public void setStatus(java.lang.String newStatus) {
		status = newStatus;
	}

	/** @pdOid 0bb8af04-cc13-44b5-9cd2-f93a9d970b0f */
	public java.lang.String getTaskNote() {
		return taskNote;
	}

	/**
	 * @param newTaskNote
	 * @pdOid 93d64ed9-818d-4cb2-a727-4cdd506f899b
	 */
	public void setTaskNote(java.lang.String newTaskNote) {
		taskNote = newTaskNote;
	}

	/** @pdGenerated default parent getter */
	public AmsProjectinfos getAmsProjectinfos() {
		return amsProjectinfos;
	}

	/**
	 * @pdGenerated default parent setter
	 * @param newAmsProjectinfos
	 */
	public void setAmsProjectinfos(AmsProjectinfos newAmsProjectinfos) {
		if (this.amsProjectinfos == null
				|| !this.amsProjectinfos.equals(newAmsProjectinfos)) {
			if (this.amsProjectinfos != null) {
				AmsProjectinfos oldAmsProjectinfos = this.amsProjectinfos;
				this.amsProjectinfos = null;
				oldAmsProjectinfos.removeTaskinfos(this);
			}
			if (newAmsProjectinfos != null) {
				this.amsProjectinfos = newAmsProjectinfos;
				this.amsProjectinfos.addTaskinfos(this);
			}
		}
	}

	public Userinfos getTaskRecever() {
		return taskRecever;
	}

	public void setTaskRecever(Userinfos taskRecever) {
		this.taskRecever = taskRecever;
	}

	public Userinfos getTaskSender() {
		return taskSender;
	}

	public void setTaskSender(Userinfos taskSender) {
		this.taskSender = taskSender;
	}

	/** @pdGenerated default parent getter */
	public AmsProtminfos getAmsProtminfos() {
		return amsProtminfos;
	}

	/**
	 * @pdGenerated default parent setter
	 * @param newAmsProtminfos
	 */
	public void setAmsProtminfos(AmsProtminfos newAmsProtminfos) {
		if (this.amsProtminfos == null
				|| !this.amsProtminfos.equals(newAmsProtminfos)) {
			if (this.amsProtminfos != null) {
				AmsProtminfos oldAmsProtminfos = this.amsProtminfos;
				this.amsProtminfos = null;
				oldAmsProtminfos.removeTaskinfos(this);
			}
			if (newAmsProtminfos != null) {
				this.amsProtminfos = newAmsProtminfos;
				this.amsProtminfos.addTaskinfos(this);
			}
		}
	}

}