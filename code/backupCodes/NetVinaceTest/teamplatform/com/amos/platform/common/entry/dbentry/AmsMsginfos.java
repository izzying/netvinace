package com.amos.platform.common.entry.dbentry;

/***********************************************************************
 * Module:  AmsMsginfos.java
 * Author:  Administrator
 * Purpose: Defines the Class AmsMsginfos
 ***********************************************************************/


/** 系统信息表
 * 
 * @pdOid 5415bb2e-0387-4ce8-8786-5316a0920814 */
public class AmsMsginfos {
   /** @pdOid 296d1121-a5b1-4c65-9a1d-2a2b322d89c9 */
   public int serid;
   /** @pdOid 7ee528fe-6d17-4414-9fe8-a06f2e30672f */
   public java.lang.String msgContent;
   /** @pdOid b283e625-380c-497d-80e2-512804083e32 */
   public int senderId;
   
   /** @pdRoleInfo migr=no name=AmsMsgType assc=msgTypeInfor mult=0..1 side=A */
   public AmsMsgType amsMsgType;
   /** @pdRoleInfo migr=no name=Userinfos assc=msgToMember mult=0..1 side=A */
   public Userinfos userinfos;
   
   /** @pdOid fa0214e6-551e-4d05-949c-b72b9295b39c */
   public int getSerid() {
      return serid;
   }
   
   /** @param newSerid
    * @pdOid ee26fda6-ddbb-4bf9-a810-4ad22978c458 */
   public void setSerid(int newSerid) {
      serid = newSerid;
   }
   
   /** @pdOid b7f75516-f8d1-4613-986b-c245683fd41d */
   public java.lang.String getMsgContent() {
      return msgContent;
   }
   
   /** @param newMsgContent
    * @pdOid 9e6dbad7-d6ff-46f5-9c4d-f42ae18660a2 */
   public void setMsgContent(java.lang.String newMsgContent) {
      msgContent = newMsgContent;
   }
   
   /** @pdOid 6fad70f6-9278-44bc-90a8-638d93976101 */
   public int getSenderId() {
      return senderId;
   }
   
   /** @param newSenderId
    * @pdOid 37dd3526-abb9-4186-afa8-4387cf050b9b */
   public void setSenderId(int newSenderId) {
      senderId = newSenderId;
   }
   
   
   /** @pdGenerated default parent getter */
   public AmsMsgType getAmsMsgType() {
      return amsMsgType;
   }
   
   /** @pdGenerated default parent setter
     * @param newAmsMsgType */
   public void setAmsMsgType(AmsMsgType newAmsMsgType) {
      if (this.amsMsgType == null || !this.amsMsgType.equals(newAmsMsgType))
      {
         if (this.amsMsgType != null)
         {
            AmsMsgType oldAmsMsgType = this.amsMsgType;
            this.amsMsgType = null;
            oldAmsMsgType.removeAmsMsginfos(this);
         }
         if (newAmsMsgType != null)
         {
            this.amsMsgType = newAmsMsgType;
            this.amsMsgType.addAmsMsginfos(this);
         }
      }
   }
   /** @pdGenerated default parent getter */
   public Userinfos getUserinfos() {
      return userinfos;
   }
   
   
   

}