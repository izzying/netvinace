package com.amos.platform.common.entry.dbentry;

/***********************************************************************
 * Module:  AmsProjectinfos.java
 * Author:  Administrator
 * Purpose: Defines the Class AmsProjectinfos
 ***********************************************************************/


/** 项目基本信息表
 * 
 * @pdOid a9f7e0d8-eb91-48fb-a922-6d72995faba7 */
public class AmsProjectinfos {
   /** @pdOid 410c8126-95e4-4d92-a549-a646cdad20ad */
   public int proId;
   /** @pdOid 0503b32e-93fe-46cf-adf1-09ad25a11a29 */
   public java.lang.String proName;
   /** @pdOid 84e35bcc-8326-4a7a-945e-332383e4470d */
   public int proStatus;
   /** @pdOid d22a8b1f-ca1b-4f5d-b606-7f6cca7b731a */
   public java.lang.String proDescp;
   /** @pdOid bd6c4bca-a7e5-4986-839d-94acdfaad3ad */
   public java.lang.String proNote;
   /** @pdOid 7eb085cd-a6a5-4f9b-be23-3299ab7ea7c6 */
   public java.lang.String respType;
   /** @pdOid c760dc5f-8c2a-40a8-947c-98985c09c84f */
   public int streamType;
   
   /** @pdRoleInfo migr=no name=AmsProcinfos assc=projectProcess coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<AmsProcinfos> amsProcinfos;
   /** @pdRoleInfo migr=no name=AmsProtminfos assc=projectTeamInfos coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<AmsProtminfos> amsProtminfos;
   /** @pdRoleInfo migr=no name=AmsProtmDetail assc=projectInfosRef coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<AmsProtmDetail> amsProtmDetail;
   /** @pdRoleInfo migr=no name=Buginfos assc=projectInfosRef coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<Buginfos> buginfos;
   /** @pdRoleInfo migr=no name=Taskinfos assc=taskProjectInfosRef coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<Taskinfos> taskinfos;
   
   /** @pdOid 21262ea6-1334-482b-8ff8-5743b2ab6630 */
   public int getProId() {
      return proId;
   }
   
   /** @param newProId
    * @pdOid aab83481-0afd-4f53-9136-7e29ec0e116e */
   public void setProId(int newProId) {
      proId = newProId;
   }
   
   /** @pdOid c59652fe-7864-49f3-9e3a-38426562b815 */
   public java.lang.String getProName() {
      return proName;
   }
   
   /** @param newProName
    * @pdOid 6a704892-3dc0-4715-ac0a-c1a75fdaeaef */
   public void setProName(java.lang.String newProName) {
      proName = newProName;
   }
   
   /** @pdOid fca1ec55-a644-4ff6-90cb-c95c66732bef */
   public int getProStatus() {
      return proStatus;
   }
   
   /** @param newProStatus
    * @pdOid d0d8353d-85e7-4002-adf0-6ce55018076d */
   public void setProStatus(int newProStatus) {
      proStatus = newProStatus;
   }
   
   /** @pdOid 9c8de9b8-61a5-4017-a482-c5d9b397ef01 */
   public java.lang.String getProDescp() {
      return proDescp;
   }
   
   /** @param newProDescp
    * @pdOid 6e50cbb9-4402-4578-b275-d7a017dceb29 */
   public void setProDescp(java.lang.String newProDescp) {
      proDescp = newProDescp;
   }
   
   /** @pdOid f3f8b8ca-20af-41e4-b4f6-60a0d3e88bea */
   public java.lang.String getProNote() {
      return proNote;
   }
   
   /** @param newProNote
    * @pdOid 5d6790bc-42bd-4161-ae39-f3e4d1ac52be */
   public void setProNote(java.lang.String newProNote) {
      proNote = newProNote;
   }
   
   /** @pdOid fe75c76a-6f5c-4efa-9cac-9803cf7c3b60 */
   public java.lang.String getRespType() {
      return respType;
   }
   
   /** @param newRespType
    * @pdOid 3011479b-46b2-4f86-888d-075a61be9ac3 */
   public void setRespType(java.lang.String newRespType) {
      respType = newRespType;
   }
   
   /** @pdOid 339a404f-7ef9-47fa-8adc-0b76b820feee */
   public int getStreamType() {
      return streamType;
   }
   
   /** @param newStreamType
    * @pdOid e2c5c0c4-87fc-424b-ad68-2f7e4814f386 */
   public void setStreamType(int newStreamType) {
      streamType = newStreamType;
   }
   
   
   /** @pdGenerated default getter */
   public java.util.Collection<AmsProcinfos> getAmsProcinfos() {
      if (amsProcinfos == null)
         amsProcinfos = new java.util.HashSet<AmsProcinfos>();
      return amsProcinfos;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorAmsProcinfos() {
      if (amsProcinfos == null)
         amsProcinfos = new java.util.HashSet<AmsProcinfos>();
      return amsProcinfos.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newAmsProcinfos */
   public void setAmsProcinfos(java.util.Collection<AmsProcinfos> newAmsProcinfos) {
      removeAllAmsProcinfos();
      for (java.util.Iterator iter = newAmsProcinfos.iterator(); iter.hasNext();)
         addAmsProcinfos((AmsProcinfos)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newAmsProcinfos */
   public void addAmsProcinfos(AmsProcinfos newAmsProcinfos) {
      if (newAmsProcinfos == null)
         return;
      if (this.amsProcinfos == null)
         this.amsProcinfos = new java.util.HashSet<AmsProcinfos>();
      if (!this.amsProcinfos.contains(newAmsProcinfos))
      {
         this.amsProcinfos.add(newAmsProcinfos);
         newAmsProcinfos.setAmsProjectinfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldAmsProcinfos */
   public void removeAmsProcinfos(AmsProcinfos oldAmsProcinfos) {
      if (oldAmsProcinfos == null)
         return;
      if (this.amsProcinfos != null)
         if (this.amsProcinfos.contains(oldAmsProcinfos))
         {
            this.amsProcinfos.remove(oldAmsProcinfos);
            oldAmsProcinfos.setAmsProjectinfos((AmsProjectinfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllAmsProcinfos() {
      if (amsProcinfos != null)
      {
         AmsProcinfos oldAmsProcinfos;
         for (java.util.Iterator iter = getIteratorAmsProcinfos(); iter.hasNext();)
         {
            oldAmsProcinfos = (AmsProcinfos)iter.next();
            iter.remove();
            oldAmsProcinfos.setAmsProjectinfos((AmsProjectinfos)null);
         }
      }
   }
   /** @pdGenerated default getter */
   public java.util.Collection<AmsProtminfos> getAmsProtminfos() {
      if (amsProtminfos == null)
         amsProtminfos = new java.util.HashSet<AmsProtminfos>();
      return amsProtminfos;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorAmsProtminfos() {
      if (amsProtminfos == null)
         amsProtminfos = new java.util.HashSet<AmsProtminfos>();
      return amsProtminfos.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newAmsProtminfos */
   public void setAmsProtminfos(java.util.Collection<AmsProtminfos> newAmsProtminfos) {
      removeAllAmsProtminfos();
      for (java.util.Iterator iter = newAmsProtminfos.iterator(); iter.hasNext();)
         addAmsProtminfos((AmsProtminfos)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newAmsProtminfos */
   public void addAmsProtminfos(AmsProtminfos newAmsProtminfos) {
      if (newAmsProtminfos == null)
         return;
      if (this.amsProtminfos == null)
         this.amsProtminfos = new java.util.HashSet<AmsProtminfos>();
      if (!this.amsProtminfos.contains(newAmsProtminfos))
      {
         this.amsProtminfos.add(newAmsProtminfos);
         newAmsProtminfos.setAmsProjectinfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldAmsProtminfos */
   public void removeAmsProtminfos(AmsProtminfos oldAmsProtminfos) {
      if (oldAmsProtminfos == null)
         return;
      if (this.amsProtminfos != null)
         if (this.amsProtminfos.contains(oldAmsProtminfos))
         {
            this.amsProtminfos.remove(oldAmsProtminfos);
            oldAmsProtminfos.setAmsProjectinfos((AmsProjectinfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllAmsProtminfos() {
      if (amsProtminfos != null)
      {
         AmsProtminfos oldAmsProtminfos;
         for (java.util.Iterator iter = getIteratorAmsProtminfos(); iter.hasNext();)
         {
            oldAmsProtminfos = (AmsProtminfos)iter.next();
            iter.remove();
            oldAmsProtminfos.setAmsProjectinfos((AmsProjectinfos)null);
         }
      }
   }
   /** @pdGenerated default getter */
   public java.util.Collection<AmsProtmDetail> getAmsProtmDetail() {
      if (amsProtmDetail == null)
         amsProtmDetail = new java.util.HashSet<AmsProtmDetail>();
      return amsProtmDetail;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorAmsProtmDetail() {
      if (amsProtmDetail == null)
         amsProtmDetail = new java.util.HashSet<AmsProtmDetail>();
      return amsProtmDetail.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newAmsProtmDetail */
   public void setAmsProtmDetail(java.util.Collection<AmsProtmDetail> newAmsProtmDetail) {
      removeAllAmsProtmDetail();
      for (java.util.Iterator iter = newAmsProtmDetail.iterator(); iter.hasNext();)
         addAmsProtmDetail((AmsProtmDetail)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newAmsProtmDetail */
   public void addAmsProtmDetail(AmsProtmDetail newAmsProtmDetail) {
      if (newAmsProtmDetail == null)
         return;
      if (this.amsProtmDetail == null)
         this.amsProtmDetail = new java.util.HashSet<AmsProtmDetail>();
      if (!this.amsProtmDetail.contains(newAmsProtmDetail))
      {
         this.amsProtmDetail.add(newAmsProtmDetail);
         newAmsProtmDetail.setAmsProjectinfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldAmsProtmDetail */
   public void removeAmsProtmDetail(AmsProtmDetail oldAmsProtmDetail) {
      if (oldAmsProtmDetail == null)
         return;
      if (this.amsProtmDetail != null)
         if (this.amsProtmDetail.contains(oldAmsProtmDetail))
         {
            this.amsProtmDetail.remove(oldAmsProtmDetail);
            oldAmsProtmDetail.setAmsProjectinfos((AmsProjectinfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllAmsProtmDetail() {
      if (amsProtmDetail != null)
      {
         AmsProtmDetail oldAmsProtmDetail;
         for (java.util.Iterator iter = getIteratorAmsProtmDetail(); iter.hasNext();)
         {
            oldAmsProtmDetail = (AmsProtmDetail)iter.next();
            iter.remove();
            oldAmsProtmDetail.setAmsProjectinfos((AmsProjectinfos)null);
         }
      }
   }
   /** @pdGenerated default getter */
   public java.util.Collection<Buginfos> getBuginfos() {
      if (buginfos == null)
         buginfos = new java.util.HashSet<Buginfos>();
      return buginfos;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorBuginfos() {
      if (buginfos == null)
         buginfos = new java.util.HashSet<Buginfos>();
      return buginfos.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newBuginfos */
   public void setBuginfos(java.util.Collection<Buginfos> newBuginfos) {
      removeAllBuginfos();
      for (java.util.Iterator iter = newBuginfos.iterator(); iter.hasNext();)
         addBuginfos((Buginfos)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newBuginfos */
   public void addBuginfos(Buginfos newBuginfos) {
      if (newBuginfos == null)
         return;
      if (this.buginfos == null)
         this.buginfos = new java.util.HashSet<Buginfos>();
      if (!this.buginfos.contains(newBuginfos))
      {
         this.buginfos.add(newBuginfos);
         newBuginfos.setAmsProjectinfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldBuginfos */
   public void removeBuginfos(Buginfos oldBuginfos) {
      if (oldBuginfos == null)
         return;
      if (this.buginfos != null)
         if (this.buginfos.contains(oldBuginfos))
         {
            this.buginfos.remove(oldBuginfos);
            oldBuginfos.setAmsProjectinfos((AmsProjectinfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllBuginfos() {
      if (buginfos != null)
      {
         Buginfos oldBuginfos;
         for (java.util.Iterator iter = getIteratorBuginfos(); iter.hasNext();)
         {
            oldBuginfos = (Buginfos)iter.next();
            iter.remove();
            oldBuginfos.setAmsProjectinfos((AmsProjectinfos)null);
         }
      }
   }
   /** @pdGenerated default getter */
   public java.util.Collection<Taskinfos> getTaskinfos() {
      if (taskinfos == null)
         taskinfos = new java.util.HashSet<Taskinfos>();
      return taskinfos;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorTaskinfos() {
      if (taskinfos == null)
         taskinfos = new java.util.HashSet<Taskinfos>();
      return taskinfos.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newTaskinfos */
   public void setTaskinfos(java.util.Collection<Taskinfos> newTaskinfos) {
      removeAllTaskinfos();
      for (java.util.Iterator iter = newTaskinfos.iterator(); iter.hasNext();)
         addTaskinfos((Taskinfos)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newTaskinfos */
   public void addTaskinfos(Taskinfos newTaskinfos) {
      if (newTaskinfos == null)
         return;
      if (this.taskinfos == null)
         this.taskinfos = new java.util.HashSet<Taskinfos>();
      if (!this.taskinfos.contains(newTaskinfos))
      {
         this.taskinfos.add(newTaskinfos);
         newTaskinfos.setAmsProjectinfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldTaskinfos */
   public void removeTaskinfos(Taskinfos oldTaskinfos) {
      if (oldTaskinfos == null)
         return;
      if (this.taskinfos != null)
         if (this.taskinfos.contains(oldTaskinfos))
         {
            this.taskinfos.remove(oldTaskinfos);
            oldTaskinfos.setAmsProjectinfos((AmsProjectinfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllTaskinfos() {
      if (taskinfos != null)
      {
         Taskinfos oldTaskinfos;
         for (java.util.Iterator iter = getIteratorTaskinfos(); iter.hasNext();)
         {
            oldTaskinfos = (Taskinfos)iter.next();
            iter.remove();
            oldTaskinfos.setAmsProjectinfos((AmsProjectinfos)null);
         }
      }
   }

}