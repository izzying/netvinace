/***********************************************************************
 * Module:  SystemConfigParameters.java
 * Author:  Administrator
 * Purpose: Defines the Class SystemConfigParameters
 ***********************************************************************/

package com.amos.main.config;

import java.util.*;

import com.amos.main.entry.CommandDefine;

/** 系统主配置文件参数值类
 * 
 * @pdOid d59a55ac-6077-4927-b86b-44e07af6e8a8 */
public class SystemConfigParameters {

//  
   /** 系统日志目录
    * 
    * @pdOid 58259b8d-ba3b-46ff-b01e-2dd414d357cf */
   public static String SYSTRM_LOGS_FOLDER;
   /** 系统默认日期格式
    * 
    * @pdOid 84d1e8e7-a3f0-4766-808c-df79b482274a */
   public static String SYSTEM_DEFAULT_DATE_FORMAT;
   /** 系统默认时间格式
    * 
    * @pdOid 159f1239-1053-460c-9e1c-29b5acc7aac9 */
   public static String SYSTEM_DEFAULT_TIME_FORMAT;
   
   public static String APP_Kernel_Version;
   
   public static String APP_ROOT_PATH=System.getProperty("user.dir");
   
   public static Map<String,CommandDefine> commandList=new HashMap<String,CommandDefine>(0);
   
   
   
   
  // /** 业务组件加载器类路径
//  * 
//  * @pdOid 8055c5c3-d368-4727-a798-b76a0b240f99 */
// public static String BUSINESS_CLASSES_LOADER;
//   /** 系统业务组件配置文件列表
//    * 
//    * @pdOid 897e7524-bfe4-4c30-ab80-ce0672521435 */
//   public static java.util.List BUSINESS_CLASSES_CONFIG_FILES;
//   /** 数据库连接池初始化大小
//    * 
//    * @pdOid f6aec281-5fab-4799-afd1-b3b3e18e8c82 */
//   public static int DATA_BASE_CONNECTION_POOL_INIT_SIZE;
//   /** 数据库连接池递增大小
//    * 
//    * @pdOid 3477a3b5-e657-4573-b1e6-a2994dc0e14d */
//   public static int DATA_BASE_CONNECTION_POOL_INCREATMENT;
   
//   /** 数据库连接URL
//    * 
//    * @pdOid be1e2282-5846-4740-8b4e-d5f7411bc41f */
//   public static String DATA_BASE_URL;
//   /** 数据库驱动类路径
//    * 
//    * @pdOid 227b2166-c33e-4209-af36-e9c3f9cd8e12 */
//   public static String DATA_BASE_DRIVER_CLASS;
   
   
   

}