/***********************************************************************
 * Module:  SystemConfigLoader.java
 * Author:  Administrator
 * Purpose: Defines the Class SystemConfigLoader
 ***********************************************************************/

package com.amos.main.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.amos.main.entry.CommandDefine;
import com.amos.main.log.SystemLogger;
import com.amos.main.tools.DateUtil;

/** 系统配置文件加载组件类
 * 
 * @pdOid be73ff47-8019-4a90-b765-cdaa36c47b0a */
public  class SystemConfigLoader implements ISystemConfigLoader {
	
	public static Logger logger=LogManager.getLogManager().getLogger("SystemConfigLoader.class");
	private static String defaultConfigFile="./config/systemConfig.properties";
	private static File config;
//	private SystemLogger sysLogger=new SystemLoggerImpl();
	private String logFileUrl;
	
	
   public SystemConfigLoader(String confiFile) {
		// TODO Auto-generated constructor stub
	   defaultConfigFile=confiFile;
	}

public SystemConfigLoader() {
	defaultConfigFile="./config/systemConfig.properties";
}

/** 以文件方式加载用户指定配置文件，当加载失败时，抛出异常并加载系统默认配置文件，
    * 
    * @param configFile
    * @pdOid dbc9611b-e2cd-43d8-a931-973424668623 */
   @Override
public boolean loadConfigFile(java.io.File configFile) {
      // TODO: implement
	    config=configFile;
	    
      return loadConfigFile();
   }
   
   /** 以文件路径方式加载用户指定配置文件，当加载失败时，抛出异常并加载系统默认配置文件，
    * 
    * @param configFileURL
    * @pdOid 0ddc9dec-99bc-4b8e-bdeb-6267a1b61a28 */
   @Override
public boolean loadConfigFile(java.lang.String configFileURL) {
      // TODO: implement
	   defaultConfigFile=configFileURL;
	   return loadConfigFile();
      
   }
   
   /** 加载系统默认配置文件
    * 
    * @pdOid f74cccf2-5559-45af-b92f-220bb34c601a */
   @Override
public boolean loadConfigFile() {
      // TODO: implement
	  
	    config=new File(defaultConfigFile);
	    System.out.println(config.getAbsolutePath());
	    return configIsExists();
   }
   
  
   
   private boolean configIsExists(){
	   if(!config.exists()){
		    new RuntimeException("configFile is not exists.");
		   return false;
	   }
	   else{
		   return true;
	   }
   }
   
   
   
   /**
    * 
    * @param args
    * 系统配置文件检索/加载程序
    */
   public static void main(String[] args) {
	   SystemConfigLoader configLoader;
	   
	   if(args.length==1){
		    configLoader=new SystemConfigLoader(args[0]);
	   }
	   else if(args.length>1){
		  throw new RuntimeException("system startup argument error..");
	   }
	   else{
		   
		    configLoader=new SystemConfigLoader(); 
	   }
	  
	   if(!configLoader.loadConfigFile()){
		   Logger.getLogger("SystemConfigLoader.class").info("config file has not found..");
		   Logger.getLogger("SystemConfigLoader.class").info("System is going to shutdown..");
		   System.exit(-1);
	   }
	   else{
		   Logger.getLogger("SystemConfigLoader.class").info("config file has been found..");
		   Logger.getLogger("SystemConfigLoader.class").info("System continue to loading");
	   }
  
   
}
   
   
	public static String getDefaultConfigFile() {
		return defaultConfigFile;
	}

	public static void setDefaultConfigFile(String defaultConfigFile) {
		SystemConfigLoader.defaultConfigFile = defaultConfigFile;
	}

	public static File getConfig() {
		return config;
	}

	public static void setConfig(File config) {
		SystemConfigLoader.config = config;
	}

	@Override
	public void loadConfigParmeters() {
	  
		InputStream fin;
		try {
			fin = new FileInputStream(config);
			InputStreamReader freader=new InputStreamReader(fin);
			Properties props=new Properties();
			props.load(freader);
			
			SystemConfigParameters.SYSTRM_LOGS_FOLDER=props.getProperty("SYSTRM_LOGS_FOLDER","log");
			SystemConfigParameters.SYSTEM_DEFAULT_DATE_FORMAT=props.getProperty("SYSTEM_DEFAULT_DATE_FORMAT","YYYY-MM-dd");	
			SystemConfigParameters.SYSTEM_DEFAULT_TIME_FORMAT=props.getProperty("SYSTEM_DEFAULT_TIME_FORMAT","HH:mm:ss");
			SystemConfigParameters.APP_Kernel_Version=props.getProperty("APP.Kernel.Version","-1");
			if("".endsWith(SystemConfigParameters.APP_Kernel_Version)||"-1".endsWith(SystemConfigParameters.APP_Kernel_Version)){
				Logger.getLogger("SystemConfigParameters.class").info("Unknow System Kernel Version.");
				System.exit(-1);
			}
			
				
				logFileUrl=SystemConfigParameters.SYSTRM_LOGS_FOLDER+"\\";
				String fileName=DateUtil.getNowDateStr()+".log";
				logFileUrl+=fileName;
				File logFile=new File(logFileUrl);
				if(!logFile.exists()){
					logFile.createNewFile();
				}
				SystemLogger.setLogFile(logFile);
				
			Logger.getLogger("SystemConfigParameters.class").info("System paremeters load successfully..");
			CommandDefine cmd=new CommandDefine("login");
			
			cmd.setCmdStr("user-login");
			cmd.setClassName("login");
			
			SystemConfigParameters.commandList.put(cmd.getCmdStr(), cmd);
			cmd.setClassName("logout");
			cmd.setCmdStr("user-logout");
			SystemConfigParameters.commandList.put(cmd.getCmdStr(), cmd);
			cmd.setClassName("listUser");
			cmd.setCmdStr("list-user");
			SystemConfigParameters.commandList.put(cmd.getCmdStr(), cmd);
			System.out.println("command size:"+SystemConfigParameters.commandList.size());
			showCommand();
		} catch (FileNotFoundException e) {
			Logger.getLogger("SystemConfigLoader.class").info(e.getCause().getMessage());
			System.exit(-1);
			
		} catch (IOException e) {
			Logger.getLogger("SystemConfigLoader.class").info(e.getCause().getMessage());
			System.exit(-1);
		}
		
				
		
	}

	private void showCommand() {
		Iterator it= SystemConfigParameters.commandList.entrySet().iterator();
		while(it.hasNext()){
			Entry cmd=(Entry) it.next();
			System.out.println(cmd.getKey()+"="+cmd.getValue());
		}
		
	}

}