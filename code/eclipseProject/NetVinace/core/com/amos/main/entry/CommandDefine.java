package com.amos.main.entry;

import java.lang.reflect.Method;

import org.apache.mina.core.session.IoSession;

public class CommandDefine implements IEntry {

	private Class clz;
	
	private String className;
	
	private Method method;
	
	private String method_name;
	
	private String cmdStr;
	
	private IoSession session;
	
	
	
	public IoSession getSession() {
		return session;
	}

	
	public void setSession(IoSession session) {
		this.session = session;
	}

	public Method getMethod() {
		return method;
	}

	public void setMethod(Method method) {
		this.method = method;
	}

	public String getMethod_name() {
		return method_name;
	}

	public void setMethod_name(String method_name) {
		this.method_name = method_name;
	}

	public CommandDefine(String cmdStr) {
		super();
		this.cmdStr = cmdStr;
	}

	public Class getClz() {
		return clz;
	}

	public void setClz(Class clz) {
		this.clz = clz;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getCmdStr() {
		return cmdStr;
	}

	public void setCmdStr(String cmdStr) {
		this.cmdStr = cmdStr;
	}
	
	

}
