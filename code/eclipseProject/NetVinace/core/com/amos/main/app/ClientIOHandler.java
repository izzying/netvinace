package com.amos.main.app;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;

import com.amos.main.entry.CommandDefine;
import com.amos.main.log.SystemLogger;
import com.amos.main.log.SystemLoggerImpl;
import com.amos.main.tools.DateUtil;

public class ClientIOHandler extends IoHandlerAdapter {

	private SystemLogger logger=new SystemLoggerImpl();
	private CommandDefine command;
	
	protected ClientApp client;
	public ClientIOHandler(CommandDefine command) {
		// TODO Auto-generated constructor stub
		this.command=command;
	}

	

	public CommandDefine getCommand() {
		return command;
	}



	public ClientIOHandler(CommandDefine cmd,ClientApp client) {
		super();
		this.command = cmd;
		this.client=client;
	}

	

	@Override
	public void messageReceived(IoSession session, Object message)
			throws Exception {
		if (command instanceof CommandDefine) {  
			CommandDefine cmd = (CommandDefine) command;
			String clientIp = session.getRemoteAddress().toString();
			String log_msg = clientIp + cmd.getCmdStr();
			
			

			if (cmd.getCmdStr().endsWith("quit")) {
				session.close(true);
				return;
			}
			System.out.println("new command is " + cmd.getCmdStr().toString()
					+ " send by:" + clientIp);
			session.write("hello,client");
			}

	}

	
	@Override
	public void messageSent(IoSession session, Object message)
			throws Exception {
		// TODO Auto-generated method stub
		System.out.println("message sent..");

	}

	@Override
	public void sessionClosed(IoSession session) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("client close..");

	}

	@Override
	public void sessionCreated(IoSession session) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("client create..");
		session.setAttribute("Client", client);

	}

	@Override
	public void sessionOpened(IoSession session) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("client open.." + command.getCmdStr());
		session.setAttribute("Client", this);

		session.write(command);

	}
}
