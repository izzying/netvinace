package com.amos.main.app;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;

import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.serialization.ObjectSerializationCodecFactory;
import org.apache.mina.filter.codec.textline.LineDelimiter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import com.amos.main.entry.CommandDefine;
import com.amos.main.service.ServiceUtil;
import com.amos.main.tools.DateUtil;

public class ServerApp implements ServiceUtil {

	private IoAcceptor acceptor;
	private int port;
	private static long onlineUser = 0;
	private Logger logger = LoggerFactory.getLogger(ServerApp.class);
	private Map<String, ArrayList<String>> user_Log_List = new HashMap<String, ArrayList<String>>(
			0);

	public long getOnlineUser() {
		return onlineUser;
	}

	public void setOnlineUser(long onlineUser) {
		ServerApp.onlineUser = onlineUser;
	}

	public IoAcceptor getAcceptor() {
		return acceptor;
	}

	public void setAcceptor(IoAcceptor acceptor) {
		this.acceptor = acceptor;
	}

	public ServerApp(int port) {
		// TODO Auto-generated constructor stub
		this.port = port;
	}

	public ServerApp() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	private void init() {
		acceptor = new NioSocketAcceptor();
		acceptor.getSessionConfig().setReadBufferSize(2048);
		acceptor.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, 10);
		acceptor.setHandler(new ServerIOHandler());

		acceptor.getFilterChain().addLast("logger", new LoggingFilter());
		acceptor.getFilterChain().addLast("codec",
				new ProtocolCodecFilter(new ObjectSerializationCodecFactory()));
	}

	private boolean startServer() {
		try {
			acceptor.bind(new InetSocketAddress(port));
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	class ServerIOHandler extends IoHandlerAdapter {

		// 这里我们使用的SLF4J作为日志门面，至于为什么在后面说明。
		private final Logger log = LoggerFactory
				.getLogger(ServerIOHandler.class);

		@Override
		public void sessionCreated(IoSession session) throws Exception {
			// TODO Auto-generated method stub
			System.out.println("Server: server session is created..");

			String clientIp = session.getRemoteAddress().toString();
			if (!user_Log_List.containsKey(clientIp)) {
				onlineUser++;
				session.setAttribute("onlineUser", onlineUser);
				String logInfo = DateUtil.getNowDateStr() + " "
						+ DateUtil.getNowTimeStr() + "session created.";
				ArrayList<String> logList = new ArrayList<String>(0);
				logList.add(logInfo);
				user_Log_List.put(clientIp, logList);
				System.out.println("new user is come:"
						+ session.getRemoteAddress().toString());

			} else {
				String logInfo = DateUtil.getNowDateStr() + " "
						+ DateUtil.getNowTimeStr() + "session created.";
				ArrayList<String> logList = user_Log_List.get(clientIp);
				logList.add(logInfo);
				user_Log_List.put(clientIp, logList);
			}

			System.out.println("now online user is"
					+ session.getAttribute("onlineUser"));

		}

		@Override
		public void sessionClosed(IoSession session) throws Exception {

			String clientIp = session.getRemoteAddress().toString();
			if (user_Log_List.containsKey(clientIp)) {
				user_Log_List.remove(clientIp);
				onlineUser--;
			}
			session.setAttribute("onlineUser", onlineUser);
			System.out.println("now online user is"
					+ session.getAttribute("onlineUser"));
		}

		@Override
		public void messageReceived(IoSession session, Object command)
				throws Exception {
			if (command instanceof CommandDefine) {  
			CommandDefine cmd = (CommandDefine) command;
			String clientIp = session.getRemoteAddress().toString();
			String log_msg = clientIp + cmd.getCmdStr();
			
			addLog(clientIp, log_msg);
			
			session.setAttribute("onlineUser", onlineUser);

			if (cmd.getCmdStr().endsWith("quit")) {
				session.close(true);
				return;
			}
			System.out.println("new command is " + cmd.getCmdStr().toString()
					+ " send by:" + clientIp);
			session.write("hello,client");
			}
			
		}

		private void addLog(String clientIp, String logInfor) {
			ArrayList<String> logList = user_Log_List.get(clientIp);
			logList.add(logInfor);
			user_Log_List.put(clientIp, logList);
			System.out.println("Log" + logInfor + "is add to ip " + clientIp);
		}

	};

	@Override
	public boolean executeService(Object[] parms) {
		// TODO Auto-generated method stub
		init();
		return startServer();
	}

}
