package com.amos.main.app;

import java.net.InetSocketAddress;

import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.future.IoFutureListener;
import org.apache.mina.core.service.IoConnector;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.serialization.ObjectSerializationCodecFactory;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import com.amos.main.config.SystemConfigParameters;
import com.amos.main.entry.CommandDefine;
import com.amos.main.service.ServiceUtil;

public class ClientApp implements ServiceUtil {

	private String serverIp;
	private int serverPort;
	private IoConnector connector;
	private ConnectFuture future;
	private ClientApp client;
	private CommandDefine command;
	private String ipAddr;

	public ClientApp() {
		// TODO Auto-generated constructor stub
	}

	public ClientApp(String serverIp, int serverPort) {

		this.serverIp = serverIp;
		this.serverPort = serverPort;

	}

	@SuppressWarnings("deprecation")
	private boolean init(String message) {
		System.out.println("message=" + message);
		String cmd = getCommandByType(message);
		if ("Bad command".equals(cmd)) {
			System.out.println("Input the Bad command..");

			return false;
		} else {
			System.out.println("build command");
			command = new CommandDefine(message);

			connector = new NioSocketConnector();

			ClientIOHandler clientHandler = new ClientIOHandler(command);
			connector.setHandler(clientHandler);
			connector.setConnectTimeoutMillis(30000);
			connector.getFilterChain().addLast("logger", new LoggingFilter());
			connector.getFilterChain().addLast(
					"codec",
					new ProtocolCodecFilter(
							new ObjectSerializationCodecFactory()));
			return true;
		}
	}

	public boolean startClient(String msg, ClientApp client) {

		if (init(msg)) {

			this.client = client;
			future = connector.connect(new InetSocketAddress(this.serverIp,
					this.serverPort));
			future.addListener(new IoFutureListener<ConnectFuture>() {

				@Override
				public void operationComplete(ConnectFuture future) {
					// TODO Auto-generated method stub
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					IoSession session = future.getSession();

					System.out.println("++++++++++++++++++++");

				}

			});
			System.out.println("*****************************");
			return future.isConnected();
		} else {
			return false;
		}
	}

	class ClientIOHandler extends IoHandlerAdapter {

	};

	public String getServerIp() {
		return serverIp;
	}

	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}

	public int getServerPort() {
		return serverPort;
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	public static void main(String[] args) {
		String connectIp = "127.0.0.1";
		int connectPort = 9000;
		ClientApp client = new ClientApp(connectIp, connectPort);
		client.startClient("user-login", client);

	}

	public String getIpAddr() {
		return ipAddr;
	}

	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}

	private String getCommandByType(String input) {
		String cmd = new String();
		input = input.toLowerCase();
		if ("".equals(input) || input == null
				|| !SystemConfigParameters.commandList.containsKey(input)) {
			cmd = "Bad command";
		} else {
			cmd = SystemConfigParameters.commandList.get(input).toString();
		}
		return cmd;
	}

	@Override
	public boolean executeService(Object[] parms) {
		// TODO Auto-generated method stub

		return true;
	}

}
