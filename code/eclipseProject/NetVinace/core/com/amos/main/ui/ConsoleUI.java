package com.amos.main.ui;

import java.util.HashMap;
import java.util.Map;

import com.amos.main.app.ClientApp;
import com.amos.main.app.ServerApp;
import com.amos.main.config.SystemConfigParameters;
import com.amos.main.entry.CommandDefine;
import com.amos.main.service.IConsoleService;
import com.amos.main.service.ServiceManager;
import com.amos.main.tools.DateUtil;

public class ConsoleUI implements IConsoleService{

	public static Map<String,CommandDefine> cmds=new HashMap<String,CommandDefine>();
	private ServiceManager svrm;
	private static ConsoleUI ui;
	
	
	
	protected ConsoleUI(ServiceManager svrm) {
		// TODO Auto-generated constructor stub
		this.svrm=svrm;
	}
	
	@SuppressWarnings("static-access")
	public static ConsoleUI initUI(ServiceManager svrm) throws InterruptedException{
		ui=new ConsoleUI(svrm);
		System.out.println("Welcome to Net-Vinace Console");
		Thread.currentThread().sleep(2000);
		System.out.println("Vinace Kenrel Version:"+SystemConfigParameters.APP_Kernel_Version);
		Thread.currentThread().sleep(2000);
		System.out.println("Now Date is: "+DateUtil.getNowDateStr()+" "+DateUtil.getNowTimeStr());
		return ui;
		
	}
	
	public ServiceManager getSvrm() {
		return svrm;
	}
	public void setSvrm(ServiceManager svrm) {
		this.svrm = svrm;
	}

	
	@Override
	public boolean startServer(int port) {
		// TODO Auto-generated method stub
		ServerApp server=(ServerApp) ServiceManager.getSystemService("server");
		server.executeService(null);
		System.out.println("Server is started,listening port: "+port);
		return true;
	}

	@Override
	public boolean stopServer() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean startClient() {
		// TODO Auto-generated method stub
		ClientApp client=(ClientApp) ServiceManager.getSystemService("client");
		client.startClient("user-login",client);
		return true;
	}

}
