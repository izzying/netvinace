package com.amos.main.tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.amos.main.config.SystemConfigParameters;
/**
 * 通用日期/时间处理类（待完善）
 * 
 * @author Administrator
 *
 */
public class DateUtil {

	private static SimpleDateFormat formatter;
	
	private final static String dataformatStr=SystemConfigParameters.SYSTEM_DEFAULT_DATE_FORMAT;
	
	private final static String timeformatStr=SystemConfigParameters.SYSTEM_DEFAULT_TIME_FORMAT;
	
	private final String dataTimeformatStr=SystemConfigParameters.SYSTEM_DEFAULT_DATE_FORMAT+" "+SystemConfigParameters.SYSTEM_DEFAULT_TIME_FORMAT;;		
	
	public static Date getDateFromStr(String dateStr){
		try {
			formatter=new SimpleDateFormat(dataformatStr);
			Date date=formatter.parse(dateStr);
			return date;
		} catch (ParseException e) {
			
			System.err.println(e.getCause().getMessage());
			e.printStackTrace();
			return null;
		}
		
	}
	
	public static String getDateStr(Date date){
		formatter=new SimpleDateFormat(dataformatStr);
		String datStr=formatter.format(date).toString();
		return datStr;
		
	}
	public static Calendar getCalendar(){
		Calendar calendar=Calendar.getInstance();
		return calendar;
	}
	
	public static String getNowDateStr(){
		Date date=new Date();
		formatter=new SimpleDateFormat(dataformatStr);
		String datStr=formatter.format(date).toString();
		return datStr;
		
	}
	
	public static String getNowTimeStr(){
		Date date=new Date();
		formatter=new SimpleDateFormat(timeformatStr);
		String datStr=formatter.format(date).toString();
		return datStr;
		
	}
	
	public static String getNowDateStr(Date date,String dateFormat){
		
		formatter=new SimpleDateFormat(dateFormat);
		String datStr=formatter.format(date).toString();
		return datStr;
		
	}
	
	public  static String getNowTimeStr(Date date,String timeFormate){
		
		formatter=new SimpleDateFormat(timeFormate);
		String datStr=formatter.format(date).toString();
		return datStr;
		
	}

	// Getter Methods
	
	public SimpleDateFormat getFormatter() {
		return formatter;
	}

	public void setFormatter(SimpleDateFormat formatter) {
		DateUtil.formatter = formatter;
	}

	public String getDataformatStr() {
		return dataformatStr;
	}

	public String getTimeformatStr() {
		return timeformatStr;
	}

	public String getDataTimeformatStr() {
		return dataTimeformatStr;
	}
	
	

	// private constructor
	
	private DateUtil() {
	
	}

}
