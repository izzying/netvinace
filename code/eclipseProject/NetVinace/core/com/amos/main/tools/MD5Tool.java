package com.amos.main.tools;

import java.io.File;
import java.io.FileInputStream;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;

public class MD5Tool {
	
	private static MessageDigest messagedigest;

	public final static String MD5ForString(String s){ 
		char hexDigits[] = { 
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 
		'e', 'f'}; 
		try { 
		byte[] strTemp = s.getBytes(); 
		MessageDigest mdTemp = MessageDigest.getInstance("MD5"); 
		mdTemp.update(strTemp); 
		byte[] md = mdTemp.digest(); 
		int j = md.length; 
		char str[] = new char[j * 2]; 
		int k = 0; 
		for (int i = 0; i < j; i++) { 
		byte byte0 = md[i]; 
		str[k++] = hexDigits[byte0 >>> 4 & 0xf]; 
		str[k++] = hexDigits[byte0 & 0xf]; 
		} 
		return new String(str); 
		} 
		catch (Exception e){ 
		return null; 
		} 
		} 
	
	
	
	public final static String MD5ForFile(String url){ 
		char hexDigits[] = { 
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 
		'e', 'f'}; 
		try { 
		
		 File file=new File(url);
		FileInputStream in = new FileInputStream(file);
		  FileChannel ch =in.getChannel();
		  
		MessageDigest mdTemp = MessageDigest.getInstance("MD5"); 
		MappedByteBuffer byteBuffer =ch.map(FileChannel.MapMode.READ_ONLY, 0, file.length());
		  messagedigest.update(byteBuffer);
		  return bufferToHex(messagedigest.digest());
		} 
		
		catch (Exception e){ 
		return null; 
		} 
	}
		public static String getMD5String(byte[] bytes){
			  messagedigest.update(bytes);
			  return bufferToHex(messagedigest.digest());
			}

			private static String bufferToHex(byte bytes[]){
			  return bufferToHex(bytes, 0,bytes.length);
			}

	private static String bufferToHex(byte bytes[], int m, int n) {
		StringBuffer stringbuffer = new StringBuffer(2 * n);
		int k = m + n;
		return null;

	}
		
}
