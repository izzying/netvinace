/***********************************************************************
 * Module:  DatabaseConnectionFactory.java
 * Author:  Administrator
 * Purpose: Defines the Class DatabaseConnectionFactory
 ***********************************************************************/

package com.amos.main.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Vector;

/**
 * 数据库连接池类 copy from
 * :http://www.360doc.com/content/11/0808/16/2159920_138942663.shtml
 * 
 * @pdOid cdedddfe-67b5-47d1-8071-78536dacdc29
 */
public class DatabaseConnectionFactory {

	protected static DatabaseConnectionFactory instance;

	/** @pdOid e6f03579-948b-40b0-997b-3ca4f1b303af */
	protected static java.util.Vector connectionFree;

	protected static HashMap<Long, Connection> connectionUsed = new HashMap<Long, Connection>(
			0);

	protected static int maxSize = 300;

	protected static int increatment = 10;

	protected static int initSize = 10;

	protected static int conUsed = 0;

	private static String driverName = "com.mysql.jdbc.Driver";

	protected static String username = "root";

	protected static String url = "jdbc:mysql://127.0.0.1:3306/netvinace";

	protected static String password = "201441";

	protected ISystemCommonDao systemDao = new SystemCommonDaoImpl();

	private DatabaseConnectionFactory() {

	}

	/** @pdOid 3de35a8d-29a5-4889-9020-fe5ee39fe7eb */
	private synchronized static Connection getNewDBConnection() {
		// TODO: implement
		try {
			Class.forName(driverName);

			Connection dbcon = DriverManager.getConnection(url, username,
					password);
			return dbcon;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @param uid
	 * 
	 * @pdOid 9418947d-1dc8-448d-9728-d98345dce276
	 */
	public synchronized int freeConnection(long uid) {
		// TODO: implement
		Iterator it = connectionUsed.keySet().iterator();
		if (connectionUsed.containsKey(uid)) {
			while (it.hasNext()) {
				long ukey = (long) it.next();
				if (uid == ukey) {
					Connection con = connectionUsed.get(uid);
					try {
						if (!con.isClosed()) {
							connectionFree.addElement(con);
							System.out.println(uid
									+ "connection free successful!");
							System.out.println("connectionFree size is"
									+ connectionFree.size());
						} else {
							con = null;
						}
						break;
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			connectionUsed.remove(uid);
			conUsed--;
			notifyAll();

			return conUsed;
		}
		return conUsed;
	}

	/**
	 * @param uid
	 * @pdOid a8d357f8-9b05-4662-b6f8-c7858c7f2e14
	 */
	public synchronized static int disConnectDB(long uid) {
		// TODO: implement

		if (connectionUsed.containsKey(uid)) {
			Connection conn = connectionUsed.get(uid);
			try {
				if (!conn.isClosed()) {
					conn.close();
				}
				connectionUsed.remove(uid);
				conUsed--;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return connectionUsed.size();
	}

	public static java.util.Vector getConnectionFree() {
		return connectionFree;
	}

	public static HashMap<Long, Connection> getConnectionUsed() {
		return connectionUsed;
	}

	public static int getConUsed() {
		return conUsed;
	}

	/**
	 * @param poolSize
	 * @pdOid 715b0c97-9c65-4982-a7a9-f632afa126af
	 */
	public synchronized static DatabaseConnectionFactory initConnectionPool() {

		// SystemConfigParameters.DATA_BASE_CONNECTION_POOL_INIT_SIZE;
		connectionFree = new Vector<Connection>(initSize);

		connectionUsed = new HashMap<Long, Connection>(0);
		if (instance == null) {
			instance = new DatabaseConnectionFactory();
		}
		return instance;

	}

	public synchronized static DatabaseConnectionFactory initConnectionPool(
			int initSize, int maxSize, int increment) {

		// SystemConfigParameters.DATA_BASE_CONNECTION_POOL_INIT_SIZE;
		DatabaseConnectionFactory.initSize = initSize;

		DatabaseConnectionFactory.maxSize = maxSize;

		increatment = increment;

		connectionFree = new Vector<Connection>(initSize);

		connectionUsed = new HashMap<Long, Connection>(0);

		if (instance == null) {
			instance = new DatabaseConnectionFactory();
		}
		return instance;

	}

	/** @pdOid 44534d35-0237-4060-a767-9b162dc91d34 */
	public Vector getConnectionPool() {
		return connectionFree;
	}

	/**
	 * @param newConnectionPool
	 * @pdOid 49965dbc-e59d-49d9-8db9-f0cf7318d682
	 */
	public void setConnectionPool(Vector newConnectionPool) {
		connectionFree = newConnectionPool;
	}

	/**
	 * @param uid
	 * @pdOid 9450ac2f-b13e-4a12-b4b7-cf81b4430b74
	 */
	public synchronized Connection getDBConnection(long uid) {
		Connection conn = null;
		try {
			if (connectionFree.size() > 0) {
				conn = (Connection) connectionFree.firstElement();
				connectionFree.removeElementAt(0);

				if (conn.isClosed()) {
					getDBConnection(uid);
				}

			} else if (maxSize == 0 || conUsed < maxSize) {
				conn = getNewDBConnection();
			}

			if (conn != null) {
				conUsed++;
				connectionUsed.put(uid, conn);
			}

			return conn;
		} catch (SQLException e) {
			conn = getDBConnection(uid);
			e.printStackTrace();
			return conn;
		}

	}

	public synchronized void disconnectAll() {

		Iterator<Entry<Long, Connection>> it = connectionUsed.entrySet()
				.iterator();
		while (it.hasNext()) {
			Entry entry = it.next();
			Connection con = (Connection) entry.getValue();
			try {
				if (!con.isClosed()) {
					con.close();
				}
				con = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		connectionUsed.clear();

	}

	public ISystemCommonDao getSystemDao() {
		return systemDao;
	}

	public void setSystemDao(ISystemCommonDao systemDao) {
		this.systemDao = systemDao;
	}

}