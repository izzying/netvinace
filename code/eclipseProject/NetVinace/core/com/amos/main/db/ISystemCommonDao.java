package com.amos.main.db;

import java.util.List;

import com.amos.main.entry.IEntry;
import com.amos.main.tools.PageUtil;

public interface ISystemCommonDao {

	public  int saveData(IEntry entry);

	/**
	 * 
	 * @param entry
	 * @return int
	 * @category 更新数据
	 */

	public  int updateData(IEntry entry);

	/**
	 * 
	 * @param entryClass
	 * @param id
	 * @return int
	 * @category 删除单条数据
	 */

	public  int deleteData(Class entryClass, String id);

	/**
	 * @author Administrator
	 * @category 批量删除数据
	 * @param entryClass,ids
	 * @return int
	 * @exception Exception
	 * @since 1.0.0   2011-01-19
	 */

	public  int deleteDatas(Class entryClass, String[] ids);

	/**
	 * 
	 * @param entryClass
	 * @param pageNo
	 * @param pageSize
	 * @return PageUtil
	 * @category DAO查询分页方法
	 */
	public  PageUtil queryAllData(Class entryClass, int pageNo,
			int pageSize);

	/**
	 * @category DAO查询非分页方法
	 * @param entryClass
	 * @return
	 */
	public  List<IEntry> queryAllData(Class entryClass);

	public  IEntry querySingleData(Class entryClass,
			String id);
	
	public  IEntry querySingleDataByCondition(Class entryClass,
			String condition);

	/**
	 * 
	 * @param condition
	 * @param entryClass
	 * @return PageUtil
	 * @category DAO条件查询分页方法
	 */

	public  PageUtil queryDataByCondition(String condition,
			Class entryClass, int pageNo, int pageSize);

	/**
	 * 
	 * @param condition
	 * @param entryClass
	 * @return
	 * @category DAO条件查询非分页方法
	 */
	public  List<IEntry> queryDataByCondition(
			String condition, Class entryClass, boolean distinct);

	/**
	 * 
	 * @param sql
	 * @return  List
	 * @category SQL查询方法
	 */

	public  List queryDataBySQL(String sql);

	/**
	 * 
	 * @param sql
	 * @return  List
	 * @category SQL更新方法
	 */

	public  void updateDataBySQL(String sql);

}