/***********************************************************************
 * Module:  SystemLogger.java
 * Author:  Administrator
 * Purpose: Defines the Class SystemLogger
 ***********************************************************************/

package com.amos.main.log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.amos.main.config.SystemConfigParameters;

/** @pdOid 3dd1ff4f-5468-4c78-9aa4-81cf9dccc6f5 */
public abstract class SystemLogger implements ISystemLogger {
	
	protected FileWriter fous;
	protected FileReader fins;
	
	protected static File logFile;


	@Override
	public boolean isLogFolderExists() {
		// TODO Auto-generated method stub
		 File logDir= new File(SystemConfigParameters.SYSTRM_LOGS_FOLDER);
		  if(!logDir.exists()){
			  System.err.println("log folder is not exists..");
			  System.out.println("creating system Log folder..");
			  boolean iscreate=createLogFolder();
			  if(iscreate){
				  return true;
			  }
			  return  iscreate;
		  }
		return true;
		
	}





	@Override
	public File createLogFile(String logFileUrl) {
		 logFile=new File(logFileUrl);
		System.out.println(logFile.getAbsolutePath());
		if(!logFile.exists()){
			try {
				logFile.createNewFile();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		setLogFile(logFile);
		return logFile;
	}

	
	
	
	
   /** 写日志文件，把日志内容写入系统当前日志文件中。
    * 
    * @param context
    * @pdOid 05c39e87-491c-47fc-afd7-13cf69ad7ce9 */
   @Override
public synchronized int writeLogFile(String context) {
      // TODO: implement
	   String oidContext=readLogFile(logFile).trim();
	   try {
	    fous=new FileWriter(logFile);
	   if(logFile.exists()&&logFile.canWrite()){
		  
		   context=oidContext+"\n"+context+"\n";
		   fous.write(context);
		   System.out.println("write log successful !");
		return 0;
	   }
	   }
	 catch (IOException e) {
		 System.out.println("write log failly !");
		e.printStackTrace();
	}
	   finally{
		   try {
			fous.flush();
			fous.close();
			notifyAll();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	   }
	   
	return -1;
	   
   
   }
   
   /** 把日志内容写入用户指定的日志文件（路径形式）当中。如果写入失败，则把日志内容写入系统当前日志文件中。
    * 
    * @param context 
    * @param logFileURL
    * @pdOid 634a18af-22c9-4171-8de8-82a3522e78f0 */
   @Override
public int writeLogFile(String context, java.lang.String logFileURL) {
	   int sign=writeLogFile(context);
	   if(sign!=0){
		   Logger.getLogger(this.getClass()).error(context+" write log failly..");
	   }
      return sign;
   }
   
   /** 把日志内容写入用户指定的日志文件（文件形式）当中。如果写入失败，则把日志内容写入系统当前日志文件中。
    * 
    * @param context 
    * @param logFile
    * @pdOid f19aa65a-bc51-4f58-b500-7f0c3f10f946 */
   @Override
public int writeLogFile(String context, java.io.File logFile) {
	   int sign=writeLogFile(context);
	   if(sign!=0){
		   Logger.getLogger(this.getClass()).error(context+" write log failly..");
	   }
      return sign;
   }
   
   /** 以路径方式复制，迁移日志文件，支持批量复制。
    * 
    * @param oldFile 
    * @param newFile
    * @pdOid ea6170a5-3979-4a87-8004-9bd4be224a8c */
   @Override
public int copyLogFile(String oldFile, String newFile) {
      // TODO: implement
      return 0;
   }
   
   /** 以文件实体方式迁移，复制日志文件,支持批量复制
    * 
    * @param oldFile 
    * @param newFile
    * @pdOid 59da1d9a-5b04-413b-b791-bb47d3f90d9c */
   @Override
public int copyLogFile(java.io.File oldFile, java.io.File newFile) {
      // TODO: implement
      return 0;
   }
   
   /** 以文件实体方式读取日志文件
    * 
    * @param logFile
    * @pdOid 60ab457a-d444-4608-b2a4-e0c9b04de856 */
   @Override
public synchronized String readLogFile(java.io.File logFile) {
	   StringBuffer oldContext=new StringBuffer();
	   char[] line=new char[1024];
      try {
		fins=new FileReader(logFile);
		// bufReader=new BufferedReader(fins);
		while(fins.read(line)!=-1){
			
			oldContext.append(line);
		}
		
		return oldContext.toString().trim();
		
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
      finally{
    	  try {
			
			fins.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	  
      }
      
      return null;
   }
   
   /** 以文件路径方式读取日志文件
    * 
    * @param logFileURL
    * @pdOid ca0146b8-ea39-4d29-9802-f538cb977578 */
   @Override
public String readLogFile(String logFileURL) {
      // TODO: implement
      return null;
   }
   
   /** 查询日志文件指定内容
    * 
    * @param context 
    * @param logFile
    * @pdOid 11d9e2d8-2fc5-410e-9adf-5b0d03ea32a9 */
   @Override
public String queryLog(String context, java.io.File logFile) {
      // TODO: implement
      return null;
   }
   
   /** 以路径方式查询日志文件内容
    * 
    * @param context 
    * @param logFileURL
    * @pdOid 183f2713-2bf0-469e-acc1-f5a89597a7af */
   @Override
public String queryLog(String context, String logFileURL) {
      // TODO: implement
      return null;
   }
   
   /** 以路径方式更新日志文件内容
    * 
    * @param logFileURL
    * @pdOid 3299acca-98b4-4efb-816a-ab6709d95359 */
   @Override
public int updateLog(String logFileURL) {
      // TODO: implement
      return 0;
   }
   
   /** 以文件实体方式更新日志文件
    * 
    * @param logFile
    * @pdOid e5bfb7eb-c887-4531-9a3e-4b042109e1a1 */
   @Override
public int updateLog(java.io.File logFile) {
      // TODO: implement
      return 0;
   }
   
   /** 查询日志文件列表
    * 
    * @param fileDIR 文件目录
    * @pdOid c81c29d4-e0ea-4858-8edb-930a2105af62 */
   @Override
public java.util.List listLogFiles(String fileDIR) {
      // TODO: implement
      return null;
   }
   
   /** 查询所有日志文件，获取系统日志文件列表
    * 
    * @pdOid 0ca2fc67-2709-4aaf-abe6-b1e387beb9da */
   @Override
public java.util.List listLogFiles() {
      // TODO: implement
	   List<File> logs=new ArrayList<File>(0);
//	  File logDir= new File(SystemConfigParameters.SYSTRM_LOGS_FOLDER);
//	  if(!logDir.exists()){
//		  System.err.println("log folder is not exists..");
//		  createLogFolder();
//	  }
	  return null;
   }
   
   private boolean createLogFolder(){
	   boolean sign=false;
	   File logDir= new File(SystemConfigParameters.SYSTRM_LOGS_FOLDER);
	  
			  try {
				 sign=logDir.createNewFile();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return sign;
			}
			  
			  return sign;
		  
   }
   
   /** 获取特定文件后缀的日志列表
    * 
    * @param filtter
    * @pdOid f7f2aa6c-aa0b-49ab-a4dc-c2b1f9e8e30c */
   @Override
public java.util.List listLogFiles(java.io.FileFilter filtter) {
      // TODO: implement
      return null;
   }
   
   /** 在指定目录下获取特定文件后缀的日志列表（文件路径方法）
    * 
    * @param folderDIR 
    * @param filter
    * @pdOid d7f57f4b-b669-4f18-a0d8-4f531cdb5d51 */
   @Override
public java.util.List listLogFiles(String folderDIR, java.io.FileFilter filter) {
      // TODO: implement
      return null;
   }
   
   /** 在指定目录下获取特定文件后缀的日志列表（文件实体方法）
    * 
    * @param folder 
    * @param filter
    * @pdOid 5347b2c4-b546-4fa9-9deb-2d38c765536b */
   @Override
public java.util.List listLogFiles(java.io.File folder, java.io.FileFilter filter) {
      // TODO: implement
      return null;
   }
   
   public static File getLogFile() {
		return logFile;
	}


	public static void setLogFile(File logFile) {
		SystemLogger.logFile = logFile;
	}


}