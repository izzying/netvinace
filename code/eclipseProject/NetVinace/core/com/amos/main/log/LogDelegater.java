package com.amos.main.log;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import com.amos.main.service.ServiceUtil;
import com.amos.main.tools.DateUtil;
/**
 * 
 * @author Administrator
 *日志写入代理类，AOP实现对ServiceUtil接口实现检测
 */
@Aspect
public class LogDelegater implements ServiceUtil {

	//自己实现的日志类，封装自己想要的日志格式，实现读写日志功能
	private SystemLogger logger=new SystemLoggerImpl();
	
	
	public LogDelegater() {
		// TODO Auto-generated constructor stub
		System.out.println("******loggerDelegate*******"+SystemLogger.getLogFile().getAbsolutePath());
		
	}
	@Before("execution(* *.*(..))")
	public void doBefore(JoinPoint jp){
		String log_msg=jp.toLongString();//获取目标对象方法执行信息
		//DateUtil是自己写的日期时间类，方便使用
		Logger.getLogger("Before"+jp.getTarget().getClass()).info(DateUtil.getNowDateStr()+" "+DateUtil.getNowTimeStr()+" "+log_msg);
		logger.writeLogFile(DateUtil.getNowDateStr()+" "+DateUtil.getNowTimeStr()+" "+log_msg+" Execution..", SystemLogger.getLogFile());
	}
	
	@After("execution(* *.*(..))")
	public void doAfter(JoinPoint jp){
		String log_msg=jp.toLongString();//获取目标对象方法执行信息
		//DateUtil是自己写的日期时间类，方便使用
		Logger.getLogger("Before"+jp.getTarget().getClass()).info(DateUtil.getNowDateStr()+" "+DateUtil.getNowTimeStr()+" "+log_msg);
		logger.writeLogFile(DateUtil.getNowDateStr()+" "+DateUtil.getNowTimeStr()+" "+log_msg+" Finish!", SystemLogger.getLogFile());
	}
	
	/**
	 * 一般业务执行方法，在此未被实现
	 */
	@Override
	public boolean executeService(Object[] parms) {
		// TODO Auto-generated method stub
		return false;
	}

}
