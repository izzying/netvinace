package com.amos.main.service;

public interface IConsoleService {

	/**
	 * 服务端启动接口
	 * @param port
	 * @return boolean
	 */
	public boolean startServer(int port);
	
	/**
	 * 服务端停止接口
	 * @return
	 */
	public boolean stopServer();
	
	
}
