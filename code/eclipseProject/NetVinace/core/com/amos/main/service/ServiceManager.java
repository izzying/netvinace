package com.amos.main.service;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.amos.main.config.SystemConfigParameters;

public class ServiceManager {

	protected static ApplicationContext cxt;
	
	private ServiceManager() {
		cxt= new FileSystemXmlApplicationContext(SystemConfigParameters.APP_ROOT_PATH+"/config/applicationContext.xml");
	}

	public static ServiceManager newInstance() {
		// TODO Auto-generated method stub
		
		ServiceManager svrm=new ServiceManager();
		return svrm;
	}

	public static ApplicationContext getCxt() {
		return cxt;
	}
	
	
	public static ServiceUtil getSystemService(String serviceName){
		
		ServiceUtil service=(ServiceUtil) cxt.getBean(serviceName);
		return service;
	}

}
