package com.amos.test;

import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.amos.main.config.SystemConfigLoader;
import com.amos.main.config.SystemConfigParameters;
import com.amos.main.log.LogDelegater;
import com.amos.main.service.ServiceManager;
import com.amos.main.ui.ConsoleUI;

public class MainTester {

	/**
	 * @param args
	 */
	private static Logger logger = LogManager.getLogManager().getLogger(
			"MainTester.class");
	protected static LogDelegater logHelper;
	
	public static void main(String[] args) throws InterruptedException {

		
		SystemConfigLoader configLoader;
		System.out.println(SystemConfigParameters.APP_ROOT_PATH);
//		if (args.length == 1) {
//			configLoader = new SystemConfigLoader(args[0]);
//		} else if (args.length > 1) {
//			throw new RuntimeException("system startup argument error..");
//		} else {

			configLoader = new SystemConfigLoader();
	//	}

		if (!configLoader.loadConfigFile()) {
			Logger.getLogger(configLoader.getClass().getName())
					.info("config file has not found..");
			Logger.getLogger(configLoader.getClass().getName())
					.info("System is going to shutdown..");
			System.exit(-1);
		} else {
			Logger.getLogger("SystemConfigLoader.class").info(
					"config file has been found..");
			Logger.getLogger("SystemConfigLoader.class").info(
					"System continue to loading");
			configLoader.loadConfigParmeters();
			Logger.getLogger("MainTester.class")
					.info("System default date format is: "
							+ SystemConfigParameters.SYSTEM_DEFAULT_DATE_FORMAT);
			Logger.getLogger("MainTester.class")
					.info("System default time format is: "
							+ SystemConfigParameters.SYSTEM_DEFAULT_TIME_FORMAT);
			Logger.getLogger("MainTester.class").info(
					"System default log file folder is: " + "/"
							+ SystemConfigParameters.SYSTRM_LOGS_FOLDER);
			//slogHelper=new LogDelegater();
			Logger.getLogger("LogDelegater.class").info(LogDelegater.class.getName()+"init successfully...");
		}
	 

		
		ServiceManager svrm = ServiceManager.newInstance();
		ConsoleUI ui = ConsoleUI.initUI(svrm);
		if("server".equalsIgnoreCase(args[0])){
			ui.startServer(new Integer(9000));
		}
		else if("client".equalsIgnoreCase(args[0])){
		ui.startClient();
		}
		else{
			Logger.getLogger("MainTester.class").info(
					"Please enter corrent parameter");
			System.exit(0);
		}
		// ����spring�Ƿ���سɹ�
		// SysMessage sysMessage=(SysMessage) cxt.getBean("systemMessage");
		//
		// Iterator it=sysMessage.getSysMessage().entrySet().iterator();
		// while(it.hasNext()){
		// Entry entry=(Entry) it.next();
		// System.out.println(entry.getKey()+"="+entry.getValue());
		// }
	}

}
