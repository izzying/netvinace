package com.amos.platform.common.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.amos.platform.common.MessageList;
import com.amos.platform.common.exception.SystemLoadingException;

public class VinacePlatFormLoader {

	 private  static String fileName = "./lib/SystemConfig.properties";
	 
	private VinacePlatFormLoader() {
		// TODO Auto-generated constructor stub
	}

	
	 public static boolean initSystem() {

	       
	        boolean passed = false;
	        try {
	            File file = new File(fileName);
	            if (!file.exists()) {
	            	SystemLoadingException ex=new SystemLoadingException(MessageList.CONFIG_FILE_NOT_FOUND);
	            	 Logger.getLogger(VinacePlatFormLoader.class.getName()).log(Level.SEVERE, ex.getMessage(),ex);
	            	 System.exit(-1);
	            }
	            fileName = file.getAbsolutePath();
	            FileInputStream ins = new FileInputStream(file);
	            Properties props = new Properties();
	            props.load(ins);
	            //load the system Intilization Parameters
	            passed = true;
	        } catch (IOException ex) {
	            passed = false;
	            Logger.getLogger(VinacePlatFormLoader.class.getName()).log(Level.SEVERE, null, ex);
	        }
			return passed;
	 }
	public static String getFileName() {
		return fileName;
	}


	public static void setFileName(String fileName) {
		VinacePlatFormLoader.fileName = fileName;
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		VinacePlatFormLoader.initSystem();

	}

}
