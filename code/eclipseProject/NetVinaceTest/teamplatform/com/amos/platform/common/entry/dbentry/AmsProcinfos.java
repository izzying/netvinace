package com.amos.platform.common.entry.dbentry;

/***********************************************************************
 * Module:  AmsProcinfos.java
 * Author:  Administrator
 * Purpose: Defines the Class AmsProcinfos
 ***********************************************************************/


/** 项目里程碑信息
 * 
 * @pdOid d47bbadd-abb9-4d80-a6c9-35daa8a64181 */
public class AmsProcinfos {
   /** @pdOid 07a11d99-962a-4538-accd-73b64cd234ab */
   public int procid;
   /** @pdOid c20b5598-2079-43d2-86c3-2255c7bd3b4f */
   public java.lang.String procname;
   /** @pdOid 77eb6f4a-90a2-4be1-a03b-a1f92f41b220 */
   public java.lang.String descp;
   /** @pdOid 1b47e1a7-f379-4ab4-b9a9-b8db72f9f0b5 */
   public java.lang.String attach;
   
   /** @pdRoleInfo migr=no name=AmsProjectinfos assc=projectProcess mult=0..1 side=A */
   public AmsProjectinfos amsProjectinfos;
   
   /** @pdOid cee74132-78f3-41af-9ec9-88cb5e96ac1f */
   public int getProcid() {
      return procid;
   }
   
   /** @param newProcid
    * @pdOid a7f0400a-bf22-4cc5-a117-319700aea18e */
   public void setProcid(int newProcid) {
      procid = newProcid;
   }
   
   /** @pdOid 6aa0d71d-8229-4f77-855a-2290beea9936 */
   public java.lang.String getProcname() {
      return procname;
   }
   
   /** @param newProcname
    * @pdOid 4af53647-4aeb-4190-ab76-6c964b3f1643 */
   public void setProcname(java.lang.String newProcname) {
      procname = newProcname;
   }
   
   /** @pdOid 34a3cbba-a8a5-4ffc-b1e4-687b47c48ab8 */
   public java.lang.String getDescp() {
      return descp;
   }
   
   /** @param newDescp
    * @pdOid 9e3c9ea8-930c-415d-9ed6-feeaf0afed64 */
   public void setDescp(java.lang.String newDescp) {
      descp = newDescp;
   }
   
   /** @pdOid 0b715aff-e295-41e2-ace3-bfcab8b5f98f */
   public java.lang.String getAttach() {
      return attach;
   }
   
   /** @param newAttach
    * @pdOid 1c4e9ff0-3286-450f-935c-f068916742ef */
   public void setAttach(java.lang.String newAttach) {
      attach = newAttach;
   }
   
   
   /** @pdGenerated default parent getter */
   public AmsProjectinfos getAmsProjectinfos() {
      return amsProjectinfos;
   }
   
   /** @pdGenerated default parent setter
     * @param newAmsProjectinfos */
   public void setAmsProjectinfos(AmsProjectinfos newAmsProjectinfos) {
      if (this.amsProjectinfos == null || !this.amsProjectinfos.equals(newAmsProjectinfos))
      {
         if (this.amsProjectinfos != null)
         {
            AmsProjectinfos oldAmsProjectinfos = this.amsProjectinfos;
            this.amsProjectinfos = null;
            oldAmsProjectinfos.removeAmsProcinfos(this);
         }
         if (newAmsProjectinfos != null)
         {
            this.amsProjectinfos = newAmsProjectinfos;
            this.amsProjectinfos.addAmsProcinfos(this);
         }
      }
   }

}