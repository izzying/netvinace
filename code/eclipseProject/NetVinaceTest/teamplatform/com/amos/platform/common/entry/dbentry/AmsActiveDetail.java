package com.amos.platform.common.entry.dbentry;

/***********************************************************************
 * Module:  AmsActiveDetail.java
 * Author:  Administrator
 * Purpose: Defines the Class AmsActiveDetail
 ***********************************************************************/


/** 活动/会议详细信息，成员信息
 * 
 * @pdOid 7f4f1d2a-bbd8-4768-9c0e-f732b20ba2ac */
public class AmsActiveDetail {
   /** @pdOid bd2e407f-68b0-4714-8529-8f56f6ea7f89 */
   public int serid;
   /** @pdOid c06bacf5-b041-46fe-86e8-d62ab6ad22e3 */
   public java.lang.String workDescp;
   /** @pdOid ea071582-2891-4282-ad10-c7f50ad1ee7e */
   public java.util.Date cometime;
   /** @pdOid 24f13407-16da-4989-931b-33d3f20c4f92 */
   public java.util.Date gotime;
   /** @pdOid 8c4f1a7c-cae8-4ace-b322-6554508b6c97 */
   public java.lang.String result;
   
   /** 活动/会议信息与详情
    * 包括参与活动/会议人员信息 */
   /** @pdRoleInfo migr=no name=AmsActiveinfos assc=activeteInfors mult=0..1 side=A */
   public AmsActiveinfos amsActiveinfos;
   /** @pdRoleInfo migr=no name=Userinfos assc=memberId mult=0..1 side=A */
   public Userinfos userinfos;
   
   /** @pdOid fa17ebdb-c943-44a8-a57e-38434fd654db */
   public int getSerid() {
      return serid;
   }
   
   /** @param newSerid
    * @pdOid a8b466bf-e6af-4a8e-b8ed-27b6e4937e87 */
   public void setSerid(int newSerid) {
      serid = newSerid;
   }
   
   /** @pdOid 9e0e03b3-b3aa-471c-892a-d06fe5a7f1ba */
   public java.lang.String getWorkDescp() {
      return workDescp;
   }
   
   /** @param newWorkDescp
    * @pdOid 2e2cfe44-5795-4ce2-80ec-3a10f4fb47d6 */
   public void setWorkDescp(java.lang.String newWorkDescp) {
      workDescp = newWorkDescp;
   }
   
   /** @pdOid e9c50575-67a6-45e7-9512-586c6bae9cc4 */
   public java.util.Date getCometime() {
      return cometime;
   }
   
   /** @param newCometime
    * @pdOid 1cb4d59e-5f57-47d6-9522-6e7d74dc3d47 */
   public void setCometime(java.util.Date newCometime) {
      cometime = newCometime;
   }
   
   /** @pdOid b5dbc673-bb1f-4a2b-81a8-80d22eab4344 */
   public java.util.Date getGotime() {
      return gotime;
   }
   
   /** @param newGotime
    * @pdOid 7216ed0b-eb25-4043-912b-5d39b056a8f1 */
   public void setGotime(java.util.Date newGotime) {
      gotime = newGotime;
   }
   
   /** @pdOid f120eb1d-eeab-4c2a-bcbf-09308005b026 */
   public java.lang.String getResult() {
      return result;
   }
   
   /** @param newResult
    * @pdOid e5979350-00e6-4147-8a8b-8d3f602f9a6b */
   public void setResult(java.lang.String newResult) {
      result = newResult;
   }
   
   
   /** @pdGenerated default parent getter */
   public AmsActiveinfos getAmsActiveinfos() {
      return amsActiveinfos;
   }
   
   /** @pdGenerated default parent setter
     * @param newAmsActiveinfos */
   public void setAmsActiveinfos(AmsActiveinfos newAmsActiveinfos) {
      if (this.amsActiveinfos == null || !this.amsActiveinfos.equals(newAmsActiveinfos))
      {
         if (this.amsActiveinfos != null)
         {
            AmsActiveinfos oldAmsActiveinfos = this.amsActiveinfos;
            this.amsActiveinfos = null;
            oldAmsActiveinfos.removeAmsActiveDetail(this);
         }
         if (newAmsActiveinfos != null)
         {
            this.amsActiveinfos = newAmsActiveinfos;
            this.amsActiveinfos.addAmsActiveDetail(this);
         }
      }
   }
   /** @pdGenerated default parent getter */
   public Userinfos getUserinfos() {
      return userinfos;
   }
   
   /** @pdGenerated default parent setter
     * @param newUserinfos */
  

}