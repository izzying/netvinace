package com.amos.platform.common.entry.dbentry;

/***********************************************************************
 * Module:  AmsResTypeInfos.java
 * Author:  Administrator
 * Purpose: Defines the Class AmsResTypeInfos
 ***********************************************************************/


/** 系统资源类型信息
 * 
 * @pdOid 9847630d-7554-4465-ba51-bd6742555ad8 */
public class AmsResTypeInfos {
   /** @pdOid 1666fd77-fffd-46f6-8665-28f17b9eefaf */
   public int retid;
   /** @pdOid a000bdea-ddac-423d-95c9-e4391dd1b99d */
   public java.lang.String retname;
   /** @pdOid 57da2de9-0b9e-4cf9-bba4-2abd8726a2dc */
   public java.lang.String handleClass;
   /** @pdOid ff5b7fee-8a29-4137-92e1-85e51f8d4189 */
   public java.lang.String note;
   
   /** @pdRoleInfo migr=no name=AmsAcsinfos assc=resourceTypeInfor coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<AmsAcsinfos> amsAcsinfos;
   /** @pdRoleInfo migr=no name=AmsResInfos assc=resourceType coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<AmsResInfos> amsResInfos;
   
   /** @pdOid b7a03bed-04e6-4c53-b221-8f45b54298d3 */
   public int getRetid() {
      return retid;
   }
   
   /** @param newRetid
    * @pdOid 8ba57cc0-a6a9-4ddb-a4c1-999dbb1a0717 */
   public void setRetid(int newRetid) {
      retid = newRetid;
   }
   
   /** @pdOid 9823a4a1-2564-4aa9-a15e-542bf12be2f6 */
   public java.lang.String getRetname() {
      return retname;
   }
   
   /** @param newRetname
    * @pdOid 846a6d85-32f9-4fcb-9015-550a476c638d */
   public void setRetname(java.lang.String newRetname) {
      retname = newRetname;
   }
   
   /** @pdOid 71099a54-fc33-4657-b631-da484ab9b95c */
   public java.lang.String getHandleClass() {
      return handleClass;
   }
   
   /** @param newHandleClass
    * @pdOid 676298d1-ca86-4fc7-b410-13c3b7a12f9a */
   public void setHandleClass(java.lang.String newHandleClass) {
      handleClass = newHandleClass;
   }
   
   /** @pdOid 3069bd78-6045-493c-b4cc-31c0a0f2091b */
   public java.lang.String getNote() {
      return note;
   }
   
   /** @param newNote
    * @pdOid a5596964-98b4-4929-8f42-42eca103087c */
   public void setNote(java.lang.String newNote) {
      note = newNote;
   }
   
   
   /** @pdGenerated default getter */
   public java.util.Collection<AmsAcsinfos> getAmsAcsinfos() {
      if (amsAcsinfos == null)
         amsAcsinfos = new java.util.HashSet<AmsAcsinfos>();
      return amsAcsinfos;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorAmsAcsinfos() {
      if (amsAcsinfos == null)
         amsAcsinfos = new java.util.HashSet<AmsAcsinfos>();
      return amsAcsinfos.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newAmsAcsinfos */
   public void setAmsAcsinfos(java.util.Collection<AmsAcsinfos> newAmsAcsinfos) {
      removeAllAmsAcsinfos();
      for (java.util.Iterator iter = newAmsAcsinfos.iterator(); iter.hasNext();)
         addAmsAcsinfos((AmsAcsinfos)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newAmsAcsinfos */
   public void addAmsAcsinfos(AmsAcsinfos newAmsAcsinfos) {
      if (newAmsAcsinfos == null)
         return;
      if (this.amsAcsinfos == null)
         this.amsAcsinfos = new java.util.HashSet<AmsAcsinfos>();
      if (!this.amsAcsinfos.contains(newAmsAcsinfos))
      {
         this.amsAcsinfos.add(newAmsAcsinfos);
         newAmsAcsinfos.setAmsResTypeInfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldAmsAcsinfos */
   public void removeAmsAcsinfos(AmsAcsinfos oldAmsAcsinfos) {
      if (oldAmsAcsinfos == null)
         return;
      if (this.amsAcsinfos != null)
         if (this.amsAcsinfos.contains(oldAmsAcsinfos))
         {
            this.amsAcsinfos.remove(oldAmsAcsinfos);
            oldAmsAcsinfos.setAmsResTypeInfos((AmsResTypeInfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllAmsAcsinfos() {
      if (amsAcsinfos != null)
      {
         AmsAcsinfos oldAmsAcsinfos;
         for (java.util.Iterator iter = getIteratorAmsAcsinfos(); iter.hasNext();)
         {
            oldAmsAcsinfos = (AmsAcsinfos)iter.next();
            iter.remove();
            oldAmsAcsinfos.setAmsResTypeInfos((AmsResTypeInfos)null);
         }
      }
   }
   /** @pdGenerated default getter */
   public java.util.Collection<AmsResInfos> getAmsResInfos() {
      if (amsResInfos == null)
         amsResInfos = new java.util.HashSet<AmsResInfos>();
      return amsResInfos;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorAmsResInfos() {
      if (amsResInfos == null)
         amsResInfos = new java.util.HashSet<AmsResInfos>();
      return amsResInfos.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newAmsResInfos */
   public void setAmsResInfos(java.util.Collection<AmsResInfos> newAmsResInfos) {
      removeAllAmsResInfos();
      for (java.util.Iterator iter = newAmsResInfos.iterator(); iter.hasNext();)
         addAmsResInfos((AmsResInfos)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newAmsResInfos */
   public void addAmsResInfos(AmsResInfos newAmsResInfos) {
      if (newAmsResInfos == null)
         return;
      if (this.amsResInfos == null)
         this.amsResInfos = new java.util.HashSet<AmsResInfos>();
      if (!this.amsResInfos.contains(newAmsResInfos))
      {
         this.amsResInfos.add(newAmsResInfos);
         newAmsResInfos.setAmsResTypeInfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldAmsResInfos */
   public void removeAmsResInfos(AmsResInfos oldAmsResInfos) {
      if (oldAmsResInfos == null)
         return;
      if (this.amsResInfos != null)
         if (this.amsResInfos.contains(oldAmsResInfos))
         {
            this.amsResInfos.remove(oldAmsResInfos);
            oldAmsResInfos.setAmsResTypeInfos((AmsResTypeInfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllAmsResInfos() {
      if (amsResInfos != null)
      {
         AmsResInfos oldAmsResInfos;
         for (java.util.Iterator iter = getIteratorAmsResInfos(); iter.hasNext();)
         {
            oldAmsResInfos = (AmsResInfos)iter.next();
            iter.remove();
            oldAmsResInfos.setAmsResTypeInfos((AmsResTypeInfos)null);
         }
      }
   }

}