package com.amos.vinace.core;

import java.io.File;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * 
 * 日志生成工厂，包装com.amos.vinace.core.SystemLogger 抽象类
 * 该类中包含常用日志操作方法 
 * @see  com.amos.vinace.core.SystemLogger
 * @author Administrator
 *
 */
public class LoggerFactory implements InvocationHandler{

	public static SystemLogger logger;
	
	protected static String logFileURL;
	
	protected static File logFile;
	
	//需要代理的对象
	public Object targetObject;
	/**
	 * 默认构造器，用于反射组件 生成LoggerFactory对象
	 * 
	 * 有关反射构造器@see  com.amos.vinace.core.SystemResourceLoader
	 * 
	 * @author Administrator
	 *
	 */
	public LoggerFactory() {
		
		Date systemDate=new Date();
		SimpleDateFormat mf=new SimpleDateFormat(SystemConfigParameters.SYSTEM_DEFAULT_DATE_FORMAT);
		String dateStr=mf.format(systemDate);
		String fileName=dateStr+".log";
		logFileURL=SystemConfigParameters.SYSTRM_LOGS_FOLDER+"/"+fileName;
		
		
	}
	
	public LoggerFactory(Object obj) {
		this();
		this.targetObject=obj;
		
	}
	
	/**
	 * 
	 * 日志操作对象生成方法 
	 * 生成 com.amos.vinace.core.SystemLoggerImpl 对象
	 * 用户可在该类中重写常用日志操作方法 
	 * @see  com.amos.vinace.core.SystemLoggerImpl
	 * @author Administrator
	 *
	 */
	public  static SystemLogger newInstance(){
		
		if(logger==null){
			 logger=new SystemLoggerImpl();
			logFile=logger.createLogFile(logFileURL);
			System.out.println(logFileURL=logFile.getAbsolutePath());
		}
		return logger;
	}

	public static SystemLogger getLogger() {
		return logger;
	}

	public static void setLogger(SystemLogger logger) {
		LoggerFactory.logger = logger;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {
		String log_prefix="<LOG_INFO>";
		String logContext=proxy.getClass().toString()+"call"+method.getName().toString();
		String log_endle="</LOG_INFO>";
		String context=log_prefix+logContext+log_endle;
		logger.writeLogFile(context);
	   Object result= method.invoke(targetObject, args);
		
		return result;
	}

}
