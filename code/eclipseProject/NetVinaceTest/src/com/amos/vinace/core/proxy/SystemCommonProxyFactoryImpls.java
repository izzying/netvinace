package com.amos.vinace.core.proxy;

import java.lang.reflect.Method;

import com.amos.vinace.core.ISystemLogger;
import com.amos.vinace.core.listener.CommonListener;

public class SystemCommonProxyFactoryImpls extends SystemCommonProxyFactory {

	private ISystemLogger logger;
	private Object target;
	public SystemCommonProxyFactoryImpls(Object object) {
		// TODO Auto-generated constructor stub
		this.target=object;
		
	}

	

	@Override
	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {
		// TODO Auto-generated method stub
		String logPrefix="<LOG_INFO>";
		String context=new String(logPrefix);
		context=proxy.getClass().getName()+" call methond "+method.getName()+" with argraments";
		context+="[";
		for(Object arg:args){
			context+=arg;
		}
		context+="]";
		String logEndTag="</LOG_INFO>";
		
		context+=logEndTag;
		
		System.out.println("Write Log Information: "+context);
		
		logger.writeLogFile(context);
		
		Object result=method.invoke(target, args);
		
		return result;
	}
	
	

	
	
	
	
}
