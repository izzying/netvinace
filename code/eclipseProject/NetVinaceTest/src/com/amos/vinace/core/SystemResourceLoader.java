/***********************************************************************
 * Module:  SystemResourceLoader.java
 * Author:  Administrator
 * Purpose: Defines the Class SystemResourceLoader
 ***********************************************************************/

package com.amos.vinace.core;

import java.io.File;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.amos.vinace.core.listener.CommonListener;

/**
 * 系统资源加载类抽象实现类，利用反射实现业务对象动态生成,重写该类方法需要重写hashCode()方法和equal()方法
 * 
 * @pdOid 70dd6214-ae8e-426f-8287-1f0dac0c4421
 */
public abstract class SystemResourceLoader implements ISystemResourceLoader,CommonListener {

	

	/**
	 * 系统资源（业务类）列表
	 * 
	 * @pdOid c0158c93-a582-430b-be95-812898931bad
	 */
	public static Map<String, File> clzPathURL = new HashMap<String, File>(0);



	/**
	 * 系统资源对象库
	 * 
	 * @pdOid c0158c93-a582-430b-be95-812898931bad
	 */
	protected static java.util.HashMap systemObjectList = new HashMap<Long, Object>(
			0);



	/**
	 * 反射类对象生成器。
	 * 
	 * @param stringClassPath
	 * @pdOid dd42f075-f52a-4802-9704-158e27f1d173
	 */
	public java.lang.Object genateObject(String stringClassPath) {

		try {
			Object obj = Class.forName(stringClassPath).newInstance();
			System.out.print(obj.getClass().getName());
			// long ooMd5=Long.parseLong(MD5Tool.MD5(""));
			// addObjectToList(ooMd5,obj);
			return obj;
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

	/**
	 * @param objkey
	 * @pdOid 28772748-ff93-44bc-b1db-4766fe9f80d3
	 */
	public static <T> java.lang.Object getSystemObject(java.lang.Long objkey) {

		if (systemObjectList.containsKey(objkey)) {
			return systemObjectList.get(objkey);
		}
		return null;

	}

	/**
	 * @param obj
	 * @pdOid a399fc1c-379a-4dc4-a2ce-53f10422845c
	 */
	public static java.lang.Long getSystemObjectKey(java.lang.Object obj) {

		if (systemObjectList.containsValue(obj)) {
			Iterator it = systemObjectList.keySet().iterator();
			long objKey = -1L;
			while (it.hasNext()) {
				objKey = Long.parseLong(it.next().toString());
				if (systemObjectList.get(objKey).equals(obj)) {
					break;
				}

			}
			return objKey;

		}

		return null;

	}

	/**
	 * 向资源库中加入对象实体
	 * 
	 * @param resource
	 * @param objKey
	 * @pdOid 984c4528-a5f5-433b-927a-2d9ad18a0435
	 */
	public static <T> int addObjectToList(java.lang.Long objKey,
			java.lang.Object resource) {

		if (!systemObjectList.containsKey(objKey)) {
			systemObjectList.put(objKey, resource);
		}
		return systemObjectList.size();
	}

	/**
	 * @param objKey
	 * @pdOid 84919f99-5c67-458b-b07a-ffc285114bec
	 */
	public static <T> int removeObjectFormList(long objKey) {

		if (systemObjectList.containsKey(objKey)) {
			systemObjectList.remove(objKey);
		}
		return systemObjectList.size();
	}

	public static int addClassFile(String clzFullPath, File file) {
		// TODO Auto-generated method stub
		systemObjectList.put(clzFullPath, file);
		return systemObjectList.size();

	}

}