/***********************************************************************
 * Module:  ISystemResourceLoader.java
 * Author:  Administrator
 * Purpose: Defines the Interface ISystemResourceLoader
 ***********************************************************************/

package com.amos.vinace.core;


/** 系统资源加载器抽象接口
 * 
 * @pdOid 49fc23c2-fd44-4615-b198-80980c4c0b4a */
public interface ISystemResourceLoader {
   /** 反射类对象生成器。
    * 
    * @param stringClassPath
    * @pdOid 94d94360-1f53-466a-bb2c-f901d331512c */
  public  java.lang.Object genateObject(String stringClassPath);

}