package com.amos.vinace.core.listener;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.FileSystems;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.List;

import com.amos.vinace.core.ObjectGenator;
import com.amos.vinace.core.SystemConfigFileLoaderImpls;
import com.amos.vinace.core.SystemConfigLoader;
import com.amos.vinace.core.SystemConfigParameters;
import com.amos.vinace.core.SystemResourceLoader;
import com.amos.vinace.core.SystemWatchedResource;
import com.amos.vinace.test.ClassInforScanner;
import com.amos.vinace.test.mainTest;

public class FileSystemListener extends SystemWatchedResource implements Runnable {

	//监听者（观众）列表
	private static List<CommonListener>systemListeners=new ArrayList<CommonListener>(0);
	
	private static  String systemStatu="INITED_SYSTEM";
			
	public FileSystemListener() {
		systemListeners=new ArrayList<CommonListener>(0);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		watchFileSystem(SystemConfigParameters.SYSTEM_CLASS_PATH);

	}

	private void watchFileSystem(String root) {
		WatchService watchService;
		try {
			watchService = FileSystems.getDefault().newWatchService();
			Paths.get(root).register(watchService,
					StandardWatchEventKinds.ENTRY_CREATE,
					StandardWatchEventKinds.ENTRY_DELETE,
					StandardWatchEventKinds.ENTRY_MODIFY);
			while (true) {
				WatchKey key = watchService.take();
				for (WatchEvent<?> event : key.pollEvents()) {
					System.out.println("类文件发生改变，重新加载系统资源");
					setSystemStatu("RESET_SYSTEM");
					reloadSystem();
					
				}
				if (!key.reset()) {
					break;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public int addListener(CommonListener listener) {
		// TODO Auto-generated method stub
		systemListeners.add(listener);
		return systemListeners.size();
	}

	@Override
	public int deleteListener(CommonListener listener) {
		// TODO Auto-generated method stub
		if(systemListeners.contains(listener)){
			systemListeners.remove(listener);
		}
		return systemListeners.size();
	}

	@Override
	public int notifyListeners() {
		// TODO Auto-generated method stub
		
		for(int i=0;i<systemListeners.size();i++){
			CommonListener listener=systemListeners.get(i);
			listener.statusChanged(systemStatu);
		}
		return systemListeners.size();
	}

	public String getSystemStatu() {
		return systemStatu;
	}

	public void setSystemStatu(String newStatus) {
		
		if(!this.systemStatu.equalsIgnoreCase(newStatus)){
			notifyListeners();
		}
		this.systemStatu = newStatus;
	}

	public void reloadSystem(){
		
			
			SystemConfigFileLoaderImpls config = new SystemConfigFileLoaderImpls();
			config.loadConfigFile();
			initSystemLogger();

			// 打印系统配置参数
			SystemConfigLoader.showSystemProperties();
			ClassInforScanner scanner = ClassInforScanner
					.newInstance(SystemConfigParameters.SYSTEM_CLASS_PATH);
			System.out.println(SystemConfigParameters.SYSTEM_CLASS_PATH);
			// 扫描类文件 默认路径可配置
			List<File> clzFileList = scanner.scanRoot();
			scanner.transfer2ClassPath(clzFileList);
		}
	
	public static void initSystemLogger() {
		SystemResourceLoader resourceLoader = ObjectGenator.newInstance();
		Object obj = resourceLoader
				.genateObject("com.amos.vinace.core.LoggerFactory");
		// System.out.println(obj.getClass().toString());

		try {
			Object log = obj.getClass().getMethod("newInstance", null)
					.invoke(obj, null);
		
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException ex3) {
			// TODO Auto-generated catch block
			ex3.printStackTrace();
		} catch (IllegalAccessException ex4) {
			// TODO Auto-generated catch block
			ex4.printStackTrace();
		} catch (InvocationTargetException ex5) {
			// TODO Auto-generated catch block
			ex5.printStackTrace();
		} catch (NoSuchMethodException ex6) {
			// TODO Auto-generated catch block
			ex6.printStackTrace();
		}
	}
}
