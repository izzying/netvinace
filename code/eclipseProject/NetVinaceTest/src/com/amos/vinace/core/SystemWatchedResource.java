package com.amos.vinace.core;

import java.util.List;

import com.amos.vinace.core.listener.CommonListener;

/**
 * 需要被监听的业务（资源）抽象类
 * @author Administrator
 *
 */
public abstract class SystemWatchedResource {

	//监听者列表
	public List<CommonListener> systemListeners;
	
	//增加监听者
	public abstract int addListener(CommonListener listener);
	
	//删除监听者
	public abstract int deleteListener(CommonListener listener);
	
	//通知监听者
	public abstract int notifyListeners();
	
	
	
}
