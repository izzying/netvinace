/***********************************************************************
 * Module:  SystemConfigFileLoaderImpls.java
 * Author:  Administrator
 * Purpose: Defines the Class SystemConfigFileLoaderImpls
 ***********************************************************************/

package com.amos.vinace.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

/**
 * 系统配置文件加载组件实现类
 * 
 * @pdOid 895f6dc4-4de8-4993-a92e-4d0a227dc432
 */
public class SystemConfigFileLoaderImpls extends SystemConfigLoader implements
		IConfigVaildation {
	/**
	 * 以文件方式加载用户指定配置文件，当加载失败时，抛出异常并加载系统默认配置文件，
	 * 
	 * @param configFile
	 * @pdOid 4ad94e97-463b-490b-a465-b803e94e5a93
	 */
	public boolean loadConfigFile(java.io.File configFile) {
		// TODO: implement
		return false;
	}
	
	private SystemResourceLoaderImpl resourceLoader;
	
	public void statusValue(Field field, String statu) {
		// TODO Auto-generated method stub
		
	}


		

	/**
	 * 以文件路径方式加载用户指定配置文件，当加载失败时，抛出异常并加载系统默认配置文件，
	 * 
	 * @param configFileURL
	 * @pdOid 4faf7bd6-c06f-4302-989a-653a5e24df41
	 */
	public boolean loadConfigFile(java.lang.String configFileURL) {
		// TODO: implement

		return false;
	}

	/**
	 * 加载系统默认配置文件
	 * 
	 * @pdOid 86259ffd-80d5-4892-a22d-e215bd9e9682
	 */
	public boolean loadConfigFile() {
		FileInputStream ins = null;
		InputStreamReader fileIn = null;
		File config = new File(SYSTEM_CONFIG_FILE);
		if (config.exists()) {
			FileInputStream fin;

			try {
				fin = new FileInputStream(config);
				Properties parameters = new Properties();
				parameters.load(fin);
				initSystemParameters(parameters);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			// exit system if master config file not exists..

		}

		return false;
	}

	/**
	 * 初始化系统参数，填充系统配置参数值对象（类）
	 * 
	 * @pdOid 86259ffd-80d5-4892-a22d-e215bd9e9682
	 */
	private boolean initSystemParameters(Properties parameters) {
		
		SystemConfigParameters.PROJECT_ROOT  = System.getProperty("user.dir");
		
		
		
		SystemConfigParameters.BUSINESS_CLASSES_FOLDER = parameters
				.getProperty("BUSINESS_CLASSES_FOLDER") == null ? "" : parameters
				.getProperty("BUSINESS_CLASSES_FOLDER");
		
		SystemConfigParameters.SYSTEM_CLASS_PATH=SystemConfigParameters.PROJECT_ROOT +"\\"+SystemConfigParameters.BUSINESS_CLASSES_FOLDER;
		
		SystemConfigParameters.BUSINESS_CLASSES_LOADER = parameters
				.getProperty("BUSINESS_CLASSES_LOADER") == null ? ""
				: parameters.getProperty("BUSINESS_CLASSES_LOADER");

		SystemConfigParameters.SYSTRM_LOGS_FOLDER = parameters
				.getProperty("SYSTRM_LOGS_FOLDER") == null ? "" : parameters
				.getProperty("SYSTRM_LOGS_FOLDER");
		
		SystemConfigParameters.SYSTEM_DEFAULT_DATE_FORMAT = parameters
				.getProperty("SYSTEM_DEFAULT_DATE_FORMAT") == null ? ""
				: parameters.getProperty("SYSTEM_DEFAULT_DATE_FORMAT");
		
		SystemConfigParameters.SYSTEM_DEFAULT_TIME_FORMAT = parameters
				.getProperty("SYSTEM_DEFAULT_TIME_FORMAT") == null ? ""
				: parameters.getProperty("SYSTEM_DEFAULT_TIME_FORMAT");

		/*
		 * String
		 * busConfigFiles=parameters.getProperty("BUSINESS_CLASSES_CONFIG_FILES"
		 * ) == null ?
		 * "":parameters.getProperty("BUSINESS_CLASSES_CONFIG_FILES");
		 * if(!"".equals(busConfigFiles)){ String
		 * []files=busConfigFiles.split(","); if(files.length>0){ for(String
		 * file :files){ File f=new File(file); if(f.exists()){
		 * SystemConfigParameters.addConfigFile(f); } } } }
		 * 
		 * SystemConfigParameters. DATA_BASE_URL = parameters
		 * .getProperty("DATA_BASE_URL") == null ? "" :
		 * parameters.getProperty("DATA_BASE_URL"); SystemConfigParameters.
		 * DATA_BASE_DRIVER_CLASS = parameters
		 * .getProperty("DATA_BASE_DRIVER_CLASS") == null ? "" : parameters
		 * .getProperty("DATA_BASE_DRIVER_CLASS"); SystemConfigParameters.
		 * DATA_BASE_CONNECTION_POOL_INIT_SIZE = parameters
		 * .getProperty("DATA_BASE_CONNECTION_POOL_INIT_SIZE") == null ? 10 :
		 * Integer.parseInt(parameters
		 * .getProperty("DATA_BASE_CONNECTION_POOL_INIT_SIZE"));
		 * SystemConfigParameters. DATA_BASE_CONNECTION_POOL_INCREATMENT =
		 * parameters .getProperty("DATA_BASE_CONNECTION_POOL_INCREATMENT") ==
		 * null ? 30 : Integer.parseInt(parameters
		 * .getProperty("DATA_BASE_CONNECTION_POOL_INCREATMENT"));
		 */

		// neccsery parm min config parms

		Map<String, String> minparms = new HashMap<String, String>(0);
		minparms.put("SystemConfigParameters.SYSTRM_LOGS_FOLDER",
				SystemConfigParameters.SYSTRM_LOGS_FOLDER);
		minparms.put("SystemConfigParameters. SYSTEM_DEFAULT_DATE_FORMAT",
				SystemConfigParameters.SYSTEM_DEFAULT_DATE_FORMAT);
		minparms.put("SystemConfigParameters. SYSTEM_DEFAULT_TIME_FORMAT",
				SystemConfigParameters.SYSTEM_DEFAULT_TIME_FORMAT);
		minparms.put("SystemConfigParameters.BUSINESS_CLASSES_LOADER",
				SystemConfigParameters.BUSINESS_CLASSES_LOADER);
		
	Iterator it=minparms.keySet().iterator();
	
		
		if (!emptyVaildate(minparms)) {
			try {
				throw new ParameterUnquireException("系统参数丢失，请检查配置文件",
						this.getClass());
			} catch (ParameterUnquireException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return true;

	}

	@Override
	public boolean emptyVaildate(Map parms) {

		Iterator it_parm = parms.keySet().iterator();

		while (it_parm.hasNext()) {

			String parmKey = it_parm.next().toString();

			String parmVal = parms.get(parmKey).toString();

			if(!singleValueEptyVaildate(parmKey, parmVal)){
				
				return false;
			}
			System.out.println(parmKey+"="+parmVal);
		}

		return true;
	}

	@Override
	public boolean singleValueEptyVaildate(String parmName, Object value) {
		if ("".equals(value)) {
			try {
				throw new ParameterUnquireException(parmName + "参数丢失，请检查配置文件",
						parmName);
			} catch (ParameterUnquireException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		}
		return true;

	}




	public SystemResourceLoaderImpl getResourceLoader() {
		return resourceLoader;
	}




	public void setResourceLoader(SystemResourceLoaderImpl resourceLoader) {
		this.resourceLoader = resourceLoader;
	}

}