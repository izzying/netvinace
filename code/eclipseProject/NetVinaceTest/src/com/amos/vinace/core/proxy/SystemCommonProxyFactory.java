package com.amos.vinace.core.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
/**
 * 系统抽象代理工厂，实现 java.lang.reflect.InvocationHandler 接口
 * 默认实现 com.amos.vinace.core.proxy.SystemCommonProxyFactoryImpls
 * @see com.amos.vinace.core.proxy.SystemCommonProxyFactoryImpls
 * @author Administrator
 *
 */
public abstract class SystemCommonProxyFactory implements InvocationHandler {

	public SystemCommonProxyFactory() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public abstract Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable ;

}
