package com.amos.vinace.core;

import java.util.Map;

public interface IConfigVaildation {

	/**
	 * 
	 * @param parm
	 * @return boolean
	 * 系统配置文件验证接口，系统配置文件加载类实现此接口，编写规则即可实现
	 * 数据验证功能
	 * 例子文件：com.amos.vinace.core.SystemConfigFileLoaderImpls.java
	 */
	
	public boolean emptyVaildate(Map<String,Object> parms);
	
	public boolean singleValueEptyVaildate(String parmName,Object value);
	

}
