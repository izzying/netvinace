/***********************************************************************
 * Module:  SystemConfigLoader.java
 * Author:  Administrator
 * Purpose: Defines the Class SystemConfigLoader
 ***********************************************************************/

package com.amos.vinace.core;

import java.util.*;
import java.util.Map.Entry;

/** 系统配置文件加载组件抽象类
 * 
 * @pdOid be73ff47-8019-4a90-b765-cdaa36c47b0a */
public abstract class SystemConfigLoader implements ISystemConfigLoader,IConfigVaildation{
	
	public final static String SYSTEM_CONFIG_FILE="config/systemConfig.properties";
	
   /** 以文件方式加载用户指定配置文件，当加载失败时，抛出异常并加载系统默认配置文件，
    * 
    * @param configFile
    * @pdOid dbc9611b-e2cd-43d8-a931-973424668623 */

   public boolean loadConfigFile(java.io.File configFile) {
      // TODO: implement
      return false;
   }
   
   /** 以文件路径方式加载用户指定配置文件，当加载失败时，抛出异常并加载系统默认配置文件，
    * 
    * @param configFileURL
    * @pdOid 0ddc9dec-99bc-4b8e-bdeb-6267a1b61a28 */
   public boolean loadConfigFile(java.lang.String configFileURL) {
      // TODO: implement
      return false;
   }
   
   /** 加载系统默认配置文件
    * 
    * @pdOid f74cccf2-5559-45af-b92f-220bb34c601a */
   public boolean loadConfigFile() {
      // TODO: implement
      return false;
   }
   
   public static void showSystemProperties(){
	   Set<Entry<Object, Object>> props = System.getProperties().entrySet();
		for (Entry prop : props) {

			System.out.println(prop.getKey().toString() + "="
					+ prop.getValue().toString());
		}
   }

}