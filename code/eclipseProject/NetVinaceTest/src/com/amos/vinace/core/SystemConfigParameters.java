/***********************************************************************
 * Module:  SystemConfigParameters.java
 * Author:  Administrator
 * Purpose: Defines the Class SystemConfigParameters
 ***********************************************************************/

package com.amos.vinace.core;

import java.io.File;
import java.util.*;

/** 系统主配置文件参数值类
 * 
 * @pdOid d59a55ac-6077-4927-b86b-44e07af6e8a8 */
public class SystemConfigParameters {
   /** 数据库连接URL
    * 
    * @pdOid be1e2282-5846-4740-8b4e-d5f7411bc41f */
   public static String DATA_BASE_URL;
   /** 数据库驱动类路径
    * 
    * @pdOid 227b2166-c33e-4209-af36-e9c3f9cd8e12 */
   public static String DATA_BASE_DRIVER_CLASS;
   /** 业务组件加载器类路径
    * 
    * @pdOid 8055c5c3-d368-4727-a798-b76a0b240f99 */
   public static String BUSINESS_CLASSES_LOADER;
   /** 系统日志目录
    * 
    * @pdOid 58259b8d-ba3b-46ff-b01e-2dd414d357cf */
   public static String SYSTRM_LOGS_FOLDER;
   /** 系统默认日期格式
    * 
    * @pdOid 84d1e8e7-a3f0-4766-808c-df79b482274a */
   public static String SYSTEM_DEFAULT_DATE_FORMAT;
   /** 系统默认时间格式
    * 
    * @pdOid 159f1239-1053-460c-9e1c-29b5acc7aac9 */
   public static String SYSTEM_DEFAULT_TIME_FORMAT;
   /** 系统业务组件配置文件列表
    * 
    * @pdOid 897e7524-bfe4-4c30-ab80-ce0672521435 */
   public static java.util.List BUSINESS_CLASSES_CONFIG_FILES=new ArrayList(0);
   /** 数据库连接池初始化大小
    * 
    * @pdOid f6aec281-5fab-4799-afd1-b3b3e18e8c82 */
   public static int DATA_BASE_CONNECTION_POOL_INIT_SIZE;
   /** 数据库连接池递增大小
    * 
    * @pdOid 3477a3b5-e657-4573-b1e6-a2994dc0e14d */
   public static int DATA_BASE_CONNECTION_POOL_INCREATMENT;
   
   /**项目根目录
    * 
    */
   public static String PROJECT_ROOT;
   
   
   public static String BUSINESS_CLASSES_FOLDER;
   
 
   
   
/**
 * 文件名前置标识
 */
   public static String CLASS_FILES_PREFIX=new String(); 

   /**
    *  文件名后置标识
    */
   public static String CLASS_FILES_ENDTAG=new String(); 
   
   
   public static String SYSTEM_CLASS_PATH=PROJECT_ROOT +"\\"+BUSINESS_CLASSES_FOLDER;
   
   /** @pdOid ed17bf7c-41d9-40d7-8824-1ba453424e6e */
   public String getDATA_BASE_URL() {
      return DATA_BASE_URL;
   }
   
   /** @pdOid 0d52b060-2c34-498c-9d79-20d58315f83f */
   public String getDATA_BASE_DRIVER_CLASS() {
      return DATA_BASE_DRIVER_CLASS;
   }
   
   /** @pdOid 28059ef2-5b5b-4bb4-bcd8-d3895098aacf */
   public String getBUSINESS_CLASSES_LOADER() {
      return BUSINESS_CLASSES_LOADER;
   }
   
   /** @pdOid 67ba1e74-3ba6-47e6-b691-314cfe45f610 */
   public String getSYSTRM_LOGS_FOLDER() {
      return SYSTRM_LOGS_FOLDER;
   }
   
   /** @pdOid 9943dc67-b306-41d5-8ba6-ca814b40b66e */
   public String getSYSTEM_DEFAULT_DATE_FORMAT() {
      return SYSTEM_DEFAULT_DATE_FORMAT;
   }
   
   /** @pdOid 499dbc6d-5133-43f8-9b6c-6e1940f6bc23 */
   public String getSYSTEM_DEFAULT_TIME_FORMAT() {
      return SYSTEM_DEFAULT_TIME_FORMAT;
   }
   
   /** @pdOid f50a7c5b-2e26-49a6-bedd-07fe97228759 */
   public java.util.List getBUSINESS_CLASSES_CONFIG_FILES() {
      return BUSINESS_CLASSES_CONFIG_FILES;
   }
   
   /** @pdOid 81dbb601-12f9-4ff1-861e-7dcad7504586 */
   public int getDATA_BASE_CONNECTION_POOL_INIT_SIZE() {
      return DATA_BASE_CONNECTION_POOL_INIT_SIZE;
   }
   
   /** @pdOid 4bd1b722-42fb-4101-a95d-e65c609b5156 */
   public int getDATA_BASE_CONNECTION_POOL_INCREATMENT() {
      return DATA_BASE_CONNECTION_POOL_INCREATMENT;
   }
   
   public static int addConfigFile(File file){
	   
	   BUSINESS_CLASSES_CONFIG_FILES.add(file);
	   
	   return BUSINESS_CLASSES_CONFIG_FILES.size();
   }
   
   

}