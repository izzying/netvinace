/**
 * 
 */
package com.amos.vinace.core;

/**
 * @author Administrator
 *
 */
public class ObjectGenator {

	/**
	 * 
	 */
	private static SystemResourceLoader resourceLoader;
	
	private ObjectGenator() {
		
	}
	
	public static SystemResourceLoader newInstance(){
		if(resourceLoader==null){
			SystemResourceLoader resourceLoader=new SystemResourceLoaderImpl();
			setResourceLoader(resourceLoader);
		}
		
		return resourceLoader;
	}

	public static SystemResourceLoader getResourceLoader() {
		return resourceLoader;
	}

	public static void setResourceLoader(SystemResourceLoader resourceLoader) {
		ObjectGenator.resourceLoader = resourceLoader;
	}
	
	

}
