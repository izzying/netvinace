package com.amos.vinace.core;

import java.util.ArrayList;

import com.amos.vinace.core.listener.CommonListener;

public class SystemWatcherImpls extends SystemWatchedResource {

	
	private String opeartion;

	private String opeart="A";
	
	public SystemWatcherImpls(){
		
		//监听者（观众）列表
		systemListeners=new ArrayList<CommonListener>(0);
	}
	
	@Override
	public int addListener(CommonListener listener) {
		// TODO Auto-generated method stub
		systemListeners.add(listener);
		return systemListeners.size();
	}

	@Override
	public int deleteListener(CommonListener listener) {
		// TODO Auto-generated method stub
		if(systemListeners.contains(listener)){
			systemListeners.remove(listener);
		}
		return systemListeners.size();
	}

	@Override
	public int notifyListeners() {
		// TODO Auto-generated method stub
		
		for(int i=0;i<systemListeners.size();i++){
			CommonListener listener=systemListeners.get(i);
			listener.statusChanged(opeartion);
		}
		return systemListeners.size();
	}

	public String getOpeartion() {
		return opeartion;
	}

	public void setOpeartion(String opeartion) {
		this.opeartion = opeartion;
		if(!this.opeartion.equalsIgnoreCase(opeart)){
			notifyListeners();
		}
		
	}

	

}
