package com.amos.vinace.core;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ParameterUnquireException extends Exception {

	private String message;

	
	private ParameterUnquireException(String msg){
		this.message=msg;
	}
	
	public ParameterUnquireException(String msg,Class clz)
	{
		
		this(msg);
		Logger.getLogger(msg, clz.getName()).log(Level.CONFIG, message, clz);;
	}

	public ParameterUnquireException(String msg, String parmName) {
		this(msg);
		Logger.getLogger(msg, parmName).log(Level.CONFIG, message, parmName);
	}
	
}
