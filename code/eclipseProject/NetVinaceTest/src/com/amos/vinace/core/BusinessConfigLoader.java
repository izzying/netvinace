/***********************************************************************
 * Module:  BusinessConfigLoader.java
 * Author:  Administrator
 * Purpose: Defines the Class BusinessConfigLoader
 ***********************************************************************/

package com.amos.vinace.core;


/** 系统业务组件配置文件加载组件
 * 
 * @pdOid 9c6f2614-d677-47c6-940b-0a2238df64d5 */
public abstract class BusinessConfigLoader implements IBusinessConfigLoader {
   /** 加载系统默认业务配置文件
    * 
    * @pdOid 9021a965-7e47-4e86-9c9d-4d0d4950a123 */
   public abstract boolean loadBusinessConfigFile();
   /** 以文件实体方式加载用户自定义业务配置文件，如果加载失败，则加载系统默认业务配置文件
    * 
    * @param businessConfigFile
    * @pdOid 0ecf07a3-bf38-401c-8977-0bbe6a02a09f */
   public abstract boolean oadBusinessConfigFile(java.io.File businessConfigFile);
   /** 以文件地址方式加载用户自定义业务配置文件，如果加载失败，则加载系统默认业务配置文件
    * 
    * @param businessConfigFileURL
    * @pdOid f747a0b6-a5a2-4796-bdaf-b916e3e4ad42 */
   public abstract boolean loadBusinessConfigFile(java.lang.String businessConfigFileURL);

}