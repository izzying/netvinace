package com.amos.vinace.test;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.filechooser.FileFilter;

import com.amos.vinace.core.SystemConfigParameters;
import com.amos.vinace.core.SystemResourceLoader;

public class ClassInforScanner {

	private static String rootPackageURL;
	
	private static ClassInforScanner scanner;

	private static String currentFolder;

	private static List<File> clzFileList = new ArrayList<File>(0);

	private FilenameFilter filters;

	private ClassInforScanner(String root) {

		
	}

	public static ClassInforScanner  newInstance(String root){
		
		rootPackageURL = root;
		
		currentFolder=rootPackageURL;
		
		if(scanner==null){
			scanner=new ClassInforScanner(rootPackageURL);
		}
		return scanner;
	}
	
	public List<File> scanRoot() {
		File[] files = null;
		File root = new File(currentFolder);

		files = root.listFiles();
		for (File file : files) {
			// System.out.println(file.getAbsolutePath());
			if (file.exists()&&file.isFile()) {
				clzFileList.add(file);
				
			} else {
				currentFolder=file.getAbsolutePath();
				scanRoot();
			}
		}
		
		
		
		return clzFileList;
	}
	
	public void transfer2ClassPath(List<File> FileList){
		
		clzFileList=FileList;
		
		System.out.println("------------list classFile start--------------------");
		 for(File file:clzFileList){
			 String packageName=(file.getParent().replace(SystemConfigParameters.SYSTEM_CLASS_PATH, "").toString()+".").toString();
			 packageName=packageName.trim().replace("\\", ".");
			 if(packageName.startsWith(".")){
				 packageName=packageName.substring(1);
			 }
			 String className=file.getName().replace(".class", "");
			 String clzFullPath=packageName+className;
			 System.out.println("find class: "+clzFullPath);
			 SystemResourceLoader.addClassFile(clzFullPath, file);
		 }
		 System.out.println("------------list classFile end--------------------");
		 System.out.print("\n");
		 System.out.println("total scan:"+clzFileList.size()+" files");

	}

}
