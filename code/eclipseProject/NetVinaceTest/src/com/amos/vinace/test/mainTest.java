package com.amos.vinace.test;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map.Entry;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import com.amos.vinace.common.DatabaseConnectionFactory;
import com.amos.vinace.common.DateUtil;
import com.amos.vinace.core.LoggerFactory;
import com.amos.vinace.core.ObjectGenator;
import com.amos.vinace.core.SystemConfigFileLoaderImpls;
import com.amos.vinace.core.SystemConfigLoader;
import com.amos.vinace.core.SystemConfigParameters;
import com.amos.vinace.core.SystemLogger;
import com.amos.vinace.core.SystemResourceLoader;
import com.amos.vinace.core.SystemWatcherImpls;
import com.amos.vinace.core.listener.CommonListener;
import com.amos.vinace.core.listener.FileSystemListener;
import com.amos.vinace.core.listener.SystemLogListener;

public class mainTest {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SystemConfigFileLoaderImpls config = new SystemConfigFileLoaderImpls();
		config.loadConfigFile();
		initSystemLogger();

		// 打印系统配置参数
		SystemConfigLoader.showSystemProperties();
		ClassInforScanner scanner = ClassInforScanner
				.newInstance(SystemConfigParameters.SYSTEM_CLASS_PATH);
		System.out.println(SystemConfigParameters.SYSTEM_CLASS_PATH);
		// 扫描类文件 默认路径可配置
		List<File> clzFileList = scanner.scanRoot();
		scanner.transfer2ClassPath(clzFileList);

//		FileSystemListener fileSystemListenTask = new FileSystemListener();
//		
//		Thread thread = new Thread(fileSystemListenTask);
//		fileSystemListenTask.addListener(config.getResourceLoader());
//		thread.start();
	

		// if(!"".equalsIgnoreCase(SystemConfigParameters.CLASS_FILES_PREFIX)&&!"".equalsIgnoreCase(SystemConfigParameters.CLASS_FILES_ENDTAG)){
		//
		// System.out.println("------------list classFile start--------------------");
		// for(File file:clzFileList){
		// System.out.println(file.getName());
		// }
		// System.out.println("------------list classFile end--------------------");
		// }

		// dbFactoryTest();
		// TestService service=new TestServiceImpls();
		// loggerProxy(service);
		// SystemLogger logger = LoggerFactory.newInstance();
		// CommonListener listener = new SystemLogListener(logger);
		// SystemWatcherImpls watcher = new SystemWatcherImpls();
		// watcher.addListener(listener);
		// watcher.setOpeartion("B");
		// try {
		// Thread.currentThread().sleep(2000);
		// watcher.setOpeartion("C");
		//
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
	}

	private static void loggerProxy(Object obj) {
		// TODO Auto-generated method stub

		LoggerFactory logFactory = new LoggerFactory(obj);
		TestService p = (TestService) Proxy.newProxyInstance(obj.getClass()
				.getClassLoader(), obj.getClass().getInterfaces(), logFactory);
		p.test("check database");

	}


	
	/**
	 * 系统初始化方法
	 * 
	 * @see com.amos.vinace.core.SystemConfigFileLoaderImpls.java
	 * @author Administrator
	 * 
	 */
	
	
	
	/**
	 * 数据库连接池类测试方法
	 * 
	 * @see com.amos.vinace.common.DatabaseConnectionFactory.java
	 * @author Administrator
	 * 
	 */
	private static void dbFactoryTest() {
		DatabaseConnectionFactory confactory = DatabaseConnectionFactory
				.initConnectionPool();

		long[] uids = new long[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		for (long uid : uids) {
			Connection con = confactory.getDBConnection(uid);

			test(uid, con);

			try {
				Thread.currentThread().sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			}
		}
		try {
			System.out.println("connection has used "
					+ confactory.getConnectionUsed().size());
			Thread.currentThread().sleep(2000);
			for (long uid : uids) {
				System.out.println("before disconnect "
						+ confactory.getConnectionFree().size());
				confactory.freeConnection(uid);
				System.out.println("after disconnect "
						+ confactory.getConnectionFree().size());

				Thread.currentThread().sleep(2000);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	
	/**
	 * 数据库操作类测试方法
	 * 
	 * @author Administrator
	 * 
	 */
	private static void test(long uid, Connection con) {
		ResultSet rs = null;
		PreparedStatement prstm = null;
		try {
			prstm = con.prepareStatement("select * from test where test.uid=?");
			prstm.setLong(1, uid);
			rs = prstm.executeQuery();
			if (rs.next()) {
				System.out.println("uid=" + uid + "name is"
						+ rs.getString("username"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				prstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public static void initSystemLogger() {
		SystemResourceLoader resourceLoader = ObjectGenator.newInstance();
		Object obj = resourceLoader
				.genateObject("com.amos.vinace.core.LoggerFactory");
		// System.out.println(obj.getClass().toString());

		try {
			Object log = obj.getClass().getMethod("newInstance", null)
					.invoke(obj, null);
			// try {
			// Field logObj = obj.getClass().getField("logger");
			// PropertyDescriptor pd = new PropertyDescriptor(
			// logObj.getName(), logObj.getDeclaringClass());
			// Object logs = pd.getReadMethod().invoke(logObj);
			// logs.getClass().getMethod("writeLogFile", String.class)
			// .invoke(logs, "hello world");

			// } catch (NoSuchFieldException ex1) {
			// // TODO Auto-generated catch block
			// ex1.printStackTrace();
			// } catch (IntrospectionException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }

		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException ex3) {
			// TODO Auto-generated catch block
			ex3.printStackTrace();
		} catch (IllegalAccessException ex4) {
			// TODO Auto-generated catch block
			ex4.printStackTrace();
		} catch (InvocationTargetException ex5) {
			// TODO Auto-generated catch block
			ex5.printStackTrace();
		} catch (NoSuchMethodException ex6) {
			// TODO Auto-generated catch block
			ex6.printStackTrace();
		}
	}

}
