package com.amos.vinace.common.dao;

import java.util.List;

public interface ISystemCommonDao {

	public abstract int saveData(SystemBusinessEntry entry);

	/**
	 * 
	 * @param entry
	 * @return int
	 * @category 更新数据
	 */

	public abstract int updateData(SystemBusinessEntry entry);

	/**
	 * 
	 * @param entryClass
	 * @param id
	 * @return int
	 * @category 删除单条数据
	 */

	public abstract int deleteData(Class entryClass, String id);

	/**
	 * @author Administrator
	 * @category 批量删除数据
	 * @param entryClass,ids
	 * @return int
	 * @exception Exception
	 * @since 1.0.0   2011-01-19
	 */

	public abstract int deleteDatas(Class entryClass, String[] ids);

	/**
	 * 
	 * @param entryClass
	 * @param pageNo
	 * @param pageSize
	 * @return PageUtil
	 * @category DAO查询分页方法
	 */
	public abstract PageUtil queryAllData(Class entryClass, int pageNo,
			int pageSize);

	/**
	 * @category DAO查询非分页方法
	 * @param entryClass
	 * @return
	 */
	public abstract List<ISystemBusinessEntry> queryAllData(Class entryClass);

	public abstract ISystemBusinessEntry querySingleData(Class entryClass,
			String id);

	/**
	 * 
	 * @param condition
	 * @param entryClass
	 * @return PageUtil
	 * @category DAO条件查询分页方法
	 */

	public abstract PageUtil queryDataByCondition(String condition,
			Class entryClass, int pageNo, int pageSize);

	/**
	 * 
	 * @param condition
	 * @param entryClass
	 * @return
	 * @category DAO条件查询非分页方法
	 */
	public abstract List<SystemBusinessEntry> queryDataByCondition(
			String condition, Class entryClass, boolean distinct);

	/**
	 * 
	 * @param sql
	 * @return  List
	 * @category SQL查询方法
	 */

	public abstract List queryDataBySQL(String sql);

	/**
	 * 
	 * @param sql
	 * @return  List
	 * @category SQL更新方法
	 */

	public abstract void updateDataBySQL(String sql);

}