package com.amos.vinace.common.dao;

import java.util.List;

/**
 * 该类为系统默认DAO实现类
 * 该类继承 com.amos.vinace.common.dao.SystemCommonDao
 * 沿用原有的方法。如用户需要改写所有方法，可直接实现
 * 接口com.amos.vinace.common.dao.ISystemCommonDao
 * @see com.amos.vinace.common.dao.SystemCommonDao
 * 
 * @author Administrator
 *
 */
public class SystemCommonDaoImpl extends SystemCommonDao {

	@Override
	public int saveData(SystemBusinessEntry entry) {
		// TODO Auto-generated method stub
		return super.saveData(entry);
	}

	@Override
	public int updateData(SystemBusinessEntry entry) {
		// TODO Auto-generated method stub
		return super.updateData(entry);
	}

	@Override
	public int deleteData(Class entryClass, String id) {
		// TODO Auto-generated method stub
		return super.deleteData(entryClass, id);
	}

	@Override
	public int deleteDatas(Class entryClass, String[] ids) {
		// TODO Auto-generated method stub
		return super.deleteDatas(entryClass, ids);
	}

	@Override
	public PageUtil queryAllData(Class entryClass, int pageNo, int pageSize) {
		// TODO Auto-generated method stub
		return super.queryAllData(entryClass, pageNo, pageSize);
	}

	@Override
	public List<ISystemBusinessEntry> queryAllData(Class entryClass) {
		// TODO Auto-generated method stub
		return super.queryAllData(entryClass);
	}

	@Override
	public ISystemBusinessEntry querySingleData(Class entryClass, String id) {
		// TODO Auto-generated method stub
		return super.querySingleData(entryClass, id);
	}

	@Override
	public PageUtil queryDataByCondition(String condition, Class entryClass,
			int pageNo, int pageSize) {
		// TODO Auto-generated method stub
		return super.queryDataByCondition(condition, entryClass, pageNo, pageSize);
	}

	@Override
	public List<SystemBusinessEntry> queryDataByCondition(String condition,
			Class entryClass, boolean distinct) {
		// TODO Auto-generated method stub
		return super.queryDataByCondition(condition, entryClass, distinct);
	}

	@Override
	public List queryDataBySQL(String sql) {
		// TODO Auto-generated method stub
		return super.queryDataBySQL(sql);
	}

	@Override
	public void updateDataBySQL(String sql) {
		// TODO Auto-generated method stub
		super.updateDataBySQL(sql);
	}

	

}
