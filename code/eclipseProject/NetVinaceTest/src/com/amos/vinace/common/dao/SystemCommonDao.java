package com.amos.vinace.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;



/**
 * 
 * 该类复制自“通用管理框架”中的com.amos.dao.CommonDao.java
 * 解除对org.springframework.orm.hibernate3.support.HibernateDaoSuport的依赖
 * 系统默认数据库操作类，实现了数据表常规增删查改功能,
 * 该类设计成抽象类，方便用户改写部分方法的同时也可以
 * 沿用原有的方法。如用户需要改写所有方法，可直接实现
 * 接口com.amos.vinace.common.dao.ISystemCommonDao
 * 
 * @see com.amos.vinace.common.dao.ISystemCommonDao
 * 
 * @author Administrator
 *
 */
public abstract class SystemCommonDao implements ISystemCommonDao   {

	private Connection dbCon;
	
	public ResultSet rs = null;
	
	public  PreparedStatement prstm = null;
	
	/* (non-Javadoc)
	 * @see com.amos.vinace.common.dao.ISystemCommonDao#saveData(com.amos.vinace.common.dao.SystemBusinessEntry)
	 */
	@Override
	public int saveData(SystemBusinessEntry entry){
		
		return 0;
		
		}
	
	/* (non-Javadoc)
	 * @see com.amos.vinace.common.dao.ISystemCommonDao#updateData(com.amos.vinace.common.dao.SystemBusinessEntry)
	 */
	
	@Override
	public int updateData( SystemBusinessEntry entry){
		
		return 0;
		
	}
	
	
	/* (non-Javadoc)
	 * @see com.amos.vinace.common.dao.ISystemCommonDao#deleteData(java.lang.Class, java.lang.String)
	 */
	
	@Override
	public int deleteData(Class entryClass,String id){
		int sign=0;
		return 0;
		
		}
	
	
	
	/* (non-Javadoc)
	 * @see com.amos.vinace.common.dao.ISystemCommonDao#deleteDatas(java.lang.Class, java.lang.String[])
	 */

	@Override
	public int deleteDatas(Class entryClass,String[] ids){
			int sign=0;
			return sign;
		
		}
	
	
	/* (non-Javadoc)
	 * @see com.amos.vinace.common.dao.ISystemCommonDao#queryAllData(java.lang.Class, int, int)
	 */
	@Override
	public PageUtil queryAllData(Class entryClass,final int pageNo,final int pageSize){
		return null;
		
		
	}
	
	/* (non-Javadoc)
	 * @see com.amos.vinace.common.dao.ISystemCommonDao#queryAllData(java.lang.Class)
	 */
	@Override
	public List<ISystemBusinessEntry> queryAllData(Class entryClass){
		return null;
		
		
	}
	
	
	/* (non-Javadoc)
	 * @see com.amos.vinace.common.dao.ISystemCommonDao#querySingleData(java.lang.Class, java.lang.String)
	 */
	@Override
	public ISystemBusinessEntry querySingleData(Class entryClass,String id){
		return null;
		
		
	}
	
	/* (non-Javadoc)
	 * @see com.amos.vinace.common.dao.ISystemCommonDao#queryDataByCondition(java.lang.String, java.lang.Class, int, int)
	 */
	
	@Override
	public PageUtil queryDataByCondition(String condition,Class entryClass,final int pageNo,final int pageSize){
		return null;
		
		
	}
	
	/* (non-Javadoc)
	 * @see com.amos.vinace.common.dao.ISystemCommonDao#queryDataByCondition(java.lang.String, java.lang.Class, boolean)
	 */
	@Override
	public List<SystemBusinessEntry> queryDataByCondition(String condition,Class entryClass,boolean distinct){
		
		String HQL=new String();
		 
		return null;
		
	}
	

	/* (non-Javadoc)
	 * @see com.amos.vinace.common.dao.ISystemCommonDao#queryDataBySQL(java.lang.String)
	 */
	
	@Override
	public List queryDataBySQL( final String sql){
		return null;
			
		
	}

	
	/* (non-Javadoc)
	 * @see com.amos.vinace.common.dao.ISystemCommonDao#updateDataBySQL(java.lang.String)
	 */
	
	@Override
	public void updateDataBySQL( final String sql){
	
	
	}
}
