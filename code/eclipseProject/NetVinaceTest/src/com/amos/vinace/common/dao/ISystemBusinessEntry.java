/**
 * 
 */
package com.amos.vinace.common.dao;

import java.io.Serializable;

/**
 * 系统业务实体接口，统一实体类接口，方便dao以及相关反射操作
 * @author Administrator
 *
 */
public interface ISystemBusinessEntry extends Serializable{

}
