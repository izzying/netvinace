/***********************************************************************
 * Module:  IBusinessConfigLoader.java
 * Author:  Administrator
 * Purpose: Defines the Interface IBusinessConfigLoader
 ***********************************************************************/

package com.amos.vinace.core;

import java.util.*;

/** 系统业务组件配置文件读取组件接口
 * 
 * @pdOid 43440115-60d5-4903-a17c-2be1d806281a */
public interface IBusinessConfigLoader {
   /** 加载系统默认业务配置文件
    * 
    * @pdOid e55228d8-3f66-43b9-b59e-b6909524808b */
   boolean loadBusinessConfigFile();
   /** 以文件实体方式加载用户自定义业务配置文件，如果加载失败，则加载系统默认业务配置文件
    * 
    * @param businessConfigFile
    * @pdOid 35baec9d-f56d-480e-81a3-a5f58d74e3a3 */
   boolean oadBusinessConfigFile(java.io.File businessConfigFile);
   /** 以文件地址方式加载用户自定义业务配置文件，如果加载失败，则加载系统默认业务配置文件
    * 
    * @param businessConfigFileURL
    * @pdOid 7d73da6a-17bf-4d83-ae05-fdc6fb1c5b92 */
   boolean loadBusinessConfigFile(java.lang.String businessConfigFileURL);

}