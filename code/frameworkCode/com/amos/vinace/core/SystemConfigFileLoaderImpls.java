/***********************************************************************
 * Module:  SystemConfigFileLoaderImpls.java
 * Author:  Administrator
 * Purpose: Defines the Class SystemConfigFileLoaderImpls
 ***********************************************************************/

package com.amos.vinace.core;

import java.util.*;

/** 系统配置文件加载组件实现类
 * 
 * @pdOid 895f6dc4-4de8-4993-a92e-4d0a227dc432 */
public class SystemConfigFileLoaderImpls extends SystemConfigLoader {
   /** 以文件方式加载用户指定配置文件，当加载失败时，抛出异常并加载系统默认配置文件，
    * 
    * @param configFile
    * @pdOid 4ad94e97-463b-490b-a465-b803e94e5a93 */
   public boolean loadConfigFile(java.io.File configFile) {
      // TODO: implement
      return false;
   }
   
   /** 以文件路径方式加载用户指定配置文件，当加载失败时，抛出异常并加载系统默认配置文件，
    * 
    * @param configFileURL
    * @pdOid 4faf7bd6-c06f-4302-989a-653a5e24df41 */
   public boolean loadConfigFile(java.lang.String configFileURL) {
      // TODO: implement
      return false;
   }
   
   /** 加载系统默认配置文件
    * 
    * @pdOid 86259ffd-80d5-4892-a22d-e215bd9e9682 */
   public boolean loadConfigFile() {
      // TODO: implement
      return false;
   }

}