/***********************************************************************
 * Module:  ISystemLogger.java
 * Author:  Administrator
 * Purpose: Defines the Interface ISystemLogger
 ***********************************************************************/

package com.amos.vinace.core;

import java.util.*;

/** 系统日志（读写）组件接口
 * 
 * @pdOid b402ae50-6d13-4889-857a-26d5a9867f8a */
public interface ISystemLogger {
   /** 写日志文件，把日志内容写入系统当前日志文件中。
    * 
    * @param context
    * @pdOid 1b81fd16-4da2-4d0c-94f1-6b0f34863f14 */
   int writeLogFile(String context);
   /** 把日志内容写入用户指定的日志文件（路径形式）当中。如果写入失败，则把日志内容写入系统当前日志文件中。
    * 
    * @param context 
    * @param logFileURL
    * @pdOid c75bc496-e15e-4643-b99e-daf359f86e01 */
   int writeLogFile(String context, java.lang.String logFileURL);
   /** 把日志内容写入用户指定的日志文件（文件形式）当中。如果写入失败，则把日志内容写入系统当前日志文件中。
    * 
    * @param context 
    * @param logFile
    * @pdOid 6bd7b45c-089e-4399-9638-58f89a2ab7c3 */
   int writeLogFile(String context, java.io.File logFile);
   /** 以路径方式复制，迁移日志文件，支持批量复制。
    * 
    * @param oldFile 
    * @param newFile
    * @pdOid f8ca9f20-2ec8-4330-ba83-2eaa0d871ea0 */
   int copyLogFile(String oldFile, String newFile);
   /** 以文件实体方式迁移，复制日志文件,支持批量复制
    * 
    * @param oldFile 
    * @param newFile
    * @pdOid 88be47de-4043-4073-8ca7-a564750a4220 */
   int copyLogFile(java.io.File oldFile, java.io.File newFile);
   /** 以文件实体方式读取日志文件
    * 
    * @param logFile
    * @pdOid 1b815b64-33ed-4285-9ab8-2bb985499586 */
   String readLogFile(java.io.File logFile);
   /** 以文件路径方式读取日志文件
    * 
    * @param logFileURL
    * @pdOid d3cf6ac9-4fd6-4a49-b9ef-efdfb248c767 */
   String readLogFile(String logFileURL);
   /** 查询日志文件指定内容
    * 
    * @param context 
    * @param logFile
    * @pdOid 97cbd741-a385-4397-9a9c-15704cea258d */
   String queryLog(String context, java.io.File logFile);
   /** 以路径方式查询日志文件内容
    * 
    * @param context 
    * @param logFileURL
    * @pdOid 484c230e-e52c-4e75-ba01-2dca71472a83 */
   String queryLog(String context, String logFileURL);
   /** 以路径方式更新日志文件内容
    * 
    * @param logFileURL
    * @pdOid db6dc9fe-3218-4c41-8d31-c59f190ae084 */
   int updateLog(String logFileURL);
   /** 以文件实体方式更新日志文件
    * 
    * @param logFile
    * @pdOid 87086876-e4e5-4c81-b8c8-f997460b4b2c */
   int updateLog(java.io.File logFile);
   /** 查询日志文件列表
    * 
    * @param fileDIR 文件目录
    * @pdOid 260ba87f-f34a-4a22-933f-f6dae2cd86cf */
   java.util.List listLogFiles(String fileDIR);
   /** 查询所有日志文件，获取系统日志文件列表
    * 
    * @pdOid 658e1382-8b90-484d-8a42-9b2b2a8faa9b */
   java.util.List listLogFiles();
   /** 获取特定文件后缀的日志列表
    * 
    * @param filtter
    * @pdOid 95291b77-ce63-4a54-809f-7448aa34da68 */
   java.util.List listLogFiles(java.io.FileFilter filtter);
   /** 在指定目录下获取特定文件后缀的日志列表（文件路径方法）
    * 
    * @param folderDIR 
    * @param filter
    * @pdOid fc96b2fe-c518-4bdf-99f8-7353d375f140 */
   java.util.List listLogFiles(String folderDIR, java.io.FileFilter filter);
   /** 在指定目录下获取特定文件后缀的日志列表（文件实体方法）
    * 
    * @param folder 
    * @param filter
    * @pdOid 239bfc86-ecaf-4896-bbd6-2c54542625ee */
   java.util.List listLogFiles(java.io.File folder, java.io.FileFilter filter);

}