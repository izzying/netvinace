/***********************************************************************
 * Module:  BusinessConfigParameters.java
 * Author:  Administrator
 * Purpose: Defines the Class BusinessConfigParameters
 ***********************************************************************/

package com.amos.vinace.core;

import java.util.*;

/** 系统业务配置文件参数值类
 * 
 * @pdOid a836f5b7-bba4-40dc-ab76-7a379036cb6a */
public class BusinessConfigParameters {
   /** @pdOid 6cb3bace-ea1b-4f24-b684-2392d936868e */
   private java.lang.Boolean IF_RETIME_SCAN_BUSINESS_CLASSES;
   /** @pdOid 3121cdb8-6484-42f0-b5d4-5b72ecfc6e40 */
   private java.lang.Long SCAN_SPEED;

}