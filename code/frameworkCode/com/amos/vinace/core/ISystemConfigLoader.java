/***********************************************************************
 * Module:  ISystemConfigLoader.java
 * Author:  Administrator
 * Purpose: Defines the Interface ISystemConfigLoader
 ***********************************************************************/

package com.amos.vinace.core;

import java.util.*;

/** 系统配置文件加载组件接口
 * 
 * 1.0.0
 * 御风林海
 * 1.0.0
 * @pdOid f157eb13-c057-43c4-9ba9-03e431c35e37 */
public interface ISystemConfigLoader {
   /** 以文件方式加载用户指定配置文件，当加载失败时，抛出异常并加载系统默认配置文件，
    * 
    * @param configFile
    * @pdOid c935bb88-86ed-4e01-a629-b8b87e7c335f */
   boolean loadConfigFile(java.io.File configFile);
   /** 以文件路径方式加载用户指定配置文件，当加载失败时，抛出异常并加载系统默认配置文件，
    * 
    * @param configFileURL
    * @pdOid 66221fff-6527-46f6-b290-aeed93e34c60 */
   boolean loadConfigFile(java.lang.String configFileURL);
   /** 加载系统默认配置文件
    * 
    * @pdOid 4ed72f23-882a-43a6-b633-e79baeaebec8 */
   boolean loadConfigFile();

}