/***********************************************************************
 * Module:  SystemResourceLoaderImpl.java
 * Author:  Administrator
 * Purpose: Defines the Class SystemResourceLoaderImpl
 ***********************************************************************/

package com.amos.vinace.core;

import java.util.*;

/** 系统资源加载类实现，使用反射原理加载系统组件，相当于spring中的beanfactory
 * 
 * @pdOid 80d1b43b-b27f-4901-ae75-d482463d5dc2 */
public class SystemResourceLoaderImpl extends SystemResourceLoader {
   /** 反射类对象生成器。
    * 
    * @param stringClassPath
    * @pdOid b5b963c7-f602-41d3-96bd-e357f496c1aa */
   public <T> java.lang.Object genateObject(String stringClassPath) {
      // TODO: implement
      return null;
   }
   
   /** @param objkey
    * @pdOid 3967a478-c3de-4019-a201-2ef13d22b6f2 */
   public java.lang.Object getSystemObject(java.lang.Long objkey) {
      // TODO: implement
      return null;
   }
   
   /** @param obj
    * @pdOid ad0abd5f-e192-4c8d-b94a-a874524e6f5e */
   public java.lang.Long getSystemObjectKey(java.lang.Object obj) {
      // TODO: implement
      return null;
   }

}