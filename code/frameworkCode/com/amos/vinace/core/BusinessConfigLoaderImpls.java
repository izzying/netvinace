/***********************************************************************
 * Module:  BusinessConfigLoaderImpls.java
 * Author:  Administrator
 * Purpose: Defines the Class BusinessConfigLoaderImpls
 ***********************************************************************/

package com.amos.vinace.core;

import java.util.*;

/** 系统业务组件配置文件读取组件实现类
 * 
 * @pdOid abdb21f8-9c4e-43b3-ac64-be855bbb3540 */
public class BusinessConfigLoaderImpls extends BusinessConfigLoader {
   /** 加载系统默认业务配置文件
    * 
    * @pdOid 512514a2-7dd4-41c6-9d69-c084dbf3aa27 */
   public boolean loadBusinessConfigFile() {
      // TODO: implement
      return false;
   }
   
   /** 以文件实体方式加载用户自定义业务配置文件，如果加载失败，则加载系统默认业务配置文件
    * 
    * @param businessConfigFile
    * @pdOid 3735b7f9-2196-44ce-8ab6-2faa4c5728c1 */
   public boolean oadBusinessConfigFile(java.io.File businessConfigFile) {
      // TODO: implement
      return false;
   }
   
   /** 以文件地址方式加载用户自定义业务配置文件，如果加载失败，则加载系统默认业务配置文件
    * 
    * @param businessConfigFileURL
    * @pdOid d90f3be3-99b9-40f7-b5b0-8bf83e40ec8b */
   public boolean loadBusinessConfigFile(java.lang.String businessConfigFileURL) {
      // TODO: implement
      return false;
   }

}