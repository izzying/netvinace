/***********************************************************************
 * Module:  SystemResourceLoader.java
 * Author:  Administrator
 * Purpose: Defines the Class SystemResourceLoader
 ***********************************************************************/

package com.amos.vinace.core;

import java.util.*;

/** 系统资源加载类抽象实现类，利用反射实现业务对象动态生成,重写该类方法需要重写hashCode()方法和equal()方法
 * 
 * @pdOid 70dd6214-ae8e-426f-8287-1f0dac0c4421 */
public abstract class SystemResourceLoader implements ISystemResourceLoader {
   /** 系统资源对象库
    * 
    * @pdOid c0158c93-a582-430b-be95-812898931bad */
   protected static java.utill.HashMap systemObjectList;
   
   /** 反射类对象生成器。
    * 
    * @param stringClassPath
    * @pdOid dd42f075-f52a-4802-9704-158e27f1d173 */
   public <T> java.lang.Object genateObject(String stringClassPath) {
      // TODO: implement
      return null;
   }
   
   /** @param objkey
    * @pdOid 28772748-ff93-44bc-b1db-4766fe9f80d3 */
   public abstract java.lang.Object getSystemObject(java.lang.Long objkey);
   /** @param obj
    * @pdOid a399fc1c-379a-4dc4-a2ce-53f10422845c */
   public abstract java.lang.Long getSystemObjectKey(java.lang.Object obj);
   /** 向资源库中加入对象实体
    * 
    * @param resource
    * @pdOid 984c4528-a5f5-433b-927a-2d9ad18a0435 */
   public static <T> int addObjectToList(java.lang.Object resource) {
      // TODO: implement
      return 0;
   }
   
   /** @param objKey
    * @pdOid 84919f99-5c67-458b-b07a-ffc285114bec */
   public static <T> int removeObjectFormList(long objKey) {
      // TODO: implement
      return 0;
   }

}