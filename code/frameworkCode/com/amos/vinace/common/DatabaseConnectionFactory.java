/***********************************************************************
 * Module:  DatabaseConnectionFactory.java
 * Author:  Administrator
 * Purpose: Defines the Class DatabaseConnectionFactory
 ***********************************************************************/

package com.amos.vinace.common;

import java.util.*;

/** 数据库连接类
 * 
 * @pdOid cdedddfe-67b5-47d1-8071-78536dacdc29 */
public class DatabaseConnectionFactory {
   /** @pdOid 2bcb18f3-239b-43f3-8130-129484bed781 */
   private DatabaseConnectionFactory() {
      // TODO: implement
   }
   
   /** @pdOid 3de35a8d-29a5-4889-9020-fe5ee39fe7eb */
   private static java.sql.Connection getNewDBConnection() {
      // TODO: implement
      return null;
   }
   
   /** @param uid 
    * @param dbCon
    * @pdOid 9418947d-1dc8-448d-9728-d98345dce276 */
   private int addConnection(long uid, java.sql.Connection dbCon) {
      // TODO: implement
      return 0;
   }
   
   /** @pdOid e6f03579-948b-40b0-997b-3ca4f1b303af */
   protected static java.util.HashMap connectionPool;
   
   /** @param uid
    * @pdOid a8d357f8-9b05-4662-b6f8-c7858c7f2e14 */
   public static int disConnectDB(long uid) {
      // TODO: implement
      return 0;
   }
   
   /** @param poolSize
    * @pdOid 715b0c97-9c65-4982-a7a9-f632afa126af */
   public static int initConnectionPool(int poolSize) {
      // TODO: implement
      return 0;
   }
   
   /** @pdOid 44534d35-0237-4060-a767-9b162dc91d34 */
   public java.util.HashMap getConnectionPool() {
      return connectionPool;
   }
   
   /** @param newConnectionPool
    * @pdOid 49965dbc-e59d-49d9-8db9-f0cf7318d682 */
   public void setConnectionPool(java.util.HashMap newConnectionPool) {
      connectionPool = newConnectionPool;
   }
   
   /** @param uid
    * @pdOid 9450ac2f-b13e-4a12-b4b7-cf81b4430b74 */
   public static java.sql.Connection getDBConnection(long uid) {
      // TODO: implement
      return null;
   }

}