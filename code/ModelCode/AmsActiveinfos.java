/***********************************************************************
 * Module:  AmsActiveinfos.java
 * Author:  Administrator
 * Purpose: Defines the Class AmsActiveinfos
 ***********************************************************************/

import java.util.*;

/** 活动/会议基本信息表
 * 
 * @pdOid fa435f09-5c5a-47cc-867c-627c65ae5836 */
public class AmsActiveinfos {
   /** @pdOid 808f7fe7-f1b2-4fb2-b02e-a07401f86d84 */
   public int actId;
   /** @pdOid c5431658-0530-4a10-835d-f6ae07747eaf */
   public java.lang.String actName;
   /** @pdOid 35877d9a-fe75-4a12-903e-f83d9f011051 */
   public int actStatus;
   /** @pdOid b8b1ca1d-a1fe-42b4-a5f6-3daa680cdf5f */
   public java.lang.String actDescp;
   /** @pdOid 50b6ebbe-d798-4fc8-b331-bd081b94823a */
   public java.lang.String actNote;
   /** @pdOid a8a9a84d-49be-4d45-a6cf-4a802bf889dc */
   public java.lang.String actAddress;
   /** @pdOid 775cbeb4-2630-4ba4-a7d1-84dee19c1626 */
   public java.util.Date starts;
   /** @pdOid a1220fe4-ba89-4625-8fd5-a306754b54d0 */
   public java.util.Date ends;
   
   /** 活动/会议信息与详情
    * 包括参与活动/会议人员信息 */
   /** @pdRoleInfo migr=no name=AmsActiveDetail assc=activeteInfors coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<AmsActiveDetail> amsActiveDetail;
   /** @pdRoleInfo migr=no name=Userinfos assc=checkerId mult=0..1 side=A */
   public Userinfos userinfos;
   
   /** @pdOid f4f21cf3-26f4-45ef-aefe-cb1c89720126 */
   public int getActId() {
      return actId;
   }
   
   /** @param newActId
    * @pdOid b04ff6a7-9063-4f06-b1fa-3ed2f17291c8 */
   public void setActId(int newActId) {
      actId = newActId;
   }
   
   /** @pdOid c8d0630f-c55f-4076-80d7-40138e0083d1 */
   public java.lang.String getActName() {
      return actName;
   }
   
   /** @param newActName
    * @pdOid c7a31dff-795e-440a-aae8-2c9444630364 */
   public void setActName(java.lang.String newActName) {
      actName = newActName;
   }
   
   /** @pdOid 3eb66aca-2c2b-42b3-b007-f844815a4fa0 */
   public int getActStatus() {
      return actStatus;
   }
   
   /** @param newActStatus
    * @pdOid 33d1796d-5d0c-4008-bf8e-8155e6525b6a */
   public void setActStatus(int newActStatus) {
      actStatus = newActStatus;
   }
   
   /** @pdOid 55d24531-2046-49f5-9b05-8c69249a6933 */
   public java.lang.String getActDescp() {
      return actDescp;
   }
   
   /** @param newActDescp
    * @pdOid a4de170e-1e67-4a8a-b62c-224b73f63839 */
   public void setActDescp(java.lang.String newActDescp) {
      actDescp = newActDescp;
   }
   
   /** @pdOid c0953e3e-4292-4b88-9c12-676f59a4386d */
   public java.lang.String getActNote() {
      return actNote;
   }
   
   /** @param newActNote
    * @pdOid e38ce250-9012-4fe2-a49a-b64b0c94366f */
   public void setActNote(java.lang.String newActNote) {
      actNote = newActNote;
   }
   
   /** @pdOid 82c53137-33e4-41ed-af14-531e850a7b48 */
   public java.lang.String getActAddress() {
      return actAddress;
   }
   
   /** @param newActAddress
    * @pdOid 694d2d34-0ea5-44f4-906a-3fe1a85e8eb8 */
   public void setActAddress(java.lang.String newActAddress) {
      actAddress = newActAddress;
   }
   
   /** @pdOid e05c9ac9-3993-4119-906c-8655c0c12807 */
   public java.util.Date getStarts() {
      return starts;
   }
   
   /** @param newStarts
    * @pdOid 38619a4a-9f6c-41ff-acf2-ce98a969d481 */
   public void setStarts(java.util.Date newStarts) {
      starts = newStarts;
   }
   
   /** @pdOid 5f3e7ce0-46b3-4cca-8e59-b5d71aac77fb */
   public java.util.Date getEnds() {
      return ends;
   }
   
   /** @param newEnds
    * @pdOid 203384ff-798e-4fcb-8c0e-5cb00e488399 */
   public void setEnds(java.util.Date newEnds) {
      ends = newEnds;
   }
   
   
   /** @pdGenerated default getter */
   public java.util.Collection<AmsActiveDetail> getAmsActiveDetail() {
      if (amsActiveDetail == null)
         amsActiveDetail = new java.util.HashSet<AmsActiveDetail>();
      return amsActiveDetail;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorAmsActiveDetail() {
      if (amsActiveDetail == null)
         amsActiveDetail = new java.util.HashSet<AmsActiveDetail>();
      return amsActiveDetail.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newAmsActiveDetail */
   public void setAmsActiveDetail(java.util.Collection<AmsActiveDetail> newAmsActiveDetail) {
      removeAllAmsActiveDetail();
      for (java.util.Iterator iter = newAmsActiveDetail.iterator(); iter.hasNext();)
         addAmsActiveDetail((AmsActiveDetail)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newAmsActiveDetail */
   public void addAmsActiveDetail(AmsActiveDetail newAmsActiveDetail) {
      if (newAmsActiveDetail == null)
         return;
      if (this.amsActiveDetail == null)
         this.amsActiveDetail = new java.util.HashSet<AmsActiveDetail>();
      if (!this.amsActiveDetail.contains(newAmsActiveDetail))
      {
         this.amsActiveDetail.add(newAmsActiveDetail);
         newAmsActiveDetail.setAmsActiveinfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldAmsActiveDetail */
   public void removeAmsActiveDetail(AmsActiveDetail oldAmsActiveDetail) {
      if (oldAmsActiveDetail == null)
         return;
      if (this.amsActiveDetail != null)
         if (this.amsActiveDetail.contains(oldAmsActiveDetail))
         {
            this.amsActiveDetail.remove(oldAmsActiveDetail);
            oldAmsActiveDetail.setAmsActiveinfos((AmsActiveinfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllAmsActiveDetail() {
      if (amsActiveDetail != null)
      {
         AmsActiveDetail oldAmsActiveDetail;
         for (java.util.Iterator iter = getIteratorAmsActiveDetail(); iter.hasNext();)
         {
            oldAmsActiveDetail = (AmsActiveDetail)iter.next();
            iter.remove();
            oldAmsActiveDetail.setAmsActiveinfos((AmsActiveinfos)null);
         }
      }
   }
   /** @pdGenerated default parent getter */
   public Userinfos getUserinfos() {
      return userinfos;
   }
   
   /** @pdGenerated default parent setter
     * @param newUserinfos */
   public void setUserinfos(Userinfos newUserinfos) {
      if (this.userinfos == null || !this.userinfos.equals(newUserinfos))
      {
         if (this.userinfos != null)
         {
            Userinfos oldUserinfos = this.userinfos;
            this.userinfos = null;
            oldUserinfos.removeAmsActiveinfos(this);
         }
         if (newUserinfos != null)
         {
            this.userinfos = newUserinfos;
            this.userinfos.addAmsActiveinfos(this);
         }
      }
   }

}