/***********************************************************************
 * Module:  AmsMsgType.java
 * Author:  Administrator
 * Purpose: Defines the Class AmsMsgType
 ***********************************************************************/

import java.util.*;

/** 消息类型信息表
 * 
 * @pdOid 74886ec6-7a5e-49c9-9657-f25c4c19566f */
public class AmsMsgType {
   /** @pdOid a12ac98b-f290-45f9-8602-5340ed3f2585 */
   public int mtid;
   /** @pdOid ed6c7759-e71f-4b4e-a4e1-218698f243dc */
   public java.lang.String typeName;
   /** @pdOid f4cf9e16-2817-40d1-a583-3f9f379dc665 */
   public java.lang.String handelClz;
   
   /** @pdRoleInfo migr=no name=AmsMsginfos assc=msgTypeInfor coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<AmsMsginfos> amsMsginfos;
   
   /** @pdOid 776c4120-4061-403c-87c9-9adfa1d0a838 */
   public int getMtid() {
      return mtid;
   }
   
   /** @param newMtid
    * @pdOid 97d9dd94-be13-43e7-8f14-d808e12bf158 */
   public void setMtid(int newMtid) {
      mtid = newMtid;
   }
   
   /** @pdOid 7251642b-0583-43e6-a8b5-c4a4638fbeec */
   public java.lang.String getTypeName() {
      return typeName;
   }
   
   /** @param newTypeName
    * @pdOid 22ca880a-824f-47e8-a2cf-85c52b9c13cc */
   public void setTypeName(java.lang.String newTypeName) {
      typeName = newTypeName;
   }
   
   /** @pdOid 908bb8be-08c0-47dc-ba34-d45b7fd4bed1 */
   public java.lang.String getHandelClz() {
      return handelClz;
   }
   
   /** @param newHandelClz
    * @pdOid 21494ea5-4ed4-4809-ba4c-dfc08d6a9326 */
   public void setHandelClz(java.lang.String newHandelClz) {
      handelClz = newHandelClz;
   }
   
   
   /** @pdGenerated default getter */
   public java.util.Collection<AmsMsginfos> getAmsMsginfos() {
      if (amsMsginfos == null)
         amsMsginfos = new java.util.HashSet<AmsMsginfos>();
      return amsMsginfos;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorAmsMsginfos() {
      if (amsMsginfos == null)
         amsMsginfos = new java.util.HashSet<AmsMsginfos>();
      return amsMsginfos.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newAmsMsginfos */
   public void setAmsMsginfos(java.util.Collection<AmsMsginfos> newAmsMsginfos) {
      removeAllAmsMsginfos();
      for (java.util.Iterator iter = newAmsMsginfos.iterator(); iter.hasNext();)
         addAmsMsginfos((AmsMsginfos)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newAmsMsginfos */
   public void addAmsMsginfos(AmsMsginfos newAmsMsginfos) {
      if (newAmsMsginfos == null)
         return;
      if (this.amsMsginfos == null)
         this.amsMsginfos = new java.util.HashSet<AmsMsginfos>();
      if (!this.amsMsginfos.contains(newAmsMsginfos))
      {
         this.amsMsginfos.add(newAmsMsginfos);
         newAmsMsginfos.setAmsMsgType(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldAmsMsginfos */
   public void removeAmsMsginfos(AmsMsginfos oldAmsMsginfos) {
      if (oldAmsMsginfos == null)
         return;
      if (this.amsMsginfos != null)
         if (this.amsMsginfos.contains(oldAmsMsginfos))
         {
            this.amsMsginfos.remove(oldAmsMsginfos);
            oldAmsMsginfos.setAmsMsgType((AmsMsgType)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllAmsMsginfos() {
      if (amsMsginfos != null)
      {
         AmsMsginfos oldAmsMsginfos;
         for (java.util.Iterator iter = getIteratorAmsMsginfos(); iter.hasNext();)
         {
            oldAmsMsginfos = (AmsMsginfos)iter.next();
            iter.remove();
            oldAmsMsginfos.setAmsMsgType((AmsMsgType)null);
         }
      }
   }

}