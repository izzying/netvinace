/***********************************************************************
 * Module:  AmsProtmDetail.java
 * Author:  Administrator
 * Purpose: Defines the Class AmsProtmDetail
 ***********************************************************************/

import java.util.*;

/** 项目组成员信息
 * 
 * @pdOid cccdbcf1-f1c1-4d3e-bb07-803ba66a17b3 */
public class AmsProtmDetail {
   /** @pdOid d2ffdfd2-d23a-4c38-a705-4793b8193ba7 */
   public int prmId;
   /** @pdOid ae0a0539-47b4-48df-91e0-5e92040b6395 */
   public java.lang.String prmNote;
   
   /** @pdRoleInfo migr=no name=AmsProtminfos assc=projectTeamMembers mult=0..1 side=A */
   public AmsProtminfos amsProtminfos;
   /** @pdRoleInfo migr=no name=Userinfos assc=projectMemberRef mult=0..1 side=A */
   public Userinfos userinfos;
   /** @pdRoleInfo migr=no name=AmsProjectinfos assc=projectInfosRef mult=0..1 side=A */
   public AmsProjectinfos amsProjectinfos;
   /** @pdRoleInfo migr=no name=AmsRoleinfos assc=projectMemberRole mult=0..1 side=A */
   public AmsRoleinfos amsRoleinfos;
   
   /** @pdOid 396420c5-2ff8-40a3-b697-1dc543403960 */
   public int getPrmId() {
      return prmId;
   }
   
   /** @param newPrmId
    * @pdOid e83e8dc1-fccf-41ae-8a9b-f01fedd60d39 */
   public void setPrmId(int newPrmId) {
      prmId = newPrmId;
   }
   
   /** @pdOid c647afc7-50b9-424e-966c-575c5dbb01f7 */
   public java.lang.String getPrmNote() {
      return prmNote;
   }
   
   /** @param newPrmNote
    * @pdOid 7c88f1cf-4997-4cea-b146-98f2cebf9393 */
   public void setPrmNote(java.lang.String newPrmNote) {
      prmNote = newPrmNote;
   }
   
   
   /** @pdGenerated default parent getter */
   public AmsProtminfos getAmsProtminfos() {
      return amsProtminfos;
   }
   
   /** @pdGenerated default parent setter
     * @param newAmsProtminfos */
   public void setAmsProtminfos(AmsProtminfos newAmsProtminfos) {
      if (this.amsProtminfos == null || !this.amsProtminfos.equals(newAmsProtminfos))
      {
         if (this.amsProtminfos != null)
         {
            AmsProtminfos oldAmsProtminfos = this.amsProtminfos;
            this.amsProtminfos = null;
            oldAmsProtminfos.removeAmsProtmDetail(this);
         }
         if (newAmsProtminfos != null)
         {
            this.amsProtminfos = newAmsProtminfos;
            this.amsProtminfos.addAmsProtmDetail(this);
         }
      }
   }
   /** @pdGenerated default parent getter */
   public Userinfos getUserinfos() {
      return userinfos;
   }
   
   /** @pdGenerated default parent setter
     * @param newUserinfos */
   public void setUserinfos(Userinfos newUserinfos) {
      if (this.userinfos == null || !this.userinfos.equals(newUserinfos))
      {
         if (this.userinfos != null)
         {
            Userinfos oldUserinfos = this.userinfos;
            this.userinfos = null;
            oldUserinfos.removeAmsProtmDetail(this);
         }
         if (newUserinfos != null)
         {
            this.userinfos = newUserinfos;
            this.userinfos.addAmsProtmDetail(this);
         }
      }
   }
   /** @pdGenerated default parent getter */
   public AmsProjectinfos getAmsProjectinfos() {
      return amsProjectinfos;
   }
   
   /** @pdGenerated default parent setter
     * @param newAmsProjectinfos */
   public void setAmsProjectinfos(AmsProjectinfos newAmsProjectinfos) {
      if (this.amsProjectinfos == null || !this.amsProjectinfos.equals(newAmsProjectinfos))
      {
         if (this.amsProjectinfos != null)
         {
            AmsProjectinfos oldAmsProjectinfos = this.amsProjectinfos;
            this.amsProjectinfos = null;
            oldAmsProjectinfos.removeAmsProtmDetail(this);
         }
         if (newAmsProjectinfos != null)
         {
            this.amsProjectinfos = newAmsProjectinfos;
            this.amsProjectinfos.addAmsProtmDetail(this);
         }
      }
   }
   /** @pdGenerated default parent getter */
   public AmsRoleinfos getAmsRoleinfos() {
      return amsRoleinfos;
   }
   
   /** @pdGenerated default parent setter
     * @param newAmsRoleinfos */
   public void setAmsRoleinfos(AmsRoleinfos newAmsRoleinfos) {
      if (this.amsRoleinfos == null || !this.amsRoleinfos.equals(newAmsRoleinfos))
      {
         if (this.amsRoleinfos != null)
         {
            AmsRoleinfos oldAmsRoleinfos = this.amsRoleinfos;
            this.amsRoleinfos = null;
            oldAmsRoleinfos.removeAmsProtmDetail(this);
         }
         if (newAmsRoleinfos != null)
         {
            this.amsRoleinfos = newAmsRoleinfos;
            this.amsRoleinfos.addAmsProtmDetail(this);
         }
      }
   }

}