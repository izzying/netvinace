/***********************************************************************
 * Module:  Userinfos.java
 * Author:  Administrator
 * Purpose: Defines the Class Userinfos
 ***********************************************************************/

import java.util.*;

/** 系统用户信息表
 * 
 * @pdOid 753be613-1abc-41d1-9a9c-549cb7754ce2 */
public class Userinfos {
   /** @pdOid d02a2a6e-ac62-435a-be02-585e66a9070d */
   public int uid;
   /** @pdOid f0cfe76a-9f87-4a62-8842-9f7b4e4861bd */
   public java.lang.String username;
   /** @pdOid 0b574cfa-812c-49e8-8048-db959c1d4570 */
   public java.lang.String pwd;
   /** @pdOid 74a683f7-de96-4765-a5bd-75529d12adab */
   public java.lang.String question;
   /** @pdOid 89b7490b-dfff-456b-9f14-45b06bb76c4e */
   public java.lang.String answer;
   /** @pdOid 0f04bb2b-32d1-4c20-9b25-6817ffe2e06e */
   public int status;
   /** @pdOid 73c218d0-0871-48e8-bb6f-29ea560ef3f7 */
   public java.lang.String phone;
   /** @pdOid d6b05cd4-8223-4a06-a784-77c87d905b17 */
   public java.lang.String qq;
   /** @pdOid 7ccc1aa2-e6a1-4204-9552-889a297b1c12 */
   public java.lang.String mobile;
   /** @pdOid c0841947-debb-48fa-ac8c-fa15071a9df2 */
   public java.lang.String mail;
   /** @pdOid 7c23c030-7718-4af9-a2ea-ca664f1dc93c */
   public java.lang.String addr;
   /** @pdOid 763ecab1-da16-45c7-834e-fcaedaeb1f49 */
   public java.lang.String note;
   
   /** @pdRoleInfo migr=no name=AmsActiveinfos assc=checkerId coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<AmsActiveinfos> amsActiveinfos;
   /** @pdRoleInfo migr=no name=AmsActiveDetail assc=memberId coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<AmsActiveDetail> amsActiveDetail;
   /** @pdRoleInfo migr=no name=AmsProtminfos assc=projectTeamLeader coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<AmsProtminfos> amsProtminfos;
   /** @pdRoleInfo migr=no name=AmsProtmDetail assc=projectMemberRef coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<AmsProtmDetail> amsProtmDetail;
   /** @pdRoleInfo migr=no name=Buginfos assc=uploaderInfosRef coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<Buginfos> buginfos;
   /** @pdRoleInfo migr=no name=Buginfos assc=handelInfosRef coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<Buginfos> buginfos;
   /** @pdRoleInfo migr=no name=Taskinfos assc=taskReceverInfosRef coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<Taskinfos> taskinfos;
   /** @pdRoleInfo migr=no name=Taskinfos assc=taskSenderInfosRef coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<Taskinfos> taskinfos;
   /** @pdRoleInfo migr=no name=AmsMsginfos assc=msgToMember coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<AmsMsginfos> amsMsginfos;
   
   /** @pdOid 92402386-57a8-4d47-9078-536a9c46f02c */
   public int getUid() {
      return uid;
   }
   
   /** @param newUid
    * @pdOid 39122581-4b87-44fe-a7a2-d0c172a68ab3 */
   public void setUid(int newUid) {
      uid = newUid;
   }
   
   /** @pdOid 4820b7e6-1e9a-42bd-bd29-b0261c6407f2 */
   public java.lang.String getUsername() {
      return username;
   }
   
   /** @param newUsername
    * @pdOid d746750b-6d28-4147-b145-801e5b94e7ca */
   public void setUsername(java.lang.String newUsername) {
      username = newUsername;
   }
   
   /** @pdOid 9d9f84ac-1872-47c4-bad0-85811d9f3bfa */
   public java.lang.String getPwd() {
      return pwd;
   }
   
   /** @param newPwd
    * @pdOid 73e0371f-1e42-4f3d-a881-d8d3cd6b03e7 */
   public void setPwd(java.lang.String newPwd) {
      pwd = newPwd;
   }
   
   /** @pdOid 45505640-65f4-4a66-a522-b6696dc4f0fc */
   public java.lang.String getQuestion() {
      return question;
   }
   
   /** @param newQuestion
    * @pdOid ac0798f1-2ed1-4e58-b816-81cd4de3776a */
   public void setQuestion(java.lang.String newQuestion) {
      question = newQuestion;
   }
   
   /** @pdOid 4ed31291-c3d7-4a8b-ae11-91b6e0f2db8c */
   public java.lang.String getAnswer() {
      return answer;
   }
   
   /** @param newAnswer
    * @pdOid a91d1ced-2e0f-4e08-938d-7306fef39ec4 */
   public void setAnswer(java.lang.String newAnswer) {
      answer = newAnswer;
   }
   
   /** @pdOid 402a92b5-52e8-421d-8a3b-cbf1d5f372f5 */
   public int getStatus() {
      return status;
   }
   
   /** @param newStatus
    * @pdOid 6d4032b2-d641-486c-9357-f969f991b09d */
   public void setStatus(int newStatus) {
      status = newStatus;
   }
   
   /** @pdOid 4c8e9e5a-f505-41f1-963f-8cc13d8088ab */
   public java.lang.String getPhone() {
      return phone;
   }
   
   /** @param newPhone
    * @pdOid ab57ab94-6c1b-435b-a94b-a4a74cee4764 */
   public void setPhone(java.lang.String newPhone) {
      phone = newPhone;
   }
   
   /** @pdOid 92fcc47e-06eb-42df-b746-7a4d0648384a */
   public java.lang.String getQq() {
      return qq;
   }
   
   /** @param newQq
    * @pdOid 37d06c01-f34a-4641-b5fc-a96612779434 */
   public void setQq(java.lang.String newQq) {
      qq = newQq;
   }
   
   /** @pdOid 6325a7c2-1561-4530-97e2-7c7228541997 */
   public java.lang.String getMobile() {
      return mobile;
   }
   
   /** @param newMobile
    * @pdOid abd1a24e-cba4-474b-aa23-f4e2aeb61785 */
   public void setMobile(java.lang.String newMobile) {
      mobile = newMobile;
   }
   
   /** @pdOid ebb8d6cf-16b9-4396-99a0-2bb1e2c0a468 */
   public java.lang.String getMail() {
      return mail;
   }
   
   /** @param newMail
    * @pdOid 43d0db10-7ec5-434c-aa0a-5547003ed877 */
   public void setMail(java.lang.String newMail) {
      mail = newMail;
   }
   
   /** @pdOid 7a5264a5-6cec-49fd-8c90-c7bd4a2db452 */
   public java.lang.String getAddr() {
      return addr;
   }
   
   /** @param newAddr
    * @pdOid 5faadbd2-4eb9-4dde-91af-4908ea713773 */
   public void setAddr(java.lang.String newAddr) {
      addr = newAddr;
   }
   
   /** @pdOid 72507ed2-c321-4dcf-9411-1e866f814c3a */
   public java.lang.String getNote() {
      return note;
   }
   
   /** @param newNote
    * @pdOid eaf4d829-508c-482a-ae6f-feb0c9f4f794 */
   public void setNote(java.lang.String newNote) {
      note = newNote;
   }
   
   
   /** @pdGenerated default getter */
   public java.util.Collection<AmsActiveinfos> getAmsActiveinfos() {
      if (amsActiveinfos == null)
         amsActiveinfos = new java.util.HashSet<AmsActiveinfos>();
      return amsActiveinfos;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorAmsActiveinfos() {
      if (amsActiveinfos == null)
         amsActiveinfos = new java.util.HashSet<AmsActiveinfos>();
      return amsActiveinfos.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newAmsActiveinfos */
   public void setAmsActiveinfos(java.util.Collection<AmsActiveinfos> newAmsActiveinfos) {
      removeAllAmsActiveinfos();
      for (java.util.Iterator iter = newAmsActiveinfos.iterator(); iter.hasNext();)
         addAmsActiveinfos((AmsActiveinfos)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newAmsActiveinfos */
   public void addAmsActiveinfos(AmsActiveinfos newAmsActiveinfos) {
      if (newAmsActiveinfos == null)
         return;
      if (this.amsActiveinfos == null)
         this.amsActiveinfos = new java.util.HashSet<AmsActiveinfos>();
      if (!this.amsActiveinfos.contains(newAmsActiveinfos))
      {
         this.amsActiveinfos.add(newAmsActiveinfos);
         newAmsActiveinfos.setUserinfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldAmsActiveinfos */
   public void removeAmsActiveinfos(AmsActiveinfos oldAmsActiveinfos) {
      if (oldAmsActiveinfos == null)
         return;
      if (this.amsActiveinfos != null)
         if (this.amsActiveinfos.contains(oldAmsActiveinfos))
         {
            this.amsActiveinfos.remove(oldAmsActiveinfos);
            oldAmsActiveinfos.setUserinfos((Userinfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllAmsActiveinfos() {
      if (amsActiveinfos != null)
      {
         AmsActiveinfos oldAmsActiveinfos;
         for (java.util.Iterator iter = getIteratorAmsActiveinfos(); iter.hasNext();)
         {
            oldAmsActiveinfos = (AmsActiveinfos)iter.next();
            iter.remove();
            oldAmsActiveinfos.setUserinfos((Userinfos)null);
         }
      }
   }
   /** @pdGenerated default getter */
   public java.util.Collection<AmsActiveDetail> getAmsActiveDetail() {
      if (amsActiveDetail == null)
         amsActiveDetail = new java.util.HashSet<AmsActiveDetail>();
      return amsActiveDetail;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorAmsActiveDetail() {
      if (amsActiveDetail == null)
         amsActiveDetail = new java.util.HashSet<AmsActiveDetail>();
      return amsActiveDetail.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newAmsActiveDetail */
   public void setAmsActiveDetail(java.util.Collection<AmsActiveDetail> newAmsActiveDetail) {
      removeAllAmsActiveDetail();
      for (java.util.Iterator iter = newAmsActiveDetail.iterator(); iter.hasNext();)
         addAmsActiveDetail((AmsActiveDetail)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newAmsActiveDetail */
   public void addAmsActiveDetail(AmsActiveDetail newAmsActiveDetail) {
      if (newAmsActiveDetail == null)
         return;
      if (this.amsActiveDetail == null)
         this.amsActiveDetail = new java.util.HashSet<AmsActiveDetail>();
      if (!this.amsActiveDetail.contains(newAmsActiveDetail))
      {
         this.amsActiveDetail.add(newAmsActiveDetail);
         newAmsActiveDetail.setUserinfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldAmsActiveDetail */
   public void removeAmsActiveDetail(AmsActiveDetail oldAmsActiveDetail) {
      if (oldAmsActiveDetail == null)
         return;
      if (this.amsActiveDetail != null)
         if (this.amsActiveDetail.contains(oldAmsActiveDetail))
         {
            this.amsActiveDetail.remove(oldAmsActiveDetail);
            oldAmsActiveDetail.setUserinfos((Userinfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllAmsActiveDetail() {
      if (amsActiveDetail != null)
      {
         AmsActiveDetail oldAmsActiveDetail;
         for (java.util.Iterator iter = getIteratorAmsActiveDetail(); iter.hasNext();)
         {
            oldAmsActiveDetail = (AmsActiveDetail)iter.next();
            iter.remove();
            oldAmsActiveDetail.setUserinfos((Userinfos)null);
         }
      }
   }
   /** @pdGenerated default getter */
   public java.util.Collection<AmsProtminfos> getAmsProtminfos() {
      if (amsProtminfos == null)
         amsProtminfos = new java.util.HashSet<AmsProtminfos>();
      return amsProtminfos;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorAmsProtminfos() {
      if (amsProtminfos == null)
         amsProtminfos = new java.util.HashSet<AmsProtminfos>();
      return amsProtminfos.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newAmsProtminfos */
   public void setAmsProtminfos(java.util.Collection<AmsProtminfos> newAmsProtminfos) {
      removeAllAmsProtminfos();
      for (java.util.Iterator iter = newAmsProtminfos.iterator(); iter.hasNext();)
         addAmsProtminfos((AmsProtminfos)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newAmsProtminfos */
   public void addAmsProtminfos(AmsProtminfos newAmsProtminfos) {
      if (newAmsProtminfos == null)
         return;
      if (this.amsProtminfos == null)
         this.amsProtminfos = new java.util.HashSet<AmsProtminfos>();
      if (!this.amsProtminfos.contains(newAmsProtminfos))
      {
         this.amsProtminfos.add(newAmsProtminfos);
         newAmsProtminfos.setUserinfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldAmsProtminfos */
   public void removeAmsProtminfos(AmsProtminfos oldAmsProtminfos) {
      if (oldAmsProtminfos == null)
         return;
      if (this.amsProtminfos != null)
         if (this.amsProtminfos.contains(oldAmsProtminfos))
         {
            this.amsProtminfos.remove(oldAmsProtminfos);
            oldAmsProtminfos.setUserinfos((Userinfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllAmsProtminfos() {
      if (amsProtminfos != null)
      {
         AmsProtminfos oldAmsProtminfos;
         for (java.util.Iterator iter = getIteratorAmsProtminfos(); iter.hasNext();)
         {
            oldAmsProtminfos = (AmsProtminfos)iter.next();
            iter.remove();
            oldAmsProtminfos.setUserinfos((Userinfos)null);
         }
      }
   }
   /** @pdGenerated default getter */
   public java.util.Collection<AmsProtmDetail> getAmsProtmDetail() {
      if (amsProtmDetail == null)
         amsProtmDetail = new java.util.HashSet<AmsProtmDetail>();
      return amsProtmDetail;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorAmsProtmDetail() {
      if (amsProtmDetail == null)
         amsProtmDetail = new java.util.HashSet<AmsProtmDetail>();
      return amsProtmDetail.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newAmsProtmDetail */
   public void setAmsProtmDetail(java.util.Collection<AmsProtmDetail> newAmsProtmDetail) {
      removeAllAmsProtmDetail();
      for (java.util.Iterator iter = newAmsProtmDetail.iterator(); iter.hasNext();)
         addAmsProtmDetail((AmsProtmDetail)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newAmsProtmDetail */
   public void addAmsProtmDetail(AmsProtmDetail newAmsProtmDetail) {
      if (newAmsProtmDetail == null)
         return;
      if (this.amsProtmDetail == null)
         this.amsProtmDetail = new java.util.HashSet<AmsProtmDetail>();
      if (!this.amsProtmDetail.contains(newAmsProtmDetail))
      {
         this.amsProtmDetail.add(newAmsProtmDetail);
         newAmsProtmDetail.setUserinfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldAmsProtmDetail */
   public void removeAmsProtmDetail(AmsProtmDetail oldAmsProtmDetail) {
      if (oldAmsProtmDetail == null)
         return;
      if (this.amsProtmDetail != null)
         if (this.amsProtmDetail.contains(oldAmsProtmDetail))
         {
            this.amsProtmDetail.remove(oldAmsProtmDetail);
            oldAmsProtmDetail.setUserinfos((Userinfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllAmsProtmDetail() {
      if (amsProtmDetail != null)
      {
         AmsProtmDetail oldAmsProtmDetail;
         for (java.util.Iterator iter = getIteratorAmsProtmDetail(); iter.hasNext();)
         {
            oldAmsProtmDetail = (AmsProtmDetail)iter.next();
            iter.remove();
            oldAmsProtmDetail.setUserinfos((Userinfos)null);
         }
      }
   }
   /** @pdGenerated default getter */
   public java.util.Collection<Buginfos> getBuginfos() {
      if (buginfos == null)
         buginfos = new java.util.HashSet<Buginfos>();
      return buginfos;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorBuginfos() {
      if (buginfos == null)
         buginfos = new java.util.HashSet<Buginfos>();
      return buginfos.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newBuginfos */
   public void setBuginfos(java.util.Collection<Buginfos> newBuginfos) {
      removeAllBuginfos();
      for (java.util.Iterator iter = newBuginfos.iterator(); iter.hasNext();)
         addBuginfos((Buginfos)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newBuginfos */
   public void addBuginfos(Buginfos newBuginfos) {
      if (newBuginfos == null)
         return;
      if (this.buginfos == null)
         this.buginfos = new java.util.HashSet<Buginfos>();
      if (!this.buginfos.contains(newBuginfos))
      {
         this.buginfos.add(newBuginfos);
         newBuginfos.setUserinfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldBuginfos */
   public void removeBuginfos(Buginfos oldBuginfos) {
      if (oldBuginfos == null)
         return;
      if (this.buginfos != null)
         if (this.buginfos.contains(oldBuginfos))
         {
            this.buginfos.remove(oldBuginfos);
            oldBuginfos.setUserinfos((Userinfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllBuginfos() {
      if (buginfos != null)
      {
         Buginfos oldBuginfos;
         for (java.util.Iterator iter = getIteratorBuginfos(); iter.hasNext();)
         {
            oldBuginfos = (Buginfos)iter.next();
            iter.remove();
            oldBuginfos.setUserinfos((Userinfos)null);
         }
      }
   }
   /** @pdGenerated default getter */
   public java.util.Collection<Buginfos> getBuginfos() {
      if (buginfos == null)
         buginfos = new java.util.HashSet<Buginfos>();
      return buginfos;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorBuginfos() {
      if (buginfos == null)
         buginfos = new java.util.HashSet<Buginfos>();
      return buginfos.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newBuginfos */
   public void setBuginfos(java.util.Collection<Buginfos> newBuginfos) {
      removeAllBuginfos();
      for (java.util.Iterator iter = newBuginfos.iterator(); iter.hasNext();)
         addBuginfos((Buginfos)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newBuginfos */
   public void addBuginfos(Buginfos newBuginfos) {
      if (newBuginfos == null)
         return;
      if (this.buginfos == null)
         this.buginfos = new java.util.HashSet<Buginfos>();
      if (!this.buginfos.contains(newBuginfos))
      {
         this.buginfos.add(newBuginfos);
         newBuginfos.setUserinfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldBuginfos */
   public void removeBuginfos(Buginfos oldBuginfos) {
      if (oldBuginfos == null)
         return;
      if (this.buginfos != null)
         if (this.buginfos.contains(oldBuginfos))
         {
            this.buginfos.remove(oldBuginfos);
            oldBuginfos.setUserinfos((Userinfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllBuginfos() {
      if (buginfos != null)
      {
         Buginfos oldBuginfos;
         for (java.util.Iterator iter = getIteratorBuginfos(); iter.hasNext();)
         {
            oldBuginfos = (Buginfos)iter.next();
            iter.remove();
            oldBuginfos.setUserinfos((Userinfos)null);
         }
      }
   }
   /** @pdGenerated default getter */
   public java.util.Collection<Taskinfos> getTaskinfos() {
      if (taskinfos == null)
         taskinfos = new java.util.HashSet<Taskinfos>();
      return taskinfos;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorTaskinfos() {
      if (taskinfos == null)
         taskinfos = new java.util.HashSet<Taskinfos>();
      return taskinfos.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newTaskinfos */
   public void setTaskinfos(java.util.Collection<Taskinfos> newTaskinfos) {
      removeAllTaskinfos();
      for (java.util.Iterator iter = newTaskinfos.iterator(); iter.hasNext();)
         addTaskinfos((Taskinfos)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newTaskinfos */
   public void addTaskinfos(Taskinfos newTaskinfos) {
      if (newTaskinfos == null)
         return;
      if (this.taskinfos == null)
         this.taskinfos = new java.util.HashSet<Taskinfos>();
      if (!this.taskinfos.contains(newTaskinfos))
      {
         this.taskinfos.add(newTaskinfos);
         newTaskinfos.setUserinfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldTaskinfos */
   public void removeTaskinfos(Taskinfos oldTaskinfos) {
      if (oldTaskinfos == null)
         return;
      if (this.taskinfos != null)
         if (this.taskinfos.contains(oldTaskinfos))
         {
            this.taskinfos.remove(oldTaskinfos);
            oldTaskinfos.setUserinfos((Userinfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllTaskinfos() {
      if (taskinfos != null)
      {
         Taskinfos oldTaskinfos;
         for (java.util.Iterator iter = getIteratorTaskinfos(); iter.hasNext();)
         {
            oldTaskinfos = (Taskinfos)iter.next();
            iter.remove();
            oldTaskinfos.setUserinfos((Userinfos)null);
         }
      }
   }
   /** @pdGenerated default getter */
   public java.util.Collection<Taskinfos> getTaskinfos() {
      if (taskinfos == null)
         taskinfos = new java.util.HashSet<Taskinfos>();
      return taskinfos;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorTaskinfos() {
      if (taskinfos == null)
         taskinfos = new java.util.HashSet<Taskinfos>();
      return taskinfos.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newTaskinfos */
   public void setTaskinfos(java.util.Collection<Taskinfos> newTaskinfos) {
      removeAllTaskinfos();
      for (java.util.Iterator iter = newTaskinfos.iterator(); iter.hasNext();)
         addTaskinfos((Taskinfos)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newTaskinfos */
   public void addTaskinfos(Taskinfos newTaskinfos) {
      if (newTaskinfos == null)
         return;
      if (this.taskinfos == null)
         this.taskinfos = new java.util.HashSet<Taskinfos>();
      if (!this.taskinfos.contains(newTaskinfos))
      {
         this.taskinfos.add(newTaskinfos);
         newTaskinfos.setUserinfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldTaskinfos */
   public void removeTaskinfos(Taskinfos oldTaskinfos) {
      if (oldTaskinfos == null)
         return;
      if (this.taskinfos != null)
         if (this.taskinfos.contains(oldTaskinfos))
         {
            this.taskinfos.remove(oldTaskinfos);
            oldTaskinfos.setUserinfos((Userinfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllTaskinfos() {
      if (taskinfos != null)
      {
         Taskinfos oldTaskinfos;
         for (java.util.Iterator iter = getIteratorTaskinfos(); iter.hasNext();)
         {
            oldTaskinfos = (Taskinfos)iter.next();
            iter.remove();
            oldTaskinfos.setUserinfos((Userinfos)null);
         }
      }
   }
   /** @pdGenerated default getter */
   public java.util.Collection<AmsMsginfos> getAmsMsginfos() {
      if (amsMsginfos == null)
         amsMsginfos = new java.util.HashSet<AmsMsginfos>();
      return amsMsginfos;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorAmsMsginfos() {
      if (amsMsginfos == null)
         amsMsginfos = new java.util.HashSet<AmsMsginfos>();
      return amsMsginfos.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newAmsMsginfos */
   public void setAmsMsginfos(java.util.Collection<AmsMsginfos> newAmsMsginfos) {
      removeAllAmsMsginfos();
      for (java.util.Iterator iter = newAmsMsginfos.iterator(); iter.hasNext();)
         addAmsMsginfos((AmsMsginfos)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newAmsMsginfos */
   public void addAmsMsginfos(AmsMsginfos newAmsMsginfos) {
      if (newAmsMsginfos == null)
         return;
      if (this.amsMsginfos == null)
         this.amsMsginfos = new java.util.HashSet<AmsMsginfos>();
      if (!this.amsMsginfos.contains(newAmsMsginfos))
      {
         this.amsMsginfos.add(newAmsMsginfos);
         newAmsMsginfos.setUserinfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldAmsMsginfos */
   public void removeAmsMsginfos(AmsMsginfos oldAmsMsginfos) {
      if (oldAmsMsginfos == null)
         return;
      if (this.amsMsginfos != null)
         if (this.amsMsginfos.contains(oldAmsMsginfos))
         {
            this.amsMsginfos.remove(oldAmsMsginfos);
            oldAmsMsginfos.setUserinfos((Userinfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllAmsMsginfos() {
      if (amsMsginfos != null)
      {
         AmsMsginfos oldAmsMsginfos;
         for (java.util.Iterator iter = getIteratorAmsMsginfos(); iter.hasNext();)
         {
            oldAmsMsginfos = (AmsMsginfos)iter.next();
            iter.remove();
            oldAmsMsginfos.setUserinfos((Userinfos)null);
         }
      }
   }

}