/***********************************************************************
 * Module:  AmsResInfos.java
 * Author:  Administrator
 * Purpose: Defines the Class AmsResInfos
 ***********************************************************************/

import java.util.*;

/** 系统资源信息
 * 
 * @pdOid cb87cd0c-c7b6-49ad-a53a-d7dc87860893 */
public class AmsResInfos {
   /** @pdOid 15971ce6-90d7-448b-947e-01e76a900ec8 */
   public int reid;
   /** @pdOid 8266f924-b788-4120-b8ac-1a9bc63c3f45 */
   public java.lang.String rename;
   /** @pdOid a87dacdf-4368-4ae1-af5b-bba796097b2e */
   public java.lang.String restatus;
   /** @pdOid cf320d62-6bcf-4f13-87a0-bb7ec84b5c78 */
   public java.lang.String resurl;
   /** @pdOid ca1e4f46-f283-4df9-9647-0732126db438 */
   public java.lang.String chksum;
   /** @pdOid 48ea6b0b-0104-4911-b9c5-d1519e117328 */
   public java.lang.String note;
   
   /** @pdRoleInfo migr=no name=AmsAcsinfos assc=roleResAccess coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<AmsAcsinfos> amsAcsinfos;
   /** @pdRoleInfo migr=no name=AmsResTypeInfos assc=resourceType mult=0..1 side=A */
   public AmsResTypeInfos amsResTypeInfos;
   
   /** @pdOid 4ea67fb5-8d3c-487f-8327-74c1919f22fe */
   public int getReid() {
      return reid;
   }
   
   /** @param newReid
    * @pdOid 6364aa2c-aa0d-4727-aa05-11ba43229d31 */
   public void setReid(int newReid) {
      reid = newReid;
   }
   
   /** @pdOid c171e1c5-5eab-4126-aa7c-842a5c9d5483 */
   public java.lang.String getRename() {
      return rename;
   }
   
   /** @param newRename
    * @pdOid a63e57c7-a412-481a-a1c5-8c03158e77c3 */
   public void setRename(java.lang.String newRename) {
      rename = newRename;
   }
   
   /** @pdOid ccd8e18a-05c6-4ca2-a439-b504b9d9a44d */
   public java.lang.String getRestatus() {
      return restatus;
   }
   
   /** @param newRestatus
    * @pdOid 0408a38e-e4fd-46d2-9e77-78bd15db2aec */
   public void setRestatus(java.lang.String newRestatus) {
      restatus = newRestatus;
   }
   
   /** @pdOid d31bb740-7736-4a5c-ba8f-a6ac251020d3 */
   public java.lang.String getResurl() {
      return resurl;
   }
   
   /** @param newResurl
    * @pdOid 355bade4-8b9f-4cb8-834f-aaa7e92b7320 */
   public void setResurl(java.lang.String newResurl) {
      resurl = newResurl;
   }
   
   /** @pdOid d98b4287-74ac-45ba-a665-2e260d3ef5d4 */
   public java.lang.String getChksum() {
      return chksum;
   }
   
   /** @param newChksum
    * @pdOid 7f13c88e-cd92-4a1f-9cb4-25022cf06fa9 */
   public void setChksum(java.lang.String newChksum) {
      chksum = newChksum;
   }
   
   /** @pdOid 912fd059-fb7f-4bb0-a313-d709d17439e9 */
   public java.lang.String getNote() {
      return note;
   }
   
   /** @param newNote
    * @pdOid e8e1bc42-7881-4fac-8969-b6d702134f40 */
   public void setNote(java.lang.String newNote) {
      note = newNote;
   }
   
   
   /** @pdGenerated default getter */
   public java.util.Collection<AmsAcsinfos> getAmsAcsinfos() {
      if (amsAcsinfos == null)
         amsAcsinfos = new java.util.HashSet<AmsAcsinfos>();
      return amsAcsinfos;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorAmsAcsinfos() {
      if (amsAcsinfos == null)
         amsAcsinfos = new java.util.HashSet<AmsAcsinfos>();
      return amsAcsinfos.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newAmsAcsinfos */
   public void setAmsAcsinfos(java.util.Collection<AmsAcsinfos> newAmsAcsinfos) {
      removeAllAmsAcsinfos();
      for (java.util.Iterator iter = newAmsAcsinfos.iterator(); iter.hasNext();)
         addAmsAcsinfos((AmsAcsinfos)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newAmsAcsinfos */
   public void addAmsAcsinfos(AmsAcsinfos newAmsAcsinfos) {
      if (newAmsAcsinfos == null)
         return;
      if (this.amsAcsinfos == null)
         this.amsAcsinfos = new java.util.HashSet<AmsAcsinfos>();
      if (!this.amsAcsinfos.contains(newAmsAcsinfos))
      {
         this.amsAcsinfos.add(newAmsAcsinfos);
         newAmsAcsinfos.setAmsResInfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldAmsAcsinfos */
   public void removeAmsAcsinfos(AmsAcsinfos oldAmsAcsinfos) {
      if (oldAmsAcsinfos == null)
         return;
      if (this.amsAcsinfos != null)
         if (this.amsAcsinfos.contains(oldAmsAcsinfos))
         {
            this.amsAcsinfos.remove(oldAmsAcsinfos);
            oldAmsAcsinfos.setAmsResInfos((AmsResInfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllAmsAcsinfos() {
      if (amsAcsinfos != null)
      {
         AmsAcsinfos oldAmsAcsinfos;
         for (java.util.Iterator iter = getIteratorAmsAcsinfos(); iter.hasNext();)
         {
            oldAmsAcsinfos = (AmsAcsinfos)iter.next();
            iter.remove();
            oldAmsAcsinfos.setAmsResInfos((AmsResInfos)null);
         }
      }
   }
   /** @pdGenerated default parent getter */
   public AmsResTypeInfos getAmsResTypeInfos() {
      return amsResTypeInfos;
   }
   
   /** @pdGenerated default parent setter
     * @param newAmsResTypeInfos */
   public void setAmsResTypeInfos(AmsResTypeInfos newAmsResTypeInfos) {
      if (this.amsResTypeInfos == null || !this.amsResTypeInfos.equals(newAmsResTypeInfos))
      {
         if (this.amsResTypeInfos != null)
         {
            AmsResTypeInfos oldAmsResTypeInfos = this.amsResTypeInfos;
            this.amsResTypeInfos = null;
            oldAmsResTypeInfos.removeAmsResInfos(this);
         }
         if (newAmsResTypeInfos != null)
         {
            this.amsResTypeInfos = newAmsResTypeInfos;
            this.amsResTypeInfos.addAmsResInfos(this);
         }
      }
   }

}