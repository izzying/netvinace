/***********************************************************************
 * Module:  AmsAcsinfos.java
 * Author:  Administrator
 * Purpose: Defines the Class AmsAcsinfos
 ***********************************************************************/

import java.util.*;

/** 系统权限信息
 * 
 * @pdOid 23af4ab5-8184-4a1f-b961-f9678b931be9 */
public class AmsAcsinfos {
   /** @pdOid 56b094f7-0ebc-4227-b0a8-eb39f0e47d12 */
   public int acsid;
   /** @pdOid e8a98ee6-f4a3-419a-9d11-8aafa9efbe2d */
   public java.lang.String ctrl;
   
   /** @pdRoleInfo migr=no name=AmsRoleinfos assc=roleAccess mult=0..1 side=A */
   public AmsRoleinfos amsRoleinfos;
   /** @pdRoleInfo migr=no name=AmsResInfos assc=roleResAccess mult=0..1 side=A */
   public AmsResInfos amsResInfos;
   /** @pdRoleInfo migr=no name=AmsResTypeInfos assc=resourceTypeInfor mult=0..1 side=A */
   public AmsResTypeInfos amsResTypeInfos;
   
   /** @pdOid 983f89cd-7db6-4138-848f-a4a865cd48a0 */
   public int getAcsid() {
      return acsid;
   }
   
   /** @param newAcsid
    * @pdOid 8df2b540-7469-4cd1-a979-2c415729437c */
   public void setAcsid(int newAcsid) {
      acsid = newAcsid;
   }
   
   /** @pdOid 1175608e-7535-4589-bf8f-05bb63ab49bf */
   public java.lang.String getCtrl() {
      return ctrl;
   }
   
   /** @param newCtrl
    * @pdOid 6974443f-2ecf-475f-ad79-6cdd36d83b13 */
   public void setCtrl(java.lang.String newCtrl) {
      ctrl = newCtrl;
   }
   
   
   /** @pdGenerated default parent getter */
   public AmsRoleinfos getAmsRoleinfos() {
      return amsRoleinfos;
   }
   
   /** @pdGenerated default parent setter
     * @param newAmsRoleinfos */
   public void setAmsRoleinfos(AmsRoleinfos newAmsRoleinfos) {
      if (this.amsRoleinfos == null || !this.amsRoleinfos.equals(newAmsRoleinfos))
      {
         if (this.amsRoleinfos != null)
         {
            AmsRoleinfos oldAmsRoleinfos = this.amsRoleinfos;
            this.amsRoleinfos = null;
            oldAmsRoleinfos.removeAmsAcsinfos(this);
         }
         if (newAmsRoleinfos != null)
         {
            this.amsRoleinfos = newAmsRoleinfos;
            this.amsRoleinfos.addAmsAcsinfos(this);
         }
      }
   }
   /** @pdGenerated default parent getter */
   public AmsResInfos getAmsResInfos() {
      return amsResInfos;
   }
   
   /** @pdGenerated default parent setter
     * @param newAmsResInfos */
   public void setAmsResInfos(AmsResInfos newAmsResInfos) {
      if (this.amsResInfos == null || !this.amsResInfos.equals(newAmsResInfos))
      {
         if (this.amsResInfos != null)
         {
            AmsResInfos oldAmsResInfos = this.amsResInfos;
            this.amsResInfos = null;
            oldAmsResInfos.removeAmsAcsinfos(this);
         }
         if (newAmsResInfos != null)
         {
            this.amsResInfos = newAmsResInfos;
            this.amsResInfos.addAmsAcsinfos(this);
         }
      }
   }
   /** @pdGenerated default parent getter */
   public AmsResTypeInfos getAmsResTypeInfos() {
      return amsResTypeInfos;
   }
   
   /** @pdGenerated default parent setter
     * @param newAmsResTypeInfos */
   public void setAmsResTypeInfos(AmsResTypeInfos newAmsResTypeInfos) {
      if (this.amsResTypeInfos == null || !this.amsResTypeInfos.equals(newAmsResTypeInfos))
      {
         if (this.amsResTypeInfos != null)
         {
            AmsResTypeInfos oldAmsResTypeInfos = this.amsResTypeInfos;
            this.amsResTypeInfos = null;
            oldAmsResTypeInfos.removeAmsAcsinfos(this);
         }
         if (newAmsResTypeInfos != null)
         {
            this.amsResTypeInfos = newAmsResTypeInfos;
            this.amsResTypeInfos.addAmsAcsinfos(this);
         }
      }
   }

}