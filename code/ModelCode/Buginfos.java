/***********************************************************************
 * Module:  Buginfos.java
 * Author:  Administrator
 * Purpose: Defines the Class Buginfos
 ***********************************************************************/

import java.util.*;

/** 项目bug跟踪信息
 * 
 * @pdOid 013cac3e-1f52-4925-8336-1f0e14dfd6d0 */
public class Buginfos {
   /** @pdOid 92c3936c-0265-429d-bf5e-35056e0b87e0 */
   public int serid;
   /** @pdOid a52ed061-715d-4482-a3b5-ed0852ff0fbb */
   public java.lang.String attach;
   /** @pdOid 1ed8201f-1b35-4476-9ee9-cfaa2683cad7 */
   public java.util.Date commitTime;
   /** @pdOid 0c5e5b15-42d1-4ef6-9a41-61523cf58d8e */
   public java.util.Date lastTime;
   /** @pdOid 2e16c9a2-7c4a-4e07-85e5-a4ea18cdaa4d */
   public java.lang.String chksum;
   /** @pdOid 636c79c1-2c47-46bb-b136-24587c2654b2 */
   public java.lang.String bugNote;
   
   /** @pdRoleInfo migr=no name=AmsProjectinfos assc=projectInfosRef mult=0..1 side=A */
   public AmsProjectinfos amsProjectinfos;
   /** @pdRoleInfo migr=no name=AmsProtminfos assc=projectTeamInfosRef mult=0..1 side=A */
   public AmsProtminfos amsProtminfos;
   /** @pdRoleInfo migr=no name=Userinfos assc=uploaderInfosRef mult=0..1 side=A */
   public Userinfos userinfos;
   /** @pdRoleInfo migr=no name=Userinfos assc=handelInfosRef mult=0..1 side=A */
   public Userinfos userinfos;
   
   /** @pdOid fab045a0-d5b3-4883-b258-23b1b4235f3c */
   public int getSerid() {
      return serid;
   }
   
   /** @param newSerid
    * @pdOid d5b18f74-d586-48c4-9b82-4518e487e447 */
   public void setSerid(int newSerid) {
      serid = newSerid;
   }
   
   /** @pdOid a07171bc-a56d-4c8c-802f-0b21d2a3e521 */
   public java.lang.String getAttach() {
      return attach;
   }
   
   /** @param newAttach
    * @pdOid 3bb1a745-5aee-4cf0-82a9-8b0b91140467 */
   public void setAttach(java.lang.String newAttach) {
      attach = newAttach;
   }
   
   /** @pdOid 0375eec8-0766-4d96-87b9-87355b7605cf */
   public java.util.Date getCommitTime() {
      return commitTime;
   }
   
   /** @param newCommitTime
    * @pdOid e45f2a92-4c2a-4494-9d37-f79c39932756 */
   public void setCommitTime(java.util.Date newCommitTime) {
      commitTime = newCommitTime;
   }
   
   /** @pdOid 0f89e450-afbb-40d2-a0e4-454263f4dcc5 */
   public java.util.Date getLastTime() {
      return lastTime;
   }
   
   /** @param newLastTime
    * @pdOid bf8b12ce-3379-450d-aa45-5b56d1863d83 */
   public void setLastTime(java.util.Date newLastTime) {
      lastTime = newLastTime;
   }
   
   /** @pdOid b7da56d3-a602-44b8-8fa5-361c3fd72860 */
   public java.lang.String getChksum() {
      return chksum;
   }
   
   /** @param newChksum
    * @pdOid 72196403-1914-4100-aa0f-cff4dd69097c */
   public void setChksum(java.lang.String newChksum) {
      chksum = newChksum;
   }
   
   /** @pdOid 572123d0-5e40-44f0-aef7-fb719be8e26c */
   public java.lang.String getBugNote() {
      return bugNote;
   }
   
   /** @param newBugNote
    * @pdOid 42772ecb-9146-4bd2-875c-5a35716e609c */
   public void setBugNote(java.lang.String newBugNote) {
      bugNote = newBugNote;
   }
   
   
   /** @pdGenerated default parent getter */
   public AmsProjectinfos getAmsProjectinfos() {
      return amsProjectinfos;
   }
   
   /** @pdGenerated default parent setter
     * @param newAmsProjectinfos */
   public void setAmsProjectinfos(AmsProjectinfos newAmsProjectinfos) {
      if (this.amsProjectinfos == null || !this.amsProjectinfos.equals(newAmsProjectinfos))
      {
         if (this.amsProjectinfos != null)
         {
            AmsProjectinfos oldAmsProjectinfos = this.amsProjectinfos;
            this.amsProjectinfos = null;
            oldAmsProjectinfos.removeBuginfos(this);
         }
         if (newAmsProjectinfos != null)
         {
            this.amsProjectinfos = newAmsProjectinfos;
            this.amsProjectinfos.addBuginfos(this);
         }
      }
   }
   /** @pdGenerated default parent getter */
   public AmsProtminfos getAmsProtminfos() {
      return amsProtminfos;
   }
   
   /** @pdGenerated default parent setter
     * @param newAmsProtminfos */
   public void setAmsProtminfos(AmsProtminfos newAmsProtminfos) {
      if (this.amsProtminfos == null || !this.amsProtminfos.equals(newAmsProtminfos))
      {
         if (this.amsProtminfos != null)
         {
            AmsProtminfos oldAmsProtminfos = this.amsProtminfos;
            this.amsProtminfos = null;
            oldAmsProtminfos.removeBuginfos(this);
         }
         if (newAmsProtminfos != null)
         {
            this.amsProtminfos = newAmsProtminfos;
            this.amsProtminfos.addBuginfos(this);
         }
      }
   }
   /** @pdGenerated default parent getter */
   public Userinfos getUserinfos() {
      return userinfos;
   }
   
   /** @pdGenerated default parent setter
     * @param newUserinfos */
   public void setUserinfos(Userinfos newUserinfos) {
      if (this.userinfos == null || !this.userinfos.equals(newUserinfos))
      {
         if (this.userinfos != null)
         {
            Userinfos oldUserinfos = this.userinfos;
            this.userinfos = null;
            oldUserinfos.removeBuginfos(this);
         }
         if (newUserinfos != null)
         {
            this.userinfos = newUserinfos;
            this.userinfos.addBuginfos(this);
         }
      }
   }
   /** @pdGenerated default parent getter */
   public Userinfos getUserinfos() {
      return userinfos;
   }
   
   /** @pdGenerated default parent setter
     * @param newUserinfos */
   public void setUserinfos(Userinfos newUserinfos) {
      if (this.userinfos == null || !this.userinfos.equals(newUserinfos))
      {
         if (this.userinfos != null)
         {
            Userinfos oldUserinfos = this.userinfos;
            this.userinfos = null;
            oldUserinfos.removeBuginfos(this);
         }
         if (newUserinfos != null)
         {
            this.userinfos = newUserinfos;
            this.userinfos.addBuginfos(this);
         }
      }
   }

}