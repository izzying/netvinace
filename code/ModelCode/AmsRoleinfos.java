/***********************************************************************
 * Module:  AmsRoleinfos.java
 * Author:  Administrator
 * Purpose: Defines the Class AmsRoleinfos
 ***********************************************************************/

import java.util.*;

/** 系统角色基本信息,包含系统角色和项目角色
 * 
 * @pdOid be4dc959-c982-4ea1-ae92-2c76cf39c0a3 */
public class AmsRoleinfos {
   /** @pdOid b5126154-1163-49b6-a7d1-bfc896635c8e */
   public int rid;
   /** @pdOid 810669b4-7805-43c4-8992-6c0427bd0d99 */
   public java.lang.String rname;
   /** @pdOid e0f5b144-4d53-4ef3-81e4-b07f66a28322 */
   public java.lang.String rtype;
   /** @pdOid 91dfdc84-3917-4251-bcc1-fe95916bd0ff */
   public java.lang.String rstatus;
   /** @pdOid 0077ea7e-9d53-45e3-bd9b-051b9572be49 */
   public java.lang.String note;
   
   /** @pdRoleInfo migr=no name=AmsAcsinfos assc=roleAccess coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<AmsAcsinfos> amsAcsinfos;
   /** @pdRoleInfo migr=no name=AmsProtmDetail assc=projectMemberRole coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<AmsProtmDetail> amsProtmDetail;
   
   /** @pdOid e2e24e6c-7b52-4c3e-aad3-93ec304c7151 */
   public int getRid() {
      return rid;
   }
   
   /** @param newRid
    * @pdOid 3e8decd7-9469-46fb-a220-111f6d5bffaf */
   public void setRid(int newRid) {
      rid = newRid;
   }
   
   /** @pdOid d7668460-0054-43db-bb8f-c4a2cadd96aa */
   public java.lang.String getRname() {
      return rname;
   }
   
   /** @param newRname
    * @pdOid 31d3b678-12e9-44bc-b615-08894beba3e8 */
   public void setRname(java.lang.String newRname) {
      rname = newRname;
   }
   
   /** @pdOid 7d9a305e-85fe-43bc-9b15-59aa93a958dc */
   public java.lang.String getRtype() {
      return rtype;
   }
   
   /** @param newRtype
    * @pdOid 791b4594-6d62-4025-b833-78dcf35e4158 */
   public void setRtype(java.lang.String newRtype) {
      rtype = newRtype;
   }
   
   /** @pdOid 8a988d97-3ec0-4e64-935d-e4bd67302812 */
   public java.lang.String getRstatus() {
      return rstatus;
   }
   
   /** @param newRstatus
    * @pdOid f4785956-b4d3-482a-81dd-a6c7c32a83c3 */
   public void setRstatus(java.lang.String newRstatus) {
      rstatus = newRstatus;
   }
   
   /** @pdOid de53bd0b-268d-43d4-a140-a4b3348c53f3 */
   public java.lang.String getNote() {
      return note;
   }
   
   /** @param newNote
    * @pdOid b3ca030c-6b73-474e-9bcb-9a4a34b953a0 */
   public void setNote(java.lang.String newNote) {
      note = newNote;
   }
   
   
   /** @pdGenerated default getter */
   public java.util.Collection<AmsAcsinfos> getAmsAcsinfos() {
      if (amsAcsinfos == null)
         amsAcsinfos = new java.util.HashSet<AmsAcsinfos>();
      return amsAcsinfos;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorAmsAcsinfos() {
      if (amsAcsinfos == null)
         amsAcsinfos = new java.util.HashSet<AmsAcsinfos>();
      return amsAcsinfos.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newAmsAcsinfos */
   public void setAmsAcsinfos(java.util.Collection<AmsAcsinfos> newAmsAcsinfos) {
      removeAllAmsAcsinfos();
      for (java.util.Iterator iter = newAmsAcsinfos.iterator(); iter.hasNext();)
         addAmsAcsinfos((AmsAcsinfos)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newAmsAcsinfos */
   public void addAmsAcsinfos(AmsAcsinfos newAmsAcsinfos) {
      if (newAmsAcsinfos == null)
         return;
      if (this.amsAcsinfos == null)
         this.amsAcsinfos = new java.util.HashSet<AmsAcsinfos>();
      if (!this.amsAcsinfos.contains(newAmsAcsinfos))
      {
         this.amsAcsinfos.add(newAmsAcsinfos);
         newAmsAcsinfos.setAmsRoleinfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldAmsAcsinfos */
   public void removeAmsAcsinfos(AmsAcsinfos oldAmsAcsinfos) {
      if (oldAmsAcsinfos == null)
         return;
      if (this.amsAcsinfos != null)
         if (this.amsAcsinfos.contains(oldAmsAcsinfos))
         {
            this.amsAcsinfos.remove(oldAmsAcsinfos);
            oldAmsAcsinfos.setAmsRoleinfos((AmsRoleinfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllAmsAcsinfos() {
      if (amsAcsinfos != null)
      {
         AmsAcsinfos oldAmsAcsinfos;
         for (java.util.Iterator iter = getIteratorAmsAcsinfos(); iter.hasNext();)
         {
            oldAmsAcsinfos = (AmsAcsinfos)iter.next();
            iter.remove();
            oldAmsAcsinfos.setAmsRoleinfos((AmsRoleinfos)null);
         }
      }
   }
   /** @pdGenerated default getter */
   public java.util.Collection<AmsProtmDetail> getAmsProtmDetail() {
      if (amsProtmDetail == null)
         amsProtmDetail = new java.util.HashSet<AmsProtmDetail>();
      return amsProtmDetail;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorAmsProtmDetail() {
      if (amsProtmDetail == null)
         amsProtmDetail = new java.util.HashSet<AmsProtmDetail>();
      return amsProtmDetail.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newAmsProtmDetail */
   public void setAmsProtmDetail(java.util.Collection<AmsProtmDetail> newAmsProtmDetail) {
      removeAllAmsProtmDetail();
      for (java.util.Iterator iter = newAmsProtmDetail.iterator(); iter.hasNext();)
         addAmsProtmDetail((AmsProtmDetail)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newAmsProtmDetail */
   public void addAmsProtmDetail(AmsProtmDetail newAmsProtmDetail) {
      if (newAmsProtmDetail == null)
         return;
      if (this.amsProtmDetail == null)
         this.amsProtmDetail = new java.util.HashSet<AmsProtmDetail>();
      if (!this.amsProtmDetail.contains(newAmsProtmDetail))
      {
         this.amsProtmDetail.add(newAmsProtmDetail);
         newAmsProtmDetail.setAmsRoleinfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldAmsProtmDetail */
   public void removeAmsProtmDetail(AmsProtmDetail oldAmsProtmDetail) {
      if (oldAmsProtmDetail == null)
         return;
      if (this.amsProtmDetail != null)
         if (this.amsProtmDetail.contains(oldAmsProtmDetail))
         {
            this.amsProtmDetail.remove(oldAmsProtmDetail);
            oldAmsProtmDetail.setAmsRoleinfos((AmsRoleinfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllAmsProtmDetail() {
      if (amsProtmDetail != null)
      {
         AmsProtmDetail oldAmsProtmDetail;
         for (java.util.Iterator iter = getIteratorAmsProtmDetail(); iter.hasNext();)
         {
            oldAmsProtmDetail = (AmsProtmDetail)iter.next();
            iter.remove();
            oldAmsProtmDetail.setAmsRoleinfos((AmsRoleinfos)null);
         }
      }
   }

}