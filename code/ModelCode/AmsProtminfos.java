/***********************************************************************
 * Module:  AmsProtminfos.java
 * Author:  Administrator
 * Purpose: Defines the Class AmsProtminfos
 ***********************************************************************/

import java.util.*;

/** 项目组信息
 * 
 * @pdOid 47d4f1f7-2f85-4ce4-837a-70d9347fdc01 */
public class AmsProtminfos {
   /** @pdOid 82e34b3c-9faf-4753-bc04-c946d822ae43 */
   public int tmid;
   /** @pdOid e27aa763-66ad-4853-a88b-6e446d6e50fb */
   public java.lang.String tmname;
   /** @pdOid bdf9c405-3430-4275-99c2-8a36403c5337 */
   public java.lang.String tmnote;
   
   /** @pdRoleInfo migr=no name=AmsProtmDetail assc=projectTeamMembers coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<AmsProtmDetail> amsProtmDetail;
   /** @pdRoleInfo migr=no name=Buginfos assc=projectTeamInfosRef coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<Buginfos> buginfos;
   /** @pdRoleInfo migr=no name=Taskinfos assc=taskProjectTeamInfors coll=java.util.Collection impl=java.util.HashSet mult=0..* */
   public java.util.Collection<Taskinfos> taskinfos;
   /** @pdRoleInfo migr=no name=AmsProjectinfos assc=projectTeamInfos mult=0..1 side=A */
   public AmsProjectinfos amsProjectinfos;
   /** @pdRoleInfo migr=no name=Userinfos assc=projectTeamLeader mult=0..1 side=A */
   public Userinfos userinfos;
   
   /** @pdOid ba41ef9e-5f0b-4f1b-bd2f-ac1cd0194d76 */
   public int getTmid() {
      return tmid;
   }
   
   /** @param newTmid
    * @pdOid bdd86ed3-c172-4db6-9ba7-5fa412125e29 */
   public void setTmid(int newTmid) {
      tmid = newTmid;
   }
   
   /** @pdOid b589d098-d2ee-4d59-ac98-b9d15eb69a88 */
   public java.lang.String getTmname() {
      return tmname;
   }
   
   /** @param newTmname
    * @pdOid 4d8e05c4-4a98-44f4-aece-9015080cff45 */
   public void setTmname(java.lang.String newTmname) {
      tmname = newTmname;
   }
   
   /** @pdOid e39bd1ec-42e9-464b-b4ba-814925a93d46 */
   public java.lang.String getTmnote() {
      return tmnote;
   }
   
   /** @param newTmnote
    * @pdOid 4153a5d0-00a0-4f6e-86d5-074a0772d8ef */
   public void setTmnote(java.lang.String newTmnote) {
      tmnote = newTmnote;
   }
   
   
   /** @pdGenerated default getter */
   public java.util.Collection<AmsProtmDetail> getAmsProtmDetail() {
      if (amsProtmDetail == null)
         amsProtmDetail = new java.util.HashSet<AmsProtmDetail>();
      return amsProtmDetail;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorAmsProtmDetail() {
      if (amsProtmDetail == null)
         amsProtmDetail = new java.util.HashSet<AmsProtmDetail>();
      return amsProtmDetail.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newAmsProtmDetail */
   public void setAmsProtmDetail(java.util.Collection<AmsProtmDetail> newAmsProtmDetail) {
      removeAllAmsProtmDetail();
      for (java.util.Iterator iter = newAmsProtmDetail.iterator(); iter.hasNext();)
         addAmsProtmDetail((AmsProtmDetail)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newAmsProtmDetail */
   public void addAmsProtmDetail(AmsProtmDetail newAmsProtmDetail) {
      if (newAmsProtmDetail == null)
         return;
      if (this.amsProtmDetail == null)
         this.amsProtmDetail = new java.util.HashSet<AmsProtmDetail>();
      if (!this.amsProtmDetail.contains(newAmsProtmDetail))
      {
         this.amsProtmDetail.add(newAmsProtmDetail);
         newAmsProtmDetail.setAmsProtminfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldAmsProtmDetail */
   public void removeAmsProtmDetail(AmsProtmDetail oldAmsProtmDetail) {
      if (oldAmsProtmDetail == null)
         return;
      if (this.amsProtmDetail != null)
         if (this.amsProtmDetail.contains(oldAmsProtmDetail))
         {
            this.amsProtmDetail.remove(oldAmsProtmDetail);
            oldAmsProtmDetail.setAmsProtminfos((AmsProtminfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllAmsProtmDetail() {
      if (amsProtmDetail != null)
      {
         AmsProtmDetail oldAmsProtmDetail;
         for (java.util.Iterator iter = getIteratorAmsProtmDetail(); iter.hasNext();)
         {
            oldAmsProtmDetail = (AmsProtmDetail)iter.next();
            iter.remove();
            oldAmsProtmDetail.setAmsProtminfos((AmsProtminfos)null);
         }
      }
   }
   /** @pdGenerated default getter */
   public java.util.Collection<Buginfos> getBuginfos() {
      if (buginfos == null)
         buginfos = new java.util.HashSet<Buginfos>();
      return buginfos;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorBuginfos() {
      if (buginfos == null)
         buginfos = new java.util.HashSet<Buginfos>();
      return buginfos.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newBuginfos */
   public void setBuginfos(java.util.Collection<Buginfos> newBuginfos) {
      removeAllBuginfos();
      for (java.util.Iterator iter = newBuginfos.iterator(); iter.hasNext();)
         addBuginfos((Buginfos)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newBuginfos */
   public void addBuginfos(Buginfos newBuginfos) {
      if (newBuginfos == null)
         return;
      if (this.buginfos == null)
         this.buginfos = new java.util.HashSet<Buginfos>();
      if (!this.buginfos.contains(newBuginfos))
      {
         this.buginfos.add(newBuginfos);
         newBuginfos.setAmsProtminfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldBuginfos */
   public void removeBuginfos(Buginfos oldBuginfos) {
      if (oldBuginfos == null)
         return;
      if (this.buginfos != null)
         if (this.buginfos.contains(oldBuginfos))
         {
            this.buginfos.remove(oldBuginfos);
            oldBuginfos.setAmsProtminfos((AmsProtminfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllBuginfos() {
      if (buginfos != null)
      {
         Buginfos oldBuginfos;
         for (java.util.Iterator iter = getIteratorBuginfos(); iter.hasNext();)
         {
            oldBuginfos = (Buginfos)iter.next();
            iter.remove();
            oldBuginfos.setAmsProtminfos((AmsProtminfos)null);
         }
      }
   }
   /** @pdGenerated default getter */
   public java.util.Collection<Taskinfos> getTaskinfos() {
      if (taskinfos == null)
         taskinfos = new java.util.HashSet<Taskinfos>();
      return taskinfos;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorTaskinfos() {
      if (taskinfos == null)
         taskinfos = new java.util.HashSet<Taskinfos>();
      return taskinfos.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newTaskinfos */
   public void setTaskinfos(java.util.Collection<Taskinfos> newTaskinfos) {
      removeAllTaskinfos();
      for (java.util.Iterator iter = newTaskinfos.iterator(); iter.hasNext();)
         addTaskinfos((Taskinfos)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newTaskinfos */
   public void addTaskinfos(Taskinfos newTaskinfos) {
      if (newTaskinfos == null)
         return;
      if (this.taskinfos == null)
         this.taskinfos = new java.util.HashSet<Taskinfos>();
      if (!this.taskinfos.contains(newTaskinfos))
      {
         this.taskinfos.add(newTaskinfos);
         newTaskinfos.setAmsProtminfos(this);      
      }
   }
   
   /** @pdGenerated default remove
     * @param oldTaskinfos */
   public void removeTaskinfos(Taskinfos oldTaskinfos) {
      if (oldTaskinfos == null)
         return;
      if (this.taskinfos != null)
         if (this.taskinfos.contains(oldTaskinfos))
         {
            this.taskinfos.remove(oldTaskinfos);
            oldTaskinfos.setAmsProtminfos((AmsProtminfos)null);
         }
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllTaskinfos() {
      if (taskinfos != null)
      {
         Taskinfos oldTaskinfos;
         for (java.util.Iterator iter = getIteratorTaskinfos(); iter.hasNext();)
         {
            oldTaskinfos = (Taskinfos)iter.next();
            iter.remove();
            oldTaskinfos.setAmsProtminfos((AmsProtminfos)null);
         }
      }
   }
   /** @pdGenerated default parent getter */
   public AmsProjectinfos getAmsProjectinfos() {
      return amsProjectinfos;
   }
   
   /** @pdGenerated default parent setter
     * @param newAmsProjectinfos */
   public void setAmsProjectinfos(AmsProjectinfos newAmsProjectinfos) {
      if (this.amsProjectinfos == null || !this.amsProjectinfos.equals(newAmsProjectinfos))
      {
         if (this.amsProjectinfos != null)
         {
            AmsProjectinfos oldAmsProjectinfos = this.amsProjectinfos;
            this.amsProjectinfos = null;
            oldAmsProjectinfos.removeAmsProtminfos(this);
         }
         if (newAmsProjectinfos != null)
         {
            this.amsProjectinfos = newAmsProjectinfos;
            this.amsProjectinfos.addAmsProtminfos(this);
         }
      }
   }
   /** @pdGenerated default parent getter */
   public Userinfos getUserinfos() {
      return userinfos;
   }
   
   /** @pdGenerated default parent setter
     * @param newUserinfos */
   public void setUserinfos(Userinfos newUserinfos) {
      if (this.userinfos == null || !this.userinfos.equals(newUserinfos))
      {
         if (this.userinfos != null)
         {
            Userinfos oldUserinfos = this.userinfos;
            this.userinfos = null;
            oldUserinfos.removeAmsProtminfos(this);
         }
         if (newUserinfos != null)
         {
            this.userinfos = newUserinfos;
            this.userinfos.addAmsProtminfos(this);
         }
      }
   }

}